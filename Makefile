-include build/base.Makefile
-include build/docker.Makefile

help::
	@echo "$(APP_NAME) targets:"
	@echo "    init.app        - init application environment"
	@echo "    init.dev        - init development environment"
	@echo "    api             - build models for API requests/responses"
	@echo "    lint            - verify code style"
	@echo "    spell           - check spelling of text"
	@echo "    format          - apply automatic formatting"
	@echo "    test            - run test suite"
	@echo "    coverage        - test the code coverage"
	@echo "    db.init         - drop and re-init the database"
	@echo "    db.status       - display database status"
	@echo "    db.connect      - connect to the database"
	@echo "    log             - tail log file"
	@echo "    run             - start app"
	@echo "    release         - tag and push release with current version"
	$(call docker_help)


# Settings

TIMEZONE := "Europe/Vienna"

AURA_ENGINE_CORE_SOCKET := "aura_engine_socket"
AURA_ENGINE_CONFIG := ${CURDIR}/config/engine.docker.yaml
AURA_AUDIO_STORE_SOURCE := ${CURDIR}/../engine-core/audio/source
AURA_AUDIO_STORE_PLAYLIST := ${CURDIR}/../engine-core/audio/playlist
AURA_LOGS := ${CURDIR}/logs
AURA_UID := 2872
AURA_GID := 2872

DOCKER_RUN = @docker run \
		--name $(APP_NAME) \
		--network="host" \
		--mount type=tmpfs,destination=/tmp \
		--env-file docker.env \
		-v aura_engine_socket:"/srv/socket" \
		-v "$(AURA_ENGINE_CONFIG)":"/etc/aura/engine.yaml":ro \
		-v "$(AURA_AUDIO_STORE_SOURCE)":"/var/audio/source":ro \
		-v "$(AURA_AUDIO_STORE_PLAYLIST)":"/var/audio/playlist":ro \
		-v "$(AURA_LOGS)":"/srv/logs" \
		-u $(AURA_UID):$(AURA_GID) \
		$(DOCKER_ENTRY_POINT) \
		autoradio/$(APP_NAME)

# Targets

init.app:: pyproject.toml
	poetry install
	cp -n config/sample.engine.yaml config/engine.yaml
	mkdir -p .cache

init.dev:: pyproject.toml
	poetry install --with dev
	poetry run pre-commit autoupdate
	poetry run pre-commit install
	cp -n config/sample.engine.yaml config/engine.yaml
	mkdir -p .cache

api::
	rm -rf .build
	poetry run openapi-python-client generate --path schemas/openapi-tank.json --config .openapi-client-tank.yml
	cp -r .build/aura_tank_api/models src/aura_tank_api
	cp .build/aura_tank_api/py.typed src/aura_tank_api
	cp .build/aura_tank_api/types.py src/aura_tank_api
	rm -rf .build
	poetry run openapi-python-client generate --path schemas/openapi-steering.json --config .openapi-client-steering.yml
	cp -r .build/aura_steering_api/models src/aura_steering_api
	cp .build/aura_steering_api/py.typed src/aura_steering_api
	cp .build/aura_steering_api/types.py src/aura_steering_api

lint::
	poetry run python3 -m flake8 .

spell::
	poetry run codespell $(wildcard *.md) docs src tests config contrib

format::
	poetry run python3 -m isort .
	poetry run black .

test::
	poetry run python3 -m unittest discover . --pattern "test_*.py"

coverage::
	poetry run coverage run -m unittest discover . --pattern "test_*.py" && poetry run coverage report -m && poetry run coverage xml

db.init::
	sudo -u postgres psql -f contrib/postgresql-drop-all.sql
	sudo -u postgres psql -f contrib/postgresql-create-database.sql

db.status::
	pg_isready -d aura_engine -h localhost -p 5432 -U aura_engine

db.connect::
	PGHOST=127.0.0.1 PGPORT=5432 PGUSER=aura_engine PGPASSWORD=pass_1234 PGDATABASE=aura_engine psql

log::
	tail -f logs/engine.log

run::
	poetry run python3 -m aura_engine.app

release::
	$(eval VERSION := $(shell python3 -c 'import tomli; print(tomli.load(open("pyproject.toml", "rb"))["tool"]["poetry"]["version"])'))
	git tag $(VERSION)
	git push origin $(VERSION)
	@echo "Release '$(VERSION)' tagged and pushed successfully."