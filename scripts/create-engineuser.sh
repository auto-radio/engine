#!/bin/bash

if getent passwd 'engineuser' > /dev/null 2>&1; then  
    echo "User 'engineuser' exists already."; 
else     
    echo "Creating Engine User ..."
    adduser engineuser
    adduser engineuser audio
fi