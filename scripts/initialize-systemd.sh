#!/bin/bash

#
# Prepare folders and permissions for installing engine on production.
#
# You'll need sudo/root privileges.
#

echo "Set Ownership of '/opt/aura/engine', '/var/log/aura/' and '/etc/aura/engine.yaml' to Engine User"
chown -R engineuser:engineuser /opt/aura
chown -R engineuser:engineuser /etc/aura
chown -R engineuser:engineuser /var/log/aura
chown -R engineuser:engineuser /var/log/supervisor

echo "Copy Systemd unit files to '/etc/systemd/system/'"
cp -n /opt/aura/engine/config/systemd/* /etc/systemd/system/