# Base config for AURA Makefiles
# Include this at the top of other Makesfiles

.DEFAULT_GOAL := help

APP_NAME := $(shell basename $(dir $(abspath $(dir $$PWD/Makefile))))

UID = $(shell id -u)
GID = $(shell id -g)