# Aura Engine Development Guide

This page gives insights on extending Aura Engine internals or through the API.

<!-- TOC -->

1. [Aura Engine Development Guide](#aura-engine-development-guide)
   1. [AURA Components](#aura-components)
   2. [Engine Components](#engine-components)
   3. [Running for Development](#running-for-development)
   4. [Testing](#testing)
   5. [API](#api)
   6. [Scheduler](#scheduler)
   7. [Daemonized Engine](#daemonized-engine)
      1. [Running with Systemd](#running-with-systemd)
      2. [Running with Supervisor](#running-with-supervisor)
   8. [Logging](#logging)
   9. [Read more](#read-more)

<!-- /TOC -->

## AURA Components

AURA Engine as part of the AURA Radio Suite uses an modulear architecture based on a REST API. All external information is retrieved using JSON data-structures.

To get the basic architectural overview, visit [docs.aura.radio](https://docs.aura.radio).

Starting development of engine can be quite tedious, as it requires all most all other AURA components to be up and running.

For example:

- Steering, to get the main incredient of an play-out engine: schedules (or "timeslots" in Steering terms),
  which hold the actual information on playlists and their entries.
- Dashboard, to have a neat interface, being able to programme the timeslots
- Tank, to get the references to audio files and other audio sources. Plus the actual files.

If you need to test and develop against the Engine's API you'll also need to get the `engine-api` project running.

For a start it is recommended to create a general `aura` project folder. In there you start cloning all the sub-projects. After having all the sub-projects configured, and verified that they are working, take a look at the `aura/aura` repository.

## Engine Components

_...TBD..._

## Running for Development

Ensure you have following other projects up and running:

- steering
- tank
- dashboard
- engine-api
- dashboard-clock (optional)

The following steps expect you having done the bases configuration and set up a database as outlined in the [Native Installation](https://gitlab.servus.at/aura/engine/-/blob/main/docs/bare-metal-installation.md) document.

Start Liquidsoap which is part of Engine Core:

```shell
~/code/aura/engine-core$ make run
```

Now run the Engine:

```shell
~/code/aura/engine$ make run
```

If your IDE of choice is _Visual Studio Code_, then there are launch settings provided in `.vscode/launch.json`.

## Testing

Test cases are located in `./tests` are executed by running:

```shell
~/code/aura/engine$ make test
```

## API

You can find the AURA API definition at https://api.aura.radio

Engine utilizes the Steering API and Tank API to query scheduling and playlist data.

The OpenAPI specification for these APIs are located in `schemas` and used for the client models.

To generate the API client models run

```bash
make api
```

Find the generated models in the packages `aura_steering_api` and `aura_tank_api`.

## Scheduler

Scheduling is split into multiple phases. Below you see a timeline with one timeslot planned at a certain
point in time and the involved phase before:

```ascii
========================================= [                  Scheduling Window               ] ===========
=======================================================  [        Timeslot Play-out                 ] ====

== (FILESYSTEM A) ========================== [ Preload ] [  Play 4  ] ====================================
== (STREAM A) ========================================== [ Preload ] [ Play 1 ] ==========================
== (LIVE 1) ====================================================== [ Preload ] [ Play 1 ] ================
== (FILESYSTEM B) ========================================================== [ Preload ] [  Play 4  ] ====
```

- **Scheduling Window**: Within the scheduling window any commands for controlling
  the mixer of the soundsystem are prepared and queued.

  Only until the start of the window, timeslots can be updated or removed via external API Endpoints
  (e.g. using Steering or Dashboard). Until here any changes on the timeslot itself will be reflected
  in the actual play-out. This only affects the start and end time of the "timeslot" itself.
  It does not involve related playlists and their entries. Those can still be modified after the
  scheduling window has started.

  The start and the end of the window is defined by the start of the timeslot minus
  a configured amount of seconds (see `scheduling_window_start` and `scheduling_window_end`
  in `engine.yaml`). The actual start of the window is calculated by (timeslot start - window start)
  and the end by (timeslot end - window end)

  During the scheduling window, the external API Endpoints are pulled continuously, to
  check for updated timeslots and related playlists. Also, any changes to playlists and
  its entries are respected within that window (see `fetching_frequency` in `engine.yaml`).

  > Important: It is vital that the the scheduling window is wider than the fetching frequency.
  > Otherwise one fetch might never hit a scheduling window, hence not being able to schedule stuff.

  > Note: If you delete any existing timeslot in Dashboard/Steering this is only reflected in Engine until the start
  > of the scheduling window. The scheduling window is defined by the start of the timeslot minus a configured offset
  > in seconds. This is limitation is required to avoid corrupted playout in case audio content has been
  > preloaded or started playing already.

- **Queuing and Pre-Loading**: Before any playlist entries of the timeslot can be turned into
  sound, they need to be queued and pre-loaded. Ideally the pre-loading happens somewhat before
  the scheduled play-out time to avoid any delays in timing. Set the maximum time reserved for
  pre-loading in your configuration (compare `preload_offset`in `engine.yaml`).

  If there is not enough time to reserve the given amount of time for preloading (i.e. some entry
  should have started in the past already) the offset is ignored and the entry is played as soon as possible.

  > Important: To ensure proper timings, the offset should not exceed the time between the start of
  > the scheduling-window and the start of the actual timeslot playout. Practically, of course there
  > are scenario where playout start later than planned e.g. during startup of the engine during a timeslot
  > or due to some severe connectivity issues to some external stream.

- **Play-out**: Finally the actual play-out is happening. The faders of the virtual mixers are pushed
  all the way up, as soon it is "time to play" for one of the pre-loaded entries.
  Transitions between playlist entries with different types of sources (file, stream and line
  inputs) are performed automatically. At the end of each timeslot the channel is faded-out,
  no matter if the total length of the playlist entries would require a longer timeslot.

  If for some reason the playout is corrupted, stopped or too silent to make any sense, then
  this <u>triggers a fallback using the silence detector</u>.

## Daemonized Engine

For this you can utilize either [Systemd](https://systemd.io/) or [Supervisor](http://supervisord.org/). Please check the their manuals on how to use these services.

The daemon configs are expecting you run engine under the user `engineuser` and being located under `/opt/aura/engine`, `/opt/aura/engine-api` and `/opt/aura/engine-core` respectively. Do prepare the project root and permissions you can use the script `scripts/initialize-systemd.sh`. To create the matching user and audio group membership run `scripts/create-engineuser.sh`.

### Running with Systemd

Copy the unit files in `/opt/aura/engine/config/systemd/aura-engine.service` to your systemd unit directory, and reload the systemd daemon:

```shell
cp /opt/aura/engine/config/systemd/* /etc/systemd/system/
systemctl daemon-reload
```

### Running with Supervisor

Now, given you are in the engine's home directory like `/opt/aura/engine/`, simply type following to start the services:

```shell
supervisord
```

This picks up the supervisor configuration provided in the local `supervisord.conf` and the service configurations located in `config/supervisor/*.conf`.

Then you'll need to reload the supervisor configuration using `sudo`:

```shell
sudo supervisorctl reload
```

## Logging

All Engine logs can be found under `./logs`.

## Read more

- [Bare Metal Installation](docs/bare-metal-installation.md)
- [Developer Guide](docs/developer-guide.md)
- [Setting up the Audio Store [docs.aura.radio]](https://docs.aura.radio/en/latest/administration/setup-audio-store.html)
