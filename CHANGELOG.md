# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- ...

### Changed

- ...

### Deprecated

- ...

### Removed

- ...

### Fixed

- ...

### Security

- ...

## [1.0.0-alpha2] - 2023-06-19

### Added

- API responses from Steering and Tank are now cached in `cache_dir`

### Changed

- Provide properties in API schemas in CamelCase notation (aura#141)

### Removed

- Remove mail service as it will be replaced by Prometheus monitoring (engine#109)

### Fixed

- Fix an issue where the Engine version is displayed in the logs wrongly

## [1.0.0-alpha1] - 2023-02-22

Initial release.

