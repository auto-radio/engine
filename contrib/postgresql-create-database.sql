\c postgres
create database aura_engine;
create user aura_engine with encrypted password 'pass_1234';
grant all privileges on database aura_engine to aura_engine;
