#!/usr/bin/env python3

# Copyright (c) 2001, Nicola Larosa
# All rights reserved.
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#    * Neither the name of the <ORGANIZATION> nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


"""PyHeartBeat server: receives and tracks UDP packets from all clients.

While the BeatLog thread logs each UDP packet in a dictionary, the main
thread periodically scans the dictionary and prints the IP addresses of the
clients that sent at least one packet during the run, but have
not sent any packet since a time longer than the definition of the timeout.

Adjust the constant parameters as needed, or call as:

    PyHeartBeat.py [udpport [timeout]]

Set the environment variable "DEBUG" to "1" in order to emit more detailed
debug messages.
In addition "127.0.0.1" is marked as a previously active peer.


Manual heartbeat messages can be easily sent via "netcat":

    echo foo | nc -q 1 -u localhost 43334


https://www.oreilly.com/library/view/python-cookbook/0596001673/ch10s13.html
"""

import os
import socket
import sys
from threading import Event, Lock, Thread
from time import ctime, sleep, time

DEFAULT_HEARTBEAT_PORT = 43334
DEFAULT_WAIT_PERIOD = 10
DEBUG_ENABLED = os.getenv("DEBUG", "0") == "1"


class BeatDict:
    """Manage heartbeat dictionary."""

    def __init__(self):
        self.beatDict = {}
        if DEBUG_ENABLED:
            self.beatDict["127.0.0.1"] = time()
        self.dictLock = Lock()

    def __repr__(self):
        result = ""
        self.dictLock.acquire()
        for key in self.beatDict.keys():
            result += "IP address: %s - Last time: %s\n" % (
                key,
                ctime(self.beatDict[key]),
            )
        self.dictLock.release()
        return result

    def update(self, entry):
        """Create or update a dictionary entry."""
        self.dictLock.acquire()
        self.beatDict[entry] = time()
        self.dictLock.release()

    def extractSilent(self, howPast):
        """Return a list of entries older than howPast."""
        silent = []
        when = time() - howPast
        self.dictLock.acquire()
        for key in self.beatDict.keys():
            if self.beatDict[key] < when:
                silent.append(key)
        self.dictLock.release()
        return silent


class BeatRec(Thread):
    """Receive UDP packets, log them in heartbeat dictionary."""

    def __init__(self, goOnEvent, updateDictFunc, port):
        Thread.__init__(self)
        self.goOnEvent = goOnEvent
        self.updateDictFunc = updateDictFunc
        self.port = port
        self.recSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.recSocket.settimeout(0.2)
        self.recSocket.bind(("", port))

    def __repr__(self):
        return f"Heartbeat Server on port: {self.port}"

    def run(self):
        """Start the beat receiver."""
        while self.goOnEvent.isSet():
            if DEBUG_ENABLED:
                print("Waiting to receive...")
            try:
                data, addr = self.recSocket.recvfrom(6)
            except socket.timeout:
                # no incoming message -> no timestamp update -> check again
                pass
            else:
                if DEBUG_ENABLED:
                    print(f"Received packet from {addr}")
                self.updateDictFunc(addr[0])


def main():
    """Listen to the heartbeats and detect inactive clients."""
    if len(sys.argv) > 1:
        heartbeat_port = int(sys.argv[1])
    else:
        heartbeat_port = DEFAULT_HEARTBEAT_PORT
    if len(sys.argv) > 2:
        wait_period = float(sys.argv[2])
    else:
        wait_period = DEFAULT_WAIT_PERIOD

    beatRecGoOnEvent = Event()
    beatRecGoOnEvent.set()
    beatDictObject = BeatDict()
    beatRecThread = BeatRec(beatRecGoOnEvent, beatDictObject.update, heartbeat_port)
    if DEBUG_ENABLED:
        print(beatRecThread)
    beatRecThread.start()
    print(f"PyHeartBeat server listening on port {heartbeat_port}")
    print("\n*** Press Ctrl-C to stop ***\n")
    while True:
        try:
            if DEBUG_ENABLED:
                print(f"Beat Dictionary: {beatDictObject}")
            silent = beatDictObject.extractSilent(wait_period)
            if silent:
                print(f"Silent clients: {' '.join(silent)}")
            sleep(wait_period)
        except KeyboardInterrupt:
            print("Exiting.")
            beatRecGoOnEvent.clear()
            beatRecThread.join()
            break


if __name__ == "__main__":
    main()
