CREATE DATABASE aura_engine CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'aura_engine'@'localhost' IDENTIFIED BY 'pass_1234';
GRANT ALL PRIVILEGES ON aura_engine.* TO 'aura_engine'@'localhost';
FLUSH PRIVILEGES;