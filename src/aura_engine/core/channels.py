#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-2020 - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
Channels Module.

Base types for channels:
    - ChannelName: Valid channel names as defined in core.
    - ChannelType: Holds mappings to concrete channel names.

Channel definitions:

    - GenericChannel: All other channels inherit from this one.
    - QueueChannel: Handles queues such as filesystem entries.
    - StreamChannel: Handles stream connections.
    - LineChannel: Handles line audio input.
"""

import json
import logging
import time
from enum import Enum
from threading import Thread

from aura_engine.base.api import LiquidsoapUtil as LU
from aura_engine.base.config import AuraConfig
from aura_engine.base.lang import DotDict, private
from aura_engine.base.utils import SimpleUtil as SU


class ChannelName(str, Enum):
    """
    Allowed channel names.

    These are name mappings to the Liqidsoap channel IDs.
    """

    QUEUE_A = "in_queue_0"
    QUEUE_B = "in_queue_1"
    HTTP_A = "in_stream_0"
    HTTP_B = "in_stream_1"
    LIVE_0 = "in_line_0"
    LIVE_1 = "in_line_1"
    LIVE_2 = "in_line_2"
    LIVE_3 = "in_line_3"
    LIVE_4 = "in_line_4"
    FALLBACK_FOLDER = "fallback_folder"
    FALLBACK_PLAYLIST = "fallback_playlist"

    def __str__(self):
        return str(self.value)


class ChannelType(dict, Enum):
    """
    Available channel types.

    Engine channel types mapped to `Entry` source types.
    """

    QUEUE = {"id": "fs", "numeric": 0, "channels": [ChannelName.QUEUE_A, ChannelName.QUEUE_B]}
    HTTP = {"id": "http", "numeric": 1, "channels": [ChannelName.HTTP_A, ChannelName.HTTP_B]}
    LIVE = {
        "id": "live",
        "numeric": 3,
        "channels": [
            ChannelName.LIVE_0,
            ChannelName.LIVE_1,
            ChannelName.LIVE_2,
            ChannelName.LIVE_3,
            ChannelName.LIVE_4,
        ],
    }
    FALLBACK_POOL = {
        "id": "fallback_pool",
        "numeric": 5,
        "channels": [ChannelName.FALLBACK_FOLDER, ChannelName.FALLBACK_PLAYLIST],
    }

    @property
    def channels(self):
        """
        Retrieve all channels for given type.
        """
        return self.value["channels"]

    @property
    def numeric(self):
        """
        Retrieve numeric representation of channel type for given type.
        """
        return self.value["numeric"]

    def __str__(self):
        return str(self.value["id"])


class GenericChannel:
    """
    Base class for channel implementations.

    Attributes:
        type (ChannelType): Type of channel such as queue, stream or line
        name (ChannelName): Name of the channel as defined in Liquidsoap
        index (int): Position of the channel on the mixer
        ready (bool): Indicates if the channel is ready to be used
        selected (bool): Indicates if the channel is selected
        single (bool): ?
        volume (int): Volume from -1 to 100, where -1 indicates an error
        remaining (float): Seconds remaining to be played
    """

    logger: None
    config: None

    type: ChannelType = None
    name: ChannelName = None
    index: int = None
    ready: bool = None
    selected = bool = None
    single: bool = None
    volume: int = None
    remaining: float = None

    def __init__(self, channel_index: int, channel_name: int, mixer):
        """
        Initialize the channel instance.

        Args:
            channel_index (int): Index of the channel on the mixer
            channel_name (ChannelName): Name of the channel
            mixer (Mixer): The mixer instance

        """
        self.config = AuraConfig.instance.config
        self.logger = logging.getLogger("engine")
        self.mixer = mixer
        self.name = channel_name
        self.index = channel_index

    def get_index(self) -> int:
        """
        Retrieve the channel index.

        Returns:
            int: The index
        """
        return self.index

    def get_type(self) -> ChannelType:
        """
        Retrieve the `ChannelType`.
        """
        return self.type

    def get_status(self) -> dict:
        """
        Get channel status information.

        Returns:
            dict: {
                ready (bool): Indicates if the channel is ready to be used
                selected (bool): Indicates if the channel is selected
                single (bool): ?
                volume (int): Volume from -1 to 100, where -1 indicates an error
                remaining (float): Seconds remaining to be played
            }
        """
        return DotDict(
            {
                "ready": self.ready,
                "selected": self.selected,
                "single": self.single,
                "volume": self.volume,
                "remaining": self.remaining,
            }
        )

    def set_status(self, ready: bool, selected: bool, single: bool, volume: int, remain: float):
        """
        Set channel status information.

        Args:
            ready (bool): Indicates if the channel is ready to be used
            selected (bool): Indicates if the channel is selected
            single (bool): ?
            volume (int): Volume from -1 to 100, where -1 indicates an error
            remaining (float): Seconds remaining to be played
        """
        self.ready = ready
        self.selected = selected
        self.single = single
        self.volume = volume
        self.remaining = remain

    def load(self, metadata: dict = None) -> bool:
        """
        Interface definition for loading a channel track.

        Args:
            uri (str): The URI to load
            metadata (dict): Metadata to assign to the channel, when playing (optional)

        Returns:
            (bool): True if track loaded successfully

        """
        if metadata:
            json_meta = json.dumps(metadata, ensure_ascii=False)
            return self.set_track_metadata(json_meta)
        return True

    def fade_in(self, volume: int, instant=False):
        """
        Perform a fade-in for the given channel.

        Args:
            instant(bool): If true the fade instantly jumps to target volume

        Returns:
            (bool): True if fade successful

        """
        if instant:
            self.logger.info(SU.pink(f"Activate channel {self}"))
            return self.mixer.activate_channel(self, True)
        else:
            self.logger.info(SU.pink(f"Fade in channel {self}"))
            faded = self.mixer.select_channel(self, True)
            selected = self.mixer.fade_in(self, volume)
            self.mixer.set_active_channel(self)
            return faded and selected

    def fade_out(self, instant=False):
        """
        Perform a fade-out for the given channel starting at its current volume.

        Args:
            instant(bool): If true the fade instantly jumps to zero volume

        Returns:
            (bool): True if fade successful

        """
        if instant:
            self.logger.info(SU.pink(f"Activate channel {self}"))
            return self.mixer.activate_channel(self, False)
        else:
            self.logger.info(SU.pink(f"Fade out channel {self}"))
            faded = self.mixer.fade_out(self)
            selected = self.mixer.select_channel(self, False)
            self.mixer.set_active_channel(None)
            return faded and selected

    def roll(self, seconds_to_roll):
        """
        Fast-forward to a position in time within the queue track.

        Most channels do not support this feature.
        """
        return True

    def __str__(self):
        """
        String representation of the Channel.
        """
        return f"[{self.index} : {self.name}]"

    @private
    def set_track_metadata(self, json_metadata: str) -> bool:
        """
        Set the metadata as current track metadata on the given channel.

        This is only needed for non-queue channels. They pass the metadata inline with their
        request URIs

        Args:
            json_metadata(str): String containing metadata as JSON

        Returns:
            (bool): True if metadata successfully set

        @private
        """
        response = self.mixer.client.exec(self.name, "set_track_metadata", json_metadata)
        msg = f"Response for '{self.name}.set_track_metadata': {response}"
        self.logger.info(SU.pink(msg))
        if response not in PlayoutStatusResponse.SUCCESS.value:
            msg = f"Error while setting metadata on {self.name} to:\n{json_metadata}"
            self.logger.error(SU.red(msg))
            return False
        return True


class QueueChannel(GenericChannel):
    """
    Channel for queues such as a collection of filesystem URIs.
    """

    def __init__(self, channel_index, channel_name, mixer):
        """
        Initialize the queue channel instance.

        Args:
            mixer (Mixer): The mixer instance
            channel_index (int): Channel index on the mixer

        """
        self.type = ChannelType.QUEUE
        super().__init__(channel_index, channel_name, mixer)

    def load(self, uri: str = None, metadata: dict = None):
        """
        Load the provided URI and pass metadata.

        Does not load the `super` implementation, as queues have their individual approach to
        pass metadata in the URI.

        Args:
            uri (str): The URI to load
            metadata (dict): Metadata to assign to the channel, when playing (optional)

        Returns:
            (bool): True if track loaded successfully
        """
        self.logger.info(SU.pink(f"{self.name}.push('{uri}')"))
        if metadata:
            uri = LU.annotate_uri(uri, metadata)
        response = self.mixer.client.exec(self.name, "push", uri)
        self.logger.debug(SU.pink(f"{self.name}.push result: {response}"))

        # If successful, Liquidsoap returns a resource ID of the queued track
        resource_id = -1
        try:
            resource_id = int(response)
        except ValueError:
            msg = SU.red(f"Got invalid resource ID: '{response}'")
            self.logger.error(msg)
            return False
        return resource_id >= 0

    def roll(self, seconds):
        """
        Fast-forward to a position in time within the queue track.

        Args:
            seconds(int): How many seconds the FFWD should performed

        Returns:
            (bool): True after successful roll

        """
        response = self.mixer.client.exec(self.name, "roll", str(seconds))
        if response == "OK":
            return True
        return False

    def fade_out(self, instant=False):
        """
        Fade out channel and flush the queue.

        Args:
            instant (bool, optional): Instant volume change instead of fade. Defaults to False

        Returns:
            (bool): True after successful fade out
        """
        response = super().fade_out(instant)
        self.flush()
        return response

    @private
    def flush(self):
        """
        Remove all items from queue.

        @private
        """

        def flush_queue():
            # Wait some moments, if there is some long fade-out. Note, this also
            # means, this channel should not be used for at least some seconds
            # (including clearing time).
            clear_timeout = 5
            msg = f"Clearing channel {self} in {clear_timeout} seconds"
            self.logger.info(SU.pink(msg))
            time.sleep(clear_timeout)

            # Deactivate channel
            response = self.mixer.client.exec(self.name, "clear")
            msg = f"Cleared queue channel '{self.name}' with result '{response}'"
            self.logger.info(SU.pink(msg))

        Thread(target=flush_queue).start()


class StreamChannel(GenericChannel):
    """
    Channel for audio stream input.
    """

    def __init__(self, channel_index, channel_name, mixer):
        """
        Initialize the queue channel instance.

        Args:
            mixer (Mixer): The mixer instance
            channel_index (int): Channel index on the mixer

        """
        self.type = ChannelType.HTTP
        super().__init__(channel_index, channel_name, mixer)

    def load(self, uri: str = None, metadata: dict = None):
        """
        Load the given stream entry and updates the entries's status codes.

        Args:
            uri (str): The URI to load
            metadata (dict): Metadata to assign to the channel, when playing (optional)

        Returns:
            (bool): True if track loaded successfully

        """
        self.logger.debug(SU.pink(f"Loading stream '{uri}'"))
        retry_delay = self.config.get("input_stream_retry_delay")
        max_retries = self.config.get("input_stream_max_retries")
        retries = 0

        self.stop()
        self.set_url(uri)
        self.start()

        while not self.is_ready(uri):
            if retries >= max_retries:
                msg = f"Stream connection failed after {retries * retry_delay} seconds!"
                raise LoadSourceException(msg)
            time.sleep(retry_delay)
            retries += 1

        response = super().load(metadata)
        return response

    @private
    def is_ready(self, url):
        """
        Check if the stream on the given channel is ready to play.

        Note this method is blocking some serious amount of time even when successful; hence it is
        worth being called asynchronously.

        Args:
            channel (ChannelName): The stream channel
            url (String): The stream URL

        Returns:
            (Boolean): `True` if successful

        @private

        """
        is_ready = True
        response = self.mixer.client.exec(self.name, "status")
        msg = f"{self.name}.status result: {response}"
        self.logger.info(SU.pink(msg))
        if not response.startswith(PlayoutStatusResponse.STREAM_STATUS_CONNECTED.value):
            return False

        lqs_url = response.split(" ")[1]

        if not url == lqs_url:
            msg = f"Wrong URL '{lqs_url}' set for channel '{self.name}', expected: '{url}'."
            self.logger.error(msg)
            is_ready = False

        if is_ready:
            stream_buffer = self.config.get("input_stream_buffer")
            msg = f"Ready to play stream, but wait {stream_buffer} seconds to fill buffer..."
            self.logger.info(SU.pink(msg))
            time.sleep(round(float(stream_buffer)))

        return is_ready

    @private
    def stop(self):
        """
        Stop the stream.
        """
        response = self.mixer.client.exec(self.name, "stop")
        if response not in PlayoutStatusResponse.SUCCESS.value:
            self.logger.error(SU.red(f"{self.name}.stop result: {response}"))
            raise LoadSourceException("Error while stopping stream!")
        return response

    @private
    def set_url(self, url):
        """
        Set the stream URL.
        """
        response = self.mixer.client.exec(self.name, "url", url)
        if response not in PlayoutStatusResponse.SUCCESS.value:
            self.logger.error(SU.red(f"{self.name}.url result: {response}"))
            raise LoadSourceException("Error while setting stream URL!")
        return response

    @private
    def start(self):
        """
        Start the stream URL.
        """
        response = self.mixer.client.exec(self.name, "start")
        self.logger.info(SU.pink(f"{self.name}.start result: {response}"))
        return response


class LineChannel(GenericChannel):
    """
    Channel for line audio input.
    """

    def __init__(self, channel_index, channel_name, mixer):
        """
        Initialize the queue channel instance.

        Args:
            mixer (Mixer): The mixer instance
            channel_index (int): Channel index on the mixer

        """
        self.type = ChannelType.LIVE
        super().__init__(channel_index, channel_name, mixer)

    def load(self, uri: str = None, metadata: dict = None):
        """
        Load the line channel.

        Args:
            uri (str): For line source the URI is always null
            metadata (dict): Metadata to assign to the channel, when playing (optional)

        Returns:
            (bool): True if track loaded successfully

        """
        response = super().load(metadata)
        return response


class ChannelFactory:
    """
    A factory to construct channels based on a given channel name.
    """

    def __init__(self, mixer):
        """
        Initialize the channel factory.

        Args:
            mixer (Mixer): The mixer instance

        """
        self.config = AuraConfig.instance
        self.logger = logging.getLogger("engine")
        self.mixer = mixer

    def create_channel(self, channel_index, channel_name: ChannelName, mixer) -> GenericChannel:
        """
        Create a channel with the provided details.

        Depending on the given channel name, a different channel is instantiated.

        Args:
            channel_index (int): The index of the channel on the mixer
            channel_name (ChannelName): The channel name as defined in Liquidsoap
            mixer (Mixer): The mixer instance

        Returns:
            (GenericChannel): A concrete implementation of the generic channel

        """
        if channel_name in ChannelType.QUEUE.channels:
            self.logger.debug(f"Create new QUEUE channel '{channel_name}'")
            return QueueChannel(channel_index, channel_name, mixer)
        if channel_name in ChannelType.HTTP.channels:
            self.logger.debug(f"Create new STREAM channel '{channel_name}'")
            return StreamChannel(channel_index, channel_name, mixer)
        if channel_name in ChannelType.LIVE.channels:
            self.logger.debug(f"Create new LINE channel '{channel_name}'")
            return LineChannel(channel_index, channel_name, mixer)


class PlayoutStatusResponse(str, Enum):
    """
    Response values indicating some status.
    """

    SUCCESS = ["OK", "Done", "Done!", "Donee!"]
    STREAM_STATUS_POLLING = "polling"
    STREAM_STATUS_STOPPED = "stopped"
    STREAM_STATUS_CONNECTED = "connected"


class LoadSourceException(Exception):
    """
    Exception thrown when some source could not be loaded or updated.
    """

    pass
