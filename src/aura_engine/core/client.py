#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-now() - The Aura Engine Team.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
Message and connection handling to Engine Core (Liquidsoap).
"""

import logging
import socket
import urllib.parse

from aura_engine.base.config import AuraConfig
from aura_engine.base.lang import private, synchronized
from aura_engine.base.utils import SimpleUtil as SU
from aura_engine.core.mixer import Mixer
from aura_engine.events import EngineEventDispatcher


class CoreClient:
    """
    Client managing communication with Engine Core (Liquidsoap).
    """

    skip_log_commands = ("aura_engine.status", "mixer.volume")

    instance = None
    logger = None
    connection = None
    event_dispatcher = None
    conn = None

    def __init__(self, event_dispatcher: EngineEventDispatcher):
        """
        Initialize the client.
        """
        self.logger = logging.getLogger("engine")
        self.config = AuraConfig.instance.config
        self.event_dispatcher = event_dispatcher
        self.conn = CoreConnection()

    @staticmethod
    def get_instance(event_dispatcher: EngineEventDispatcher):
        """
        Get an instance of the client singleton.
        """
        if not CoreClient.instance:
            CoreClient.instance = CoreClient(event_dispatcher)
        return CoreClient.instance

    @synchronized
    def connect(self):
        """
        Open connection.

        @synchronized
        """
        try:
            if not self.conn.is_connected():
                self.conn.open()
        except CoreConnectionError as e:
            self.logger.critical(SU.red(e.message))
            self.event_dispatcher.on_critical("Client connection error", e.message, str(e))

    @synchronized
    def disconnect(self):
        """
        Close the connection.

        @synchronized
        """
        if not self.conn.is_connected():
            self.conn.close()

    @synchronized
    def exec(self, namespace: str, action: str, args: str = "") -> str:
        """
        Execute a command.

        Args:
            namespace (str): The namespace for the command to execute.
            action (str): The action to execute.
            args (str, optional): Arguments passed with the action. Defaults to "".

        Raises:
            CoreConnectionError: Raised when there is a connection or communication error.

        Returns:
            str: result of the command (optional).

        @synchronized

        """
        response = None

        if not self.conn.is_connected():
            self.conn.open()
        try:
            command = self.build_command(namespace, action, args)
            self.log_debug(command, f"[>>] {command}")
            response = self.conn.send(command)
            if response:
                self.log_debug(command, f"[<<] {response}")
        except CoreConnectionError as e:
            msg = "Error while issuing command to Liquidsoap"
            self.event_dispatcher.on_critical("Core client connection issue", msg, str(e))
            raise CoreConnectionError(msg, e)
        return response

    @private
    def build_command(self, namespace: str, action: str, args: str) -> str:
        """
        Construct a command string for sending to Liquidsoap.

        Args:
            namespace (str): The namespace for the command to execute.
            action (str): The action to execute.
            args (str, optional): Arguments passed with the action. Defaults to "".

        Returns:
            str: The command string

        @private
        """
        args = str(args).strip()
        args = " " + urllib.parse.unquote(args) if args != "" else ""
        namespace = str(namespace) + "." if namespace else ""
        command = f"{namespace}{action}{args}"
        return command

    @private
    def log_debug(self, command: str, log_message: str):
        """
        Check if the command is excluded from debug logging.

        This is meant to avoid log-pollution by status and fade commands.

        @private

        """
        if self.config.log.level == "debug":
            cmds = CoreClient.skip_log_commands
            base_cmd = command.split(" ")[0]
            if not base_cmd.startswith(cmds):
                self.logger.debug(log_message)


class PlayoutClient(CoreClient):
    """
    Client managing communication with Engine Core (Liquidsoap).
    """

    mixer = None

    def __init__(self, event_dispatcher: EngineEventDispatcher):
        """
        Initialize the client.
        """
        super().__init__(event_dispatcher)
        self.mixer = Mixer("mixer", self)

    @staticmethod
    def get_instance(event_dispatcher: EngineEventDispatcher) -> CoreClient:
        """
        Get an instance of the client singleton.
        """
        if not PlayoutClient.instance:
            PlayoutClient.instance = PlayoutClient(event_dispatcher)
        return PlayoutClient.instance

    def get_mixer(self):
        """
        Get the mixer instance.
        """
        return self.mixer

    # ns:*

    def get_uptime(self) -> str:
        """
        Get info on how long core is running already.
        """
        return self.exec("", "uptime")

    # ns:aura_engine

    def get_version(self) -> str:
        """
        Get JSON with version information.
        """
        return self.exec("aura_engine", "version")

    def get_status(self) -> str:
        """
        Get engine status such as uptime and fallback mode.
        """
        return self.exec("aura_engine", "status")

    def set_config(self, json_config: str) -> str:
        """
        Send JSON with configuration options to core.
        """
        return self.exec("aura_engine", "update_config", json_config)


class CoreConnection:
    """
    Handles connections and sends commands to Engine Core (Liquidsoap).
    """

    ENCODING = "UTF-8"

    logger = None
    socket_path = None
    socket = None
    connected = None
    message = None

    def __init__(self):
        """
        Initialize the connection.
        """
        self.logger = logging.getLogger("engine")
        config = AuraConfig.instance
        socket_path = config.config.general.socket_dir + "/engine.sock"
        self.socket_path = config.to_abs_path(socket_path)
        self.logger.debug(f"Using socket at '{self.socket_path}'")

        self.connected = False
        self.message = ""
        self.socket = None

    def is_connected(self):
        """
        Return `True` if a connection is established.
        """
        return self.connected

    def open(self):
        """
        Connect to Liquidsoap socket.
        """
        try:
            self.socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.socket.connect(self.socket_path)
        except FileNotFoundError as e:
            msg = f"Socket file at '{self.socket_path}' not found. Is Liquidsoap running?"
            self.connected = False
            raise CoreConnectionError(msg, e)
        except socket.error as e:
            msg = f"Cannot connect to socket at '{self.socket_path}'"
            self.connected = False
            raise CoreConnectionError(msg, e)
        except Exception as e:
            msg = f"Unknown error while connecting to socket at '{self.socket_path}'"
            self.connected = False
            raise CoreConnectionError(msg, e)
        else:
            self.connection_attempts = 0
            self.connected = True

    def close(self):
        """
        Send quit command and close connection.
        """
        if self.connected:
            message = "quit\r"
            self.socket.sendall(message.decode(CoreConnection.ENCODING))
            self.socket.close()
            self.connected = False

    def send(self, command: str) -> str:
        """
        Send command to Liquidsoap.

        Args:
            command (str): The command string to be executed

        Raises:
            CoreConnectionError: Thrown when not connected

        Returns:
            str: Result of the command
        """
        result = None
        command += "\n"

        try:
            self.socket.sendall(command.encode())
            result = self.read()
        except BrokenPipeError:
            msg = "Broken Pipe while sending command"
            self.logger.info(SU.pink(msg))
            self.connected = False
            raise CoreConnectionError(msg)
        except Exception as e:
            msg = "Unknown Error while sending command"
            self.logger.error(SU.red(msg), e)
            self.connected = False
            raise CoreConnectionError(msg)

        return str(result)

    @private
    def read_all(self, timeout: int = 2) -> str:
        """
        Read data from the socket until `END` signal is received.

        Args:
            timeout (int, optional): Reading timeout in seconds. Defaults to 2.

        Returns:
            str: The response

        @private

        """
        data = ""
        try:
            self.socket.settimeout(timeout)
            while True:
                data += self.socket.recv(1).decode(CoreConnection.ENCODING)
                if data.find("END\r\n") != -1 or data.find("Bye!\r\n") != -1:
                    data.replace("END\r\n", "")
                    break
        except Exception as e:
            msg = "Unknown error while socket.read_all()"
            self.logger.error(SU.red(msg), e)
        return data

    @private
    def read(self) -> str:
        """
        Read from socket and store return value in `self.message` and return it.

        Returns:
            str: message read from socket

        @private

        """
        if self.connected:
            ret = self.read_all().splitlines()
            try:
                last = ret.pop()
                if last != "Bye!":
                    if len(ret) > 1:
                        self.message = str.join(" - ", ret)
                    elif len(ret) == 1:
                        self.message = ret[0]
                else:
                    self.message = last
            except Exception as e:
                msg = "Unknown error while socket.read()"
                self.logger.error(SU.red(msg), e)
            return self.message
        return None


class CoreConnectionError(Exception):
    """
    Exception thrown when there is a connection problem with Liquidsoap.
    """

    pass
