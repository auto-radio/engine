#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-2020 - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
The virtual mixer as it is realized using Liquidsoap.

A mixer consists of general methods to adjust volume, enable/disable channels etc. It also has
set of channels with their own control options.
"""

import logging
import time

from aura_engine.base.api import LiquidsoapUtil as LU
from aura_engine.base.config import AuraConfig
from aura_engine.base.lang import private
from aura_engine.base.utils import SimpleUtil as SU
from aura_engine.core.channels import (
    ChannelFactory,
    ChannelName,
    ChannelType,
    GenericChannel,
)


class Mixer:
    """
    A virtual mixer.
    """

    config = None
    logger = None
    client = None
    mixer_id = None
    channels = None
    channel_names = None
    active_channel_name: ChannelName = None

    def __init__(self, mixer_id: str, client):
        """
        Initialize the mixer.

        It loads all the available channels from Liquidsoap and pulls the faders down to zero.

        Args:
            mixer_id (str): The ID of the mixer in Liquidsoap
            client (PlayoutClient): The client for controlling playout

        """
        self.config = AuraConfig.instance.config
        self.logger = logging.getLogger("engine")
        self.mixer_id = mixer_id
        self.client = client
        self.channel_names = []
        self.channels = {}

        self.refresh_channels()

        # TODO Graceful reboot: At some point the current track playing could
        # resume inside Liquidsoap in case only Engine restarted (See #77).
        for n in self.channel_names:
            self.set_channel_volume(self.channels.get(n), 0)

    def get_inputs(self) -> dict:
        """
        Return the state of all mixer input channels.
        """
        self.refresh_channels()
        inputs = {}

        for idx, channel in enumerate(self.channel_names):
            inputs[channel] = self.get_channel_status(idx)
        return inputs

    def get_outputs(self) -> dict:
        """
        Retrieve the state of all mixer outputs.
        """
        outputs = self.client.exec(self.mixer_id, "outputs")
        outputs = LU.json_to_dict(outputs)
        return outputs

    def get_channel(self, channel_name: ChannelName) -> GenericChannel:
        """
        Retrieve a channel identified by name.
        """
        if channel_name:
            return self.channels.get(channel_name)
        return None

    def get_active_channel(self) -> GenericChannel:
        """
        Retrieve the currently active channel.
        """
        if self.active_channel_name:
            return self.get_channel(self.active_channel_name)
        return None

    def set_active_channel(self, channel: GenericChannel):
        """
        Set the currently active channel.
        """
        if channel:
            self.active_channel_name = channel.name
        else:
            self.active_channel_name = None
            self.logger.info(SU.pink("Reset active channel"))

    def get_free_channel(self, channel_type: ChannelType) -> GenericChannel:
        """
        Return any _free_ channel of the given type.

        A channel which is not currently active is seen as _free_.

        Args:
            channel_type (ChannelType): The type of channel to be retrieved

        Returns:
            (ChannelName, ChannelName): The active and next free channel of the requested type

        """
        free_channel: ChannelName = None
        active_channel = self.get_active_channel()
        if active_channel and active_channel.type == channel_type:
            free_channels = [c for c in channel_type.channels if c != self.active_channel_name]
            if len(free_channels) < 1:
                msg = f"Requesting channel of type '{channel_type}' but none free. \
                    Active channel: '{self.active_channel}'"
                self.logger.critical(SU.red(msg))
            else:
                free_channel = free_channels[0]
        else:
            free_channel = channel_type.channels[0]
        self.logger.info(SU.pink(f"Got free '{channel_type}' channel '{free_channel}'"))
        return self.channels.get(free_channel)

    def select_channel(self, channel: GenericChannel, select: bool) -> str:
        """
        Select or deselect some mixer channel.

        Args:
            channel (ChannelName): The channel number
            select (Boolean): Select or deselect

        Returns:
            (String): Liquidsoap server response

        """
        self.refresh_channels()

        try:
            if not self.channels:
                self.logger.critical(SU.red("Cannot select channel cuz there are no channels"))
            else:
                index = channel.get_index()
                select = "true" if select else "false"
                response = self.client.exec(self.mixer_id, "select", f"{index} {select}")
                self.update_channel_status(index, response)
                return True
        except Exception as e:
            self.logger.critical(SU.red("Ran into exception when selecting channel"), e)
            return False

    def activate_channel(self, channel: GenericChannel, activate: bool) -> str:
        """
        Activate a channel.

        Combined call of following to save execution time:
          - Select some mixer channel
          - Increase the volume to 100,

        Args:
            channel (ChannelName): The channel number
            activate (bool): Activate or deactivate

        Returns:
            (str): Liquidsoap server response

        """
        self.refresh_channels()

        try:
            if not self.channels:
                self.logger.critical(SU.red("Cannot activate channel cuz there are no channels"))
            else:
                index = channel.get_index()
                activate = "true" if activate else "false"
                response = self.client.exec(self.mixer_id, "activate", f"{index} {activate}")
                if response == "OK":
                    return True
        except Exception as e:
            self.logger.critical(SU.red("Ran into exception when activating channel."), e)
        return False

    def fade_in(self, channel: GenericChannel, volume: int = 100) -> bool:
        """
        Perform a fade-in for the given channel.

        Args:
            channel (GenericChannel): The channel to fade
            volume (Integer): The target volume

        Returns:
            (bool): `True` if successful

        TODO Think about using native Liquidsoap fading. This should bring better performance
             and wanted separation of concerns.
        """
        try:
            current_volume = self.get_channel_volume(channel)
            if current_volume == volume:
                msg = f"Skip fade in of {channel}: Already at target volume of {volume}%"
                self.logger.info(msg)
                return
            elif current_volume > volume:
                msg = f"Skip fade in of {channel}: Current volume of {current_volume}% exceeds \
                    target volume of {volume}%"
                self.logger.info(msg)
                return

            fade_in_time = self.config.scheduler.fade_in_time

            if fade_in_time > 0:
                self.fade_in_active = True
                target_volume = volume
                step = fade_in_time / target_volume
                msg = f"Fade in of {channel} to {target_volume}% ({step}s steps)"
                self.logger.debug(SU.pink(msg))
                for i in range(target_volume):
                    self.set_channel_volume(channel, i + 1)
                    time.sleep(step)
                msg = f"Fade in of {channel} done"
                self.logger.debug(SU.pink(msg))

        except Exception as e:
            self.logger.critical(SU.red(e.message), e)
            return False
        return True

    def fade_out(self, channel: GenericChannel) -> bool:
        """
        Perform a fade-out for the given channel starting at its current volume.

        Args:
            channel (GenericChannel): The channel to fade

        Returns:
            (Boolean): `True` if successful

        TODO Think about using native Liquidsoap fading. This should bring better performance
             and wanted separation of concerns.
        """
        try:
            current_volume = self.get_channel_volume(channel)

            if current_volume == 0:
                msg = f"Channel {channel} already at target volume of 0%. SKIPPING..."
                self.logger.info(msg)
                return

            fade_out_time = self.config.scheduler.fade_out_time

            if fade_out_time > 0:
                step = abs(fade_out_time) / current_volume
                msg = f"Start to fade out {channel} ({step}s step)"
                self.logger.debug(SU.pink(msg))
                for i in range(current_volume):
                    self.set_channel_volume(channel, current_volume - i - 1)
                    time.sleep(step)
                msg = f"Finished fade out of {channel}"
                self.logger.debug(SU.pink(msg))

        except Exception as e:
            self.logger.critical(SU.red(e.message), e)
            return False
        return True

    #
    #   Private Methods
    #

    @private
    def refresh_channels(self):
        """
        Retrieve all mixer channel names and create channel instances, if not available.

        @private
        """

        def create_channel(name) -> int:
            self.logger.debug(f"Set new channel name '{name}'")
            self.channel_names.append(name)
            idx = self.channel_names.index(name)
            channel = ChannelFactory.create_channel(self, idx, name, self)
            self.channels[name] = channel
            return idx

        if not self.channel_names:
            # Get channel names
            channel_names = self.client.exec(self.mixer_id, "inputs")
            channel_names = channel_names.split(" ")

            # Create channels objects if not yet available
            for name in channel_names:
                try:
                    self.channel_names.index(name)
                except ValueError:
                    idx = create_channel(name)
                    status = self.get_channel_status(idx)
                    msg = f"Channel {self.get_channel(name)} status: {status}"
                    self.logger.info(SU.pink(msg))

    @private
    def get_channel_number(self, channel_name: ChannelName) -> int:
        """
        Return the channel number for the given channel name.

        Args:
            channel (ChannelName): The channel

        Returns:
            (Integer): The channel number

        @private

        """
        self.refresh_channels()

        index = self.channel_names.index(channel_name)
        if index < 0:
            msg = f"There's no valid channel number for channel ID '{channel_name}'"
            self.logger.critical(SU.red(msg))
            return None
        return index

    @private
    def get_channel_status(self, channel_number: int) -> dict:
        """
        Retrieve the status of a channel identified by the channel number.

        Args:
            channel_number (int): The channel number

        Returns:
            (dict): Channel status dictionary

        @private

        """
        response = self.client.exec(self.mixer_id, "status", channel_number)
        return self.update_channel_status(channel_number, response)

    @private
    def update_channel_status(self, channel_number: int, status_string: str):
        """
        Update channel status.

        Args:
            channel_number (int): the index of the channel on the mixer
            status_string (dict): channel status fields as single string

        @private
        """
        status = {}
        pairs = status_string.split(" ")
        for pair in pairs:
            kv = pair.split("=")
            status[kv[0]] = kv[1]

        channel_name = self.channel_names[channel_number]
        channel: GenericChannel = self.channels.get(channel_name)
        channel.set_status(
            bool(status.get("ready")),
            bool(status.get("selected")),
            bool(status.get("single")),
            int(status.get("volume").split("%")[0]),
            float(status.get("remaining")),
        )
        return channel.get_status()

    @private
    def get_channel_volume(self, channel: GenericChannel) -> int:
        """
        Retrieve the current volume of the channel.

        Args:
            channel (GenericChannel): The channel

        Returns:
            (int): Volume between 0 and 100 or -1 in case of an error

        @private

        """
        status = self.get_channel_status(channel.get_index())
        return status.volume

    @private
    def set_channel_volume(self, channel: GenericChannel, volume: int):
        """
        Set volume of a channel.

        Args:
            channel (GenericChannel): The channel
            volume (int): Volume between 0 and 100

        @private

        """
        self.refresh_channels()

        playout_volume = str(int(volume) / 100)
        args = f"{channel.get_index()} {playout_volume}"
        message = self.client.exec(self.mixer_id, "volume", args)
        if not message.find(f"volume={volume}%"):
            msg = f"Error setting volume of channel {channel}: {message}"
            self.logger.error(SU.red(msg))
        else:
            self.update_channel_status(channel.index, message)
