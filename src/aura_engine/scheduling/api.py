#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-2020 - The Aura Engine Team.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
Collection of tools for AURA API handling.
"""

import logging
import queue
import threading

from aura_engine.base.api import SimpleCachedRestApi, SimpleRestApi
from aura_engine.base.config import AuraConfig
from aura_engine.base.lang import private
from aura_engine.base.utils import SimpleUtil as SU
from aura_engine.scheduling.utils import TimeslotFilter


class ApiFetcher(threading.Thread):
    """
    Get data from AURA REST APIs.

    Retrieve timeslots, playlists and playlist entries as JSON via the API endpoints of Steering
    and Tank.
    """

    API_TIMESLOT_ID = "id"
    API_PLID_PLAYLIST = "playlistId"
    API_PLID_SCHEDULE = "scheduleDefaultPlaylistId"
    API_PLID_SHOW = "showDefaultPlaylistId"
    MODEL_TIMESLOT_ID = "timeslot_id"
    MODEL_PLID_PLAYLIST = "playlist_id"
    MODEL_PLID_SCHEDULE = "default_schedule_playlist_id"
    MODEL_PLID_SHOW = "default_show_playlist_id"

    config = None
    logging = None
    queue = None
    record_mapping_timeslot = None
    has_already_fetched = False
    stop_event = None
    api = None

    # Config for API Endpoints
    url_api_timeslots = None
    url_api_playlist = None
    tank_headers = None

    def __init__(self):
        """
        Initialize the API Fetcher.
        """
        self.config = AuraConfig.instance.config
        self.logger = logging.getLogger("engine")
        cache_location = self.config.general.cache_dir
        self.api = SimpleCachedRestApi(SimpleRestApi(), cache_location)
        self.url_api_timeslots = self.config.api.steering.calendar
        self.url_api_playlist = self.config.api.tank.playlist
        self.queue = queue.Queue()
        self.stop_event = threading.Event()

        self.record_mapping_timeslot = {
            ApiFetcher.MODEL_TIMESLOT_ID: ApiFetcher.API_TIMESLOT_ID,
            ApiFetcher.MODEL_PLID_PLAYLIST: ApiFetcher.API_PLID_PLAYLIST,
            ApiFetcher.MODEL_PLID_SCHEDULE: ApiFetcher.API_PLID_SCHEDULE,
            ApiFetcher.MODEL_PLID_SHOW: ApiFetcher.API_PLID_SHOW,
        }

        tank_session = self.config.api.tank.session
        tank_secret = self.config.api.tank.secret
        self.tank_headers = {
            "Authorization": f"Bearer {tank_session}:{tank_secret}",
            "content-type": "application/json",
        }

        threading.Thread.__init__(self)

    def run(self):
        """
        Fetch timeslot data from the Steering and Tank API.
        """
        try:
            self.logger.debug("Fetching timeslots from STEERING")
            timeslots = self.get_current_timeslots()
            self.logger.debug("Fetching playlists from TANK")
            timeslots = self.add_playlists_to_timeslots(timeslots)

            # If nothing is fetched, return
            if not timeslots:
                self.queue.put("fetching_aborted fetched nothing")
                return None

            # Release the mutex
            self.queue.put(timeslots)
        except Exception as e:
            # Release the mutex
            self.queue.put("fetching_aborted " + str(e))

        # Terminate the thread
        return

    def get_fetched_data(self):
        """
        Retrieve fetched data from the queue.
        """
        return self.queue.get()

    def terminate(self):
        """
        Terminate the thread.
        """
        self.logger.info(SU.yellow("[ApiFetcher] Shutting down..."))
        self.stop_event.set()

    #
    # private
    #

    @private
    def get_current_timeslots(self) -> list:
        """
        Fetch timeslot data from Steering.

        This method also:
            - Filters invalid and unnecessary timeslots.
            - Remaps any API fields to there local modal representation.

        Returns:
            ([Timeslot]): A list of timeslots

        @private
        """
        timeslots = None
        url = self.url_api_timeslots
        self.logger.debug("Fetch timeslots from Steering API...")
        result = self.api.get(url)
        timeslots = result.json
        timeslots = self.filter_timeslots(timeslots)

        for t in timeslots:
            self.remap_record(t, self.record_mapping_timeslot)

        return timeslots

    @private
    def add_playlists_to_timeslots(self, timeslots) -> list:
        """
        Fetch and assign playlists to every timeslots.

        This method retrieve all playlist types per timeslot. If a playlist is already fetched,
        it is not fetched again but reused.

        Args:
            timeslots([Timeslot]): All timeslots fetched from Steering API

        Returns:
            ([Timeslot]): All timeslots with additional playlist records from Tank API

        @private
        """
        playlists = []

        for timeslot in timeslots:
            id_playlist = timeslot.get(ApiFetcher.MODEL_PLID_PLAYLIST)
            id_schedule = timeslot.get(ApiFetcher.MODEL_PLID_SCHEDULE)
            id_show = timeslot.get(ApiFetcher.MODEL_PLID_SHOW)

            timeslot["playlist"] = self.fetch_playlist(id_playlist, playlists)
            timeslot["default_schedule_playlist"] = self.fetch_playlist(id_schedule, playlists)
            timeslot["default_show_playlist"] = self.fetch_playlist(id_show, playlists)

        return timeslots

    @private
    def fetch_playlist(self, playlist_id: int, fetched_playlists: dict):
        """
        Fetch a playlist from Tank.

        If a playlist was already fetched within this round, it uses the existing one.

        Args:
            playlist_id (int): The ID of the playlist
            fetched_playlists (dict): Previously fetched playlists to avoid re-fetching

        Returns:
            (Playlist): Playlist for `playlist_id`

        @private
        """
        if not playlist_id:
            return None
        playlist_id = str(playlist_id)

        for playlist in fetched_playlists:
            if playlist["id"] == playlist_id:
                self.logger.debug(f"Playlist #{playlist_id} already fetched")
                return playlist

        playlist = None
        url = self.url_api_playlist.replace("${ID}", playlist_id)
        self.logger.debug("Fetch playlist from Tank API...")
        result = self.api.get(url, headers=self.tank_headers)
        playlist = result.json
        if playlist:
            fetched_playlists.append(playlist)
        return playlist

    @private
    def filter_timeslots(self, timeslots: list) -> list:
        """
        Remove all timeslots which are not relevant for further processing.

        Also, add transparent timeslot ID assignment for more expressive use.

        Args:
            timeslots (dict): The timeslots to be filtered
        @private
        """
        if not timeslots:
            return []

        count_before = len(timeslots)
        timeslots = TimeslotFilter.filter_24h(timeslots)
        timeslots = TimeslotFilter.filter_past(timeslots)
        timeslots = TimeslotFilter.filter_invalid(timeslots)
        count_after = len(timeslots)
        count_removed = count_before - count_after

        msg = f"Removed {count_removed} unnecessary timeslots, {count_after} left"
        self.logger.debug(msg)
        return timeslots

    @private
    def remap_record(self, record: dict, mapping: dict) -> dict:
        """
        Maps certain fields from an API record to the fields of the internal model.

        It changes the record in place, but also returns the updated record.

        Args:
            record (dict): The record to apply a re-mapping on
            mapping (dict): A dictionary with source (value) and target (key) mappings

        Returns:
            dict: The updated record

        @private
        """
        for key, value in mapping.items():
            if key != value:
                record[key] = record.pop(value)
        return record
