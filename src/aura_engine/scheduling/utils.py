#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-2020 - The Aura Engine Team.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
A collection of utilities used for scheduling.
"""

import logging
from datetime import datetime
from enum import Enum

from aura_engine.base.config import AuraConfig
from aura_engine.base.utils import SimpleUtil as SU


class EntryQueueState(Enum):
    """
    Types of playlist entry behaviours.
    """

    OKAY = "ok"
    CUT = "cut"
    OUT_OF_SCHEDULE = "oos"


class TimeslotFilter:
    """
    Filters timeslot dictionaries with various criteria.
    """

    @staticmethod
    def filter_invalid(timeslots: list) -> bool:
        """
        Remove invalid timeslots.

        Args:
            timeslots (list): The timeslot record

        Returns:
            (list): filtered timeslots
        """
        items = []
        for timeslot in timeslots:
            if "start" in timeslot and "end" in timeslot:
                items.append(timeslot)
        return items

    @staticmethod
    def filter_24h(timeslots: list):
        """
        Filter timeslot of the last 24 hours.

        Remove entries 24h in the future and 12 hours in the past.
        Note: This might influence resuming (in case of a crash)
        single timeslots which are longer than 12 hours long.
        Think e.g. live broadcasts.
        """
        items = []
        now = SU.timestamp()
        now_plus_24hours = now + (12 * 60 * 60)
        now_minus_12hours = now - (12 * 60 * 60)

        for s in timeslots:
            start_time = datetime.strptime(s["start"], "%Y-%m-%dT%H:%M:%S")
            start_time = SU.timestamp(start_time)

            if start_time <= now_plus_24hours and start_time >= now_minus_12hours:
                items.append(s)

        return items

    @staticmethod
    def filter_past(timeslots: list):
        """
        Filter old timeslots.

        Remove all timeslot dictionaries from the past, except the one which is
        currently playing.
        """
        items = []
        now = SU.timestamp()
        for s in timeslots:
            start_time = datetime.strptime(s["start"], "%Y-%m-%dT%H:%M:%S")
            start_time = SU.timestamp(start_time)
            end_time = datetime.strptime(s["end"], "%Y-%m-%dT%H:%M:%S")
            end_time = SU.timestamp(end_time)

            # Append all elements in the future
            if start_time >= now:
                items.append(s)
            # Append the one which is playing now
            elif start_time < now < end_time:
                items.append(s)

        return items


class M3UPlaylistProcessor:
    """
    Render a M3U Playlist as a engine compatible playlist dictionary.
    """

    config = None
    logging = None
    playlist_folder = None

    def __init__(self):
        self.config = AuraConfig.instance
        self.logger = logging.getLogger("engine")
        self.playlist_folder = self.config.abs_playlist_path()

    def spread(self, entries):
        """
        Convert a playlist with M3U entries and renders them as individual playlist entries.
        """
        if not self.playlist_folder:
            return entries

        result = []
        m3u_entries = None

        for entry in entries:
            # It's a M3U Playlist which needs to be spread
            if "uri" in entry and entry["uri"].startswith("playlist://"):
                playlist_name = entry["uri"].split("playlist://")[1]
                self.logger.info(f"Spreading entries of M3U playlist '{playlist_name}'")
                m3u_entries = self.read_m3u_file(self.playlist_folder + playlist_name)
                result += m3u_entries
            # It's an ordinary entry to be taken as it is
            else:
                result.append(entry)

        return result

    def read_m3u_file(self, source_file):
        """
        Read entries from an M3U file.
        """
        file = open(source_file, "r")
        lines = file.readlines()
        entries = []

        for i in range(0, len(lines)):
            if lines[i].startswith("#EXTINF:"):
                metadata = lines[i].split("#EXTINF:")[1].split(",")
                entry = {
                    "file": {
                        "metadata": {
                            "artist": metadata[1].split(" - ")[0],
                            "album": "",
                            "title": metadata[1].split(" - ")[1],
                        }
                    },
                    "duration": SU.seconds_to_nano(int(metadata[0])),
                    "uri": "file://" + lines[i + 1],
                }
                entries.append(entry)

        file.close()
        return entries


class TimetableRenderer:
    """
    Display current and next timeslots in ASCII for maintenance and debugging.
    """

    logger = None
    scheduler = None
    programme = None

    def __init__(self, scheduler):
        self.logger = logging.getLogger("engine")
        self.scheduler = scheduler
        self.programme = scheduler.get_timetable()

    def get_ascii_timeslots(self):
        """
        Create a printable version of the current programme.

        The output contains playlists and entries as per timeslot.

        Returns:
            (String):   An ASCII representation of the current and next timeslots

        """
        active_timeslot = self.programme.get_current_timeslot()

        s = "\n\n   SCHEDULED NOW:"
        s += (
            "\n┌───────────────────────────────────────────────────────────────────────"
            "───────────────────────────────"
        )
        if active_timeslot:
            planned_playlist = None
            if active_timeslot.playlist:
                planned_playlist = active_timeslot.playlist

            (playlist_type, resolved_playlist) = self.scheduler.resolve_playlist(active_timeslot)

            s += "\n│   Playing timeslot %s         " % active_timeslot
            if planned_playlist:
                if (
                    resolved_playlist
                    and resolved_playlist.playlist_id != planned_playlist.playlist_id
                ):
                    s += "\n│       └── Playlist %s         " % planned_playlist
                    s += "\n│       "
                    s += SU.red("↑↑↑ That's the originally planned playlist.") + (
                        "Instead playing the default playlist below ↓↓↓"
                    )

            if resolved_playlist:
                if not planned_playlist:
                    s += "\n│      "
                    s += SU.red(
                        f"No playlist assigned to timeslot. Instead playing the"
                        f" '{playlist_type.get('name')}' playlist below ↓↓↓"
                    )

                s += "\n│       └── Playlist %s         " % resolved_playlist

                active_entry = self.programme.get_current_entry()

                # Finished entries
                for entry in resolved_playlist.entries:
                    if active_entry == entry:
                        break
                    else:
                        s += self.build_entry_string("\n│         └── ", entry, True)

                # Entry currently being played
                if active_entry:
                    s += "\n│         └── Entry %s | %s         " % (
                        str(active_entry.entry_num + 1),
                        SU.green("PLAYING > " + str(active_entry)),
                    )

                    # Open entries for current playlist
                    rest_of_playlist = active_entry.get_next_entries(False)
                    entries = self.preprocess_entries(rest_of_playlist, False)
                    s += self.build_playlist_string(entries)

            else:
                s += "\n│       └── %s" % (
                    SU.red(
                        "No active playlist."
                        " There should be at least some fallback playlist running..."
                    )
                )
        else:
            s += "\n│   Nothing.           "
        s += (
            "\n└───────────────────────────────────────────────────────────────────────"
            "───────────────────────────────"
        )

        s += "\n   SCHEDULED NEXT:"
        s += (
            "\n┌───────────────────────────────────────────────────────────────────────"
            "───────────────────────────────"
        )

        next_timeslots = self.programme.get_next_timeslots()
        if not next_timeslots:
            s += "\n│   Nothing.         "
        else:
            for timeslot in next_timeslots:
                (playlist_type, resolved_playlist) = self.scheduler.resolve_playlist(timeslot)
                if resolved_playlist:
                    s += "\n│   Queued timeslot %s         " % timeslot
                    s += "\n│      └── Playlist %s         (Type: %s)" % (
                        resolved_playlist,
                        SU.cyan(str(playlist_type)),
                    )
                    if resolved_playlist.end_unix > timeslot.end_unix:
                        s += "\n│          %s!              " % (
                            SU.red(
                                "↑↑↑ Playlist #%s ends after timeslot #%s!"
                                % (resolved_playlist.playlist_id, timeslot.timeslot_id)
                            )
                        )

                    entries = self.preprocess_entries(resolved_playlist.entries, False)
                    s += self.build_playlist_string(entries)

        s += (
            "\n└───────────────────────────────────────────────────────────────────────"
            "───────────────────────────────\n\n"
        )
        return s

    def build_playlist_string(self, entries):
        """
        Return a stringified list of entries.
        """
        s = ""
        is_out_of_timeslot = False

        for entry in entries:
            if entry.queue_state == EntryQueueState.OUT_OF_SCHEDULE and not is_out_of_timeslot:
                s += "\n│             %s" % SU.red(
                    "↓↓↓ These entries won't be played because they are out of timeslot."
                )
                is_out_of_timeslot = True

            s += self.build_entry_string("\n│         └── ", entry, is_out_of_timeslot)

        return s

    def build_entry_string(self, prefix, entry, strike):
        """
        Return an stringified entry.
        """
        s = ""
        if entry.queue_state == EntryQueueState.CUT:
            s = "\n│             %s" % SU.red("↓↓↓ This entry is going to be cut.")

        if strike:
            entry_str = SU.strike(entry)
        else:
            entry_str = str(entry)

        entry_line = "%sEntry %s | %s" % (prefix, str(entry.entry_num + 1), entry_str)
        return s + entry_line

    def preprocess_entries(self, entries, cut_oos):
        """
        Analyse and mark entries which are going to be cut or excluded.

        Args:
            entries ([PlaylistEntry]): The playlist entries to be scheduled for playout
            cut_oos (Boolean): If `True` entries which are 'out of timeslot' are not returned

        Returns:
            ([PlaylistEntry]): The list of processed playlist entries

        """
        clean_entries = []

        for entry in entries:
            if entry.entry_start >= entry.playlist.timeslot.timeslot_end:
                msg = "Filtered entry (%s) after end-of timeslot (%s) ... SKIPPED" % (
                    entry,
                    entry.playlist.timeslot,
                )
                self.logger.debug(msg)
                entry.queue_state = EntryQueueState.OUT_OF_SCHEDULE
            elif entry.end_unix > entry.playlist.timeslot.end_unix:
                entry.queue_state = EntryQueueState.CUT
            else:
                entry.queue_state = EntryQueueState.OKAY

            if not entry.queue_state == EntryQueueState.OUT_OF_SCHEDULE or not cut_oos:
                clean_entries.append(entry)

        return clean_entries
