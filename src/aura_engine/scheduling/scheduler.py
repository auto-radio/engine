#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-2020 - The Aura Engine Team.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
The scheduler.
"""

import logging
import threading
import time

from aura_engine.base.config import AuraConfig
from aura_engine.base.utils import SimpleUtil as SU
from aura_engine.control import EngineExecutor
from aura_engine.core.channels import LoadSourceException
from aura_engine.engine import Engine, Player
from aura_engine.resources import ResourceClass, ResourceUtil
from aura_engine.scheduling.models import AuraDatabaseModel
from aura_engine.scheduling.programme import ProgrammeService
from aura_engine.scheduling.utils import TimetableRenderer


class NoActiveTimeslotException(Exception):
    """
    Exception thrown when there is no timeslot active.
    """

    pass


class AuraScheduler(threading.Thread):
    """
    The Scheduler.

    The programme scheduler has two main duties:
        - Retrieve data from Steering and Tank
        - Execute engine actions in an automated, timed fashion

    """

    config = None
    logger = None
    engine: Engine = None
    exit_event: threading.Event = None
    timetable_renderer: TimetableRenderer = None
    timetable: ProgrammeService = None
    message_timer = []
    is_initialized: bool = None
    is_engine_ready: bool = None

    def __init__(self, engine):
        self.config = AuraConfig.instance.config
        self.logger = logging.getLogger("engine")
        self.timetable = ProgrammeService()
        self.timetable_renderer = TimetableRenderer(self)
        self.engine = engine
        self.engine.scheduler = self
        self.is_soundsytem_init = False

        # Scheduler Initialization
        AuraDatabaseModel.init_database()
        self.is_initialized = False
        self.is_engine_ready = False

        # Init scheduling thread
        threading.Thread.__init__(self)
        self.exit_event = threading.Event()
        self.start()

    def run(self):
        """Execute the scheduler.

        Called when thread is started via `start()`. It does the following:

            1. `self.fetch_new_programme()` periodically from the API depending on the
                `fetching_frequency` defined in the engine configuration.
            2. Loads the latest programme from the database and sets the instance state
                `self.timetable` with current timeslots.
            3. Queues all timeslots of the programme, if the soundssystem is ready to accept
                commands.

        On every cycle the configuration file is reloaded, to allow modifications while running the
        engine.
        """
        while not self.exit_event.is_set():
            try:
                AuraConfig.instance.load_config()
                seconds_to_wait = self.config.scheduler.fetching_frequency
                msg = f"== start fetching new timeslots (every {seconds_to_wait} seconds) =="
                self.logger.info(SU.cyan(msg))

                # Load some stuff from the API in any case
                self.timetable.refresh()

                # Queue only when the engine is ready to play
                if self.is_initialized:
                    self.queue_programme()

            except Exception as e:
                msg = f"Unhandled error while fetching & scheduling new programme! ({str(e)})"
                self.logger.critical(SU.red(msg))
                # Keep on working anyway

            EngineExecutor.log_commands()
            self.exit_event.wait(seconds_to_wait)

    #
    #   EVENT HANDLERS
    #

    def on_ready(self):
        """Handle `on_ready` event.

        Called when the engine has finished booting and is ready to play.
        """
        self.is_initialized = True
        self.logger.info(self.timetable_renderer.get_ascii_timeslots())

        try:
            self.play_active_entry()
            self.queue_startup_entries()
        except NoActiveTimeslotException:
            # That's not good, but keep on working...
            pass

    def on_play(self, entry):
        """Handle `on_play` event.

        Event Handler which is called by the engine when some play command to Liquidsoap is issued.
        This does not indicate that Liquidsoap started playing actually, only that the command has
        been issued. To get the metadata update issued by Liquidsoap use `on_metadata` instead.

        This event is not issued when media is played by Liquidsoap in fallback scenarios.

        Args:
            entry (PlaylistEntry):

        """

        # Nothing to do atm
        pass

    #
    #   METHODS
    #

    def get_timetable(self):
        """
        Return the current timetable.
        """
        return self.timetable

    def play_active_entry(self):
        """Play currently active playlist entry, as per timetable.

        Plays the entry scheduled for the very current moment and forwards to the scheduled
        position in time. Usually called when the Engine boots.

        Raises:
            (NoActiveTimeslotException): If there's no timeslot in the timetable, within the
                scheduling window

        """
        sleep_offset = 10
        active_timeslot = self.timetable.get_current_timeslot()

        # Schedule any available fallback playlist
        if active_timeslot:
            # Create command timer to indicate the start of the timeslot
            TimeslotCommand(self.engine, active_timeslot)

        active_entry = self.timetable.get_current_entry()
        if not active_entry:
            raise NoActiveTimeslotException

        # In case of a file-system source, we need to fast-foward to the current marker as per
        # timeslot
        if active_entry.get_content_type() in ResourceClass.FILE.types:
            # Calculate the seconds we have to fast-forward
            now_unix = Engine.engine_time()
            seconds_to_roll = now_unix - active_entry.start_unix

            # If the roll exceeds the length of the current track,
            # there's no need to do anything - the scheduler takes care of the rest
            if (seconds_to_roll + sleep_offset) > active_entry.duration:
                msg = "The FFWD [>>] range exceeds the length of the entry. \
                    Drink some tea and wait for the sound of the next entry."
                self.logger.info(msg)
            else:
                # Preload and play active entry
                PlayCommand(self.engine, [active_entry])

                # Fast-forward to the scheduled position
                # TODO The roll should happen before the channel is active
                if seconds_to_roll > 0:
                    # Without plenty of timeout (10s) the roll doesn't work
                    # TODO Check if this is still the case with Liquidsoap 2
                    def async_preroll(seconds_to_roll):
                        seconds_to_roll += sleep_offset
                        time.sleep(sleep_offset)
                        self.logger.info("Going to fast-forward %s seconds" % seconds_to_roll)
                        rolled = self.engine.player.roll(active_entry.channel, seconds_to_roll)
                        if rolled:
                            self.logger.info("Pre-roll done")

                    thread = threading.Thread(target=async_preroll, args=(seconds_to_roll,))
                    thread.start()

        elif (
            active_entry.get_content_type() in ResourceClass.STREAM.types
            or active_entry.get_content_type() in ResourceClass.LIVE.types
        ):
            # Preload and play active entry
            PlayCommand(self.engine, [active_entry])

        else:
            self.logger.critical("Unknown Entry Type: %s" % active_entry)

    def get_active_playlist(self):
        """Retrieve the currently playing playlist.

        If there is no specific playlist for this timeslots, then any "default playlist" available
        on the timeslot or show level, is returned.

        Returns:
            (Dict, Playlist): A dictionary holding the playlist type and the resolved playlist

        """
        timeslot = self.timetable.get_current_timeslot()
        if timeslot:
            return self.resolve_playlist(timeslot)
        return (-1, None)

    def resolve_playlist(self, timeslot):
        """
        Retrieve the planned or default playlist for the given timeslot.

        Args:
            timeslot (Timeslot)

        Returns:
            (Dict, Playlist): A dictionary holding the playlist type and the resolved playlist

        """
        playlist_type, playlist = self.timetable.get_current_playlist(timeslot)
        return (playlist_type, playlist)

    def queue_programme(self):
        """
        Queue the current programme.

        Playlists of every timeslot are queued by creating timed commands to the sound-system to
        enable the individual tracks of playlists.
        """
        # Get a clean set of the timeslots within the scheduling window
        timeslots = self.timetable.get_next_timeslots()
        timeslots = self.filter_scheduling_window(timeslots)

        # Queue the timeslots, their playlists and entries
        if timeslots:
            for next_timeslot in timeslots:
                # Create command timer to indicate the start of the timeslot
                TimeslotCommand(self.engine, next_timeslot)

                playlist_type, playlist = self.timetable.get_current_playlist(next_timeslot)
                if playlist:
                    self.queue_playlist_entries(next_timeslot, playlist.entries, False, True)

        self.logger.info(SU.green("Finished queuing programme."))

    def queue_startup_entries(self):
        """
        Queue all entries after the one currently playing upon startup.

        Don't use this method in any other scenario, as it doesn't respect the scheduling window.
        """
        current_timeslot = self.timetable.get_current_timeslot()

        # Queue the (rest of the) currently playing timeslot upon startup
        if current_timeslot:
            playlist_type, current_playlist = self.timetable.get_current_playlist(current_timeslot)

            if current_playlist:
                active_entry = self.timetable.get_current_entry()
                if active_entry:
                    # Queue open entries for current playlist
                    rest_of_playlist = active_entry.get_next_entries(True)
                    self.queue_playlist_entries(current_timeslot, rest_of_playlist, False, True)

    def queue_playlist_entries(self, timeslot, entries, fade_in, fade_out):
        """Create player commands for all playlist items to be executed at the scheduled time.

        Since each scheduled playlist can consist of multiple entry types such as *file*, *live*,
        and *stream*, the play-out of the timeslot is actually a bit more complex.
        Before any playlist entries of the timeslot can be turned into sound, they need to be
        aggregated, queued and pre-loaded.

        1. First, all entries are aggregated when they hold filesystem entries.
            Given you have a playlist with 10 entries, the first 4 are consisting of files, the
            next two of a a stream and a live source. The last 4 are files again.
            These entries are now aggregated into 4 groups: one for the files, one for the stream,
            one for the live entry and another one for files.
            For each group a timer for executing the next step is created.

        2. Now, the playlist entries are going to be "pre-loaded".
            This means that filesystem entries are queued and pre-loaded and entries which are
            based on audio streams are buffered.
            This is required to allow a seamless play-out, when its time to do so (in the next
            stage).
            Due to their nature, playlist entries which hold live audio sources are not affected by
            this stage at all.

        Args:
            timeslot (Timeslot): The timeslot this entries belong to
            entries ([PlaylistEntry]): The playlist entries to be scheduled for playout
            fade_in (Boolean): Fade-in at the beginning of the set of entries
            fade_out (Boolean): Fade-out at the end of the set of entries

        Returns:
            (String): Formatted string to display playlist entries in log

        """
        entry_groups = []
        entry_groups.append([])
        previous_entry = None
        index = 0

        # Mark entries which start after the end of their timeslot or are cut
        # clean_entries = self.preprocess_entries(entries, True)

        # Group/aggregate all filesystem entries, allowing them to be queued at once
        for entry in entries:
            if previous_entry is None or (
                previous_entry.get_content_type() == entry.get_content_type()
                and entry.get_content_type() in ResourceClass.FILE.types
            ):
                entry_groups[index].append(entry)
            else:
                index += 1
                entry_groups.append([])
                entry_groups[index].append(entry)
            previous_entry = entry
        self.logger.info("Built %s entry group(s)" % len(entry_groups))

        # Timeslot function calls
        if len(entries) > 0 and len(entry_groups) > 0:
            for entries in entry_groups:
                if not isinstance(entries, list):
                    raise ValueError("Invalid Entry Group: %s" % str(entries))

                # Create command timers for each entry group
                PlayCommand(self.engine, entries)
        else:
            self.logger.warn(SU.red("Nothing to schedule ..."))

    def filter_scheduling_window(self, timeslots):
        """Filter only timeslots within the scheduling window.

        Ignore timeslots which are before the start of scheduling window
        (start of timeslot - `scheduling_window_start`) or after the end of the scheduling window
        (end of timeslot -`scheduling_window_end`).

        Before the scheduling window:
            - Timeslots can still be deleted in Steering and the playout will respect this

        During the scheduling window:
            - Timeslots and it's playlists are queued as timed commands

        After the scheduling window:
            - Such timeslots are ignored, because it doesn't make sense anymore to schedule them
                before the next timeslot starts

        """
        if not timeslots:
            return timeslots

        now_unix = Engine.engine_time()
        len_before = len(timeslots)
        window_start = self.config.scheduler.scheduling_window_start
        window_end = self.config.scheduler.scheduling_window_end
        timeslots = list(
            filter(
                lambda t: (t.start_unix - window_start) < now_unix
                and now_unix < (t.end_unix - window_end),
                timeslots,
            )
        )
        len_after = len(timeslots)
        self.logger.info(
            "For now, skipped %s future timeslot(s) which are out of the scheduling window"
            " (T¹-%ss to T²-%ss)",
            (len_before - len_after),
            window_start,
            window_end,
        )

        return timeslots

    def terminate(self):
        """
        Call this method when thread is stopped or a signal to terminate is received.
        """
        self.logger.info(SU.yellow("[Scheduler] Shutting down..."))
        self.timetable.terminate()
        self.exit_event.set()


#
#   EngineExecutor Commands
#


class TimeslotCommand(EngineExecutor):
    """
    Command for triggering start and end of timeslot events.
    """

    engine: Engine = None
    config: AuraConfig = None

    def __init__(self, engine: Engine, timeslot):
        """Initialize the timeslot command.

        Args:
            engine (Engine): The engine
            timeslot (Timeslot): The timeslot which is starting at this time
        """
        self.config = AuraConfig.instance.config
        self.engine = engine

        now_unix = SU.timestamp()
        fade_out_time = self.config.scheduler.fade_out_time
        start_fade_in = timeslot.start_unix - now_unix
        start_fade_out = timeslot.end_unix - now_unix - fade_out_time
        self.logger.debug(
            f"Fading in timeslot in {start_fade_in} seconds at {SU.fmt_time(timeslot.start_unix)}"
            f" | Timeslot: {timeslot}"
        )
        self.logger.debug(
            f"Fading out timeslot in {start_fade_out} seconds at"
            f" {SU.fmt_time(timeslot.end_unix - fade_out_time)} | Timeslot: {timeslot}"
        )
        # Initialize the "fade in" EngineExecuter and instantiate a connected child EngineExecuter
        # for "fade out" when the parent is ready
        timeslot_id = timeslot.timeslot_id
        super().__init__(
            f"TIMESLOT#{timeslot_id}",
            None,
            timeslot.start_unix,
            self.do_start_timeslot,
            timeslot,
        )
        EngineExecutor(
            f"TIMESLOT#{timeslot_id}",
            self,
            timeslot.end_unix - fade_out_time,
            self.do_end_timeslot,
            timeslot,
        )

    def do_start_timeslot(self, timeslot):
        """
        Indicate the start of the timeslot by sending a `on_timeslot_start` event.
        """
        self.logger.info(SU.cyan(f"=== on_timeslot_start('{timeslot}') ==="))
        self.engine.event_dispatcher.on_timeslot_start(timeslot)

    def do_end_timeslot(self, timeslot):
        """
        Indicate the start of the timeslot by sending a `on_timeslot_end` event.

        Also resetting the used channel.
        """
        self.logger.info(SU.cyan(f"=== on_timeslot_end('{timeslot}') ==="))
        self.engine.event_dispatcher.on_timeslot_end(timeslot)

        def has_direct_successor():
            timetable = self.engine.scheduler.timetable
            next_timeslot = timetable.get_next_timeslots(1)
            if next_timeslot:
                next_timeslot = next_timeslot[0]
                if next_timeslot.timeslot_start > timeslot.timeslot_end:
                    return False
                return True
            else:
                return False

        if not has_direct_successor():
            self.engine.player.stop(Player.TransitionType.FADE)


class PlayCommand(EngineExecutor):
    """
    Command for triggering timed preloading and playing as a child command.
    """

    engine: Engine = None
    config = None

    def __init__(self, engine: Engine, entries):
        """Initialize the play command.

        Args:
            engine (Engine): The engine
            entries (PlaylistEntry): One or more playlist entries to be started
        """
        self.config = AuraConfig.instance.config
        self.engine = engine

        preload_offset = self.config.scheduler.preload_offset
        start_preload = entries[0].start_unix - preload_offset
        start_play = entries[0].start_unix
        msg = (
            f"Preloading entries at {SU.fmt_time(start_preload)}, {preload_offset} seconds before"
            f" playing it at {SU.fmt_time(start_play)}"
        )
        self.logger.info(msg)
        # Initialize the "preload" EngineExecuter and attach a child `PlayCommand` to the
        # "on_ready" event handler
        timeslot_id = entries[0].playlist.timeslot.timeslot_id
        super().__init__(f"PRELOAD#{timeslot_id}", None, start_preload, self.do_preload, entries)
        EngineExecutor(f"PLAY#{timeslot_id}", self, start_play, self.do_play, entries)

    def do_preload(self, entries):
        """
        Preload the entries.
        """
        entries_str = ResourceUtil.get_entries_string(entries)
        try:
            if entries[0].get_content_type() in ResourceClass.FILE.types:
                self.logger.info(SU.cyan(f"=== preload_group('{entries_str}') ==="))
                self.engine.player.preload_group(entries)
            else:
                self.logger.info(SU.cyan(f"=== preload('{entries_str}') ==="))
                self.engine.player.preload(entries[0])
        except LoadSourceException as e:
            self.logger.critical(SU.red(f"Could not preload entries {entries_str}"), e)

        if entries[-1].status != Player.EntryPlayState.READY:
            msg = f"Entries didn't reach 'ready' state during preloading (Entries: {entries_str})"
            self.logger.warning(SU.red(msg))

    def do_play(self, entries):
        """
        Play the entries.
        """
        entries_str = ResourceUtil.get_entries_string(entries)
        self.logger.info(SU.cyan(f"=== play('{entries_str}') ==="))
        if entries[-1].status != Player.EntryPlayState.READY:
            # Let 'em play anyway ...
            msg = (
                f"PLAY: The entry/entries are not yet ready to be played"
                f" (Entries: {entries_str})"
            )
            self.logger.critical(SU.red(msg))
            while entries[-1].status != Player.EntryPlayState.READY:
                self.logger.info("PLAY: Wait a little bit until preloading is done ...")
                time.sleep(2)

        self.engine.player.play(entries[0], Player.TransitionType.FADE)
        self.logger.info(self.engine.scheduler.timetable_renderer.get_ascii_timeslots())
