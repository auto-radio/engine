#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-2020 - The Aura Engine Team.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
Services representing and dealing with the programme timetable.
"""

import logging
from datetime import datetime

from aura_engine.base.config import AuraConfig
from aura_engine.base.utils import SimpleUtil as SU
from aura_engine.engine import Engine
from aura_engine.scheduling.api import ApiFetcher
from aura_engine.scheduling.models import (
    Playlist,
    PlaylistEntry,
    PlaylistEntryMetaData,
    Timeslot,
)
from aura_engine.scheduling.utils import M3UPlaylistProcessor


class ProgrammeService:
    """
    The current programme of the calendar. The programme is a set of timeslots for the current day.
    """

    config = None
    logger = None
    timeslots = None
    api_fetcher = None
    last_successful_fetch = None
    programme_store = None

    def __init__(self):
        self.config = AuraConfig.instance.config
        self.logger = logging.getLogger("engine")
        self.programme_store = ProgrammeStore()

    def refresh(self):
        """
        Do fetch the latest programme from `ProgrammeStore` which stores it to the database.

        After that, the programme is in turn loaded from the database and stored in
        `self.timeslots`.
        """
        # Fetch programme from API endpoints
        self.logger.debug("Trying to fetch new programme from API endpoints...")

        # Create a fetching thread and wait until it is done
        self.api_fetcher = ApiFetcher()
        self.api_fetcher.start()
        response = self.api_fetcher.get_fetched_data()

        if response is None:
            msg = SU.red("Load timeslots from local cache, got nothing from API")
            self.logger.warning(msg)
        elif type(response) is list:
            if len(response) > 0:
                self.last_successful_fetch = datetime.now()
                self.timeslots = self.programme_store.store_timeslots(response)
                msg = f"Finished fetching current programme from API ({len(response)} timeslots)"
                self.logger.info(SU.green(msg))
            else:
                self.logger.critical("Programme fetched from Steering/Tank has no entries!")
        elif response.startswith("fetching_aborted"):
            msg = f"Load timeslots from local cache because API returned: {response[17:]}"
            self.logger.warning(SU.red(msg))
        else:
            msg = SU.red(f"Load timeslots from local cache because API responded with: {response}")
            self.logger.warning(msg)

        # Load latest programme from the database
        if not self.timeslots:
            self.timeslots = self.programme_store.load_timeslots()
            msg = f"Finished loading local programme ({len(self.timeslots)} timeslots)"
            self.logger.info(SU.green(msg))

    def get_current_entry(self):
        """
        Retrieve the current `PlaylistEntry` which should be played as per programme.

        Returns:
            (PlaylistEntry): The track which is (or should) currently being played

        """
        now_unix = Engine.engine_time()

        # Load programme if necessary
        if not self.timeslots:
            self.refresh()

        # Check for current timeslot
        current_timeslot = self.get_current_timeslot()
        if not current_timeslot:
            self.logger.warning(SU.red("There's no active timeslot"))
            return None

        # Check for scheduled playlist
        playlist_type, current_playlist = self.get_current_playlist(current_timeslot)
        if not current_playlist:
            msg = (
                "There's no (default) playlist assigned to the current timeslot."
                " Most likely a fallback will make things okay again."
            )
            self.logger.warning(SU.red(msg))
            return None

        # Iterate over playlist entries and store the current one
        current_entry = None
        for entry in current_playlist.entries:
            if entry.start_unix <= now_unix and now_unix <= entry.end_unix:
                current_entry = entry
                break

        if not current_entry:
            # Nothing playing ... fallback will kick-in
            msg = f"There's no current entry in playlist {current_playlist}"
            self.logger.warning(SU.red(msg))
            return None

        return current_entry

    def get_current_timeslot(self):
        """
        Retrieve the timeslot currently to be played.

        Returns:
            (Timeslot): The current timeslot

        """
        current_timeslot = None
        now_unix = Engine.engine_time()

        # Iterate over all timeslots and find the one to be played right now
        if self.timeslots:
            for timeslot in self.timeslots:
                if timeslot.start_unix <= now_unix and now_unix < timeslot.end_unix:
                    current_timeslot = timeslot
                    break

        return current_timeslot

    def get_current_playlist(self, timeslot):
        """
        Retrieve the playlist to be scheduled.

        If no specific playlist is assigned, the default schedule or show playlist is returned.
        This method does not respect any defined default playlists.

        Returns:
            (dict, Playlist): A dictionary holding the playlist type, the currently assigned
                playlist

        """
        playlist_type = Playlist.TYPE_TIMESLOT
        playlist = timeslot.playlist
        if not playlist:
            playlist_type = Playlist.TYPE_SCHEDULE
            playlist = timeslot.default_schedule_playlist
            if not playlist:
                playlist_type = Playlist.TYPE_SHOW
                playlist = timeslot.default_show_playlist

        return (playlist_type, playlist)

    def get_next_timeslots(self, max_count=0):
        """
        Retrieve the timeslots to be played after the current one.

        Args:
            max_count (Integer): Maximum of timeslots to return, if `0` all existing ones are
                returned

        Returns:
            ([Timeslot]): The next timeslots

        """
        now_unix = Engine.engine_time()
        next_timeslots = []

        if not self.timeslots:
            return []

        for timeslot in self.timeslots:
            if timeslot.start_unix > now_unix:
                if (len(next_timeslots) < max_count) or max_count == 0:
                    next_timeslots.append(timeslot)
                else:
                    break

        return next_timeslots

    def is_timeslot_in_window(self, timeslot):
        """
        Check if the timeslot is within the scheduling window.
        """
        now_unix = Engine.engine_time()
        window_start = self.config.scheduler.scheduling_window_start
        window_end = self.config.scheduler.scheduling_window_end

        if (
            timeslot.start_unix - window_start < now_unix
            and timeslot.start_unix - window_end > now_unix
        ):
            return True
        return False

    def terminate(self):
        """
        Call this when the thread is stopped or a signal to terminate is received.
        """
        self.logger.info(SU.yellow("[ProgrammeService] Shutting down..."))
        if self.api_fetcher:
            self.api_fetcher.terminate()


class ProgrammeStore:
    """
    The `ProgrammeStore` service retrieves all current schedules and related playlists.

    This includes audio files from the configured API endpoints and stores it in the local
    database.

    To perform the API queries it utilizes the ApiFetcher class.
    """

    config = None
    logger = None
    m3u_processor = None

    def __init__(self):
        self.config = AuraConfig.instance.config
        self.logger = logging.getLogger("engine")
        self.m3u_processor = M3UPlaylistProcessor()

    def load_timeslots(self):
        """
        Load the programme from the database.
        """
        timeslots = None
        try:
            timeslots = Timeslot.get_timeslots(datetime.now())
        except Exception as e:
            self.logger.critical(
                SU.red("Could not load programme from database. We are in big trouble my friend!"),
                e,
            )
        return timeslots

    def store_timeslots(self, fetched_timeslots):
        """
        Store the fetched timeslots to the database.
        """
        timeslots = []

        # Check if existing timeslots have been deleted
        self.update_deleted_timeslots(fetched_timeslots)

        # Process fetched timeslots
        for timeslot in fetched_timeslots:
            # Store the timeslot
            timeslot_db = self.store_timeslot(timeslot)
            timeslots.append(timeslot_db)

            # Store assigned playlists
            self.store_playlist(timeslot_db, timeslot_db.playlist_id, timeslot["playlist"])
            if timeslot_db.default_schedule_playlist_id:
                self.store_playlist(
                    timeslot_db,
                    timeslot_db.default_schedule_playlist_id,
                    timeslot["default_schedule_playlist"],
                )
            if timeslot_db.default_show_playlist_id:
                self.store_playlist(
                    timeslot_db,
                    timeslot_db.default_show_playlist_id,
                    timeslot["default_show_playlist"],
                )

        return timeslots

    def update_deleted_timeslots(self, fetched_timeslots):
        """
        Check if some timeslot has been deleted remotely, so delete it locally too.

        Attention: This method has no effect if only a single existing timeslot got
        deleted, i.e. zero timeslots got returned, because this could simply indicate
        an issue with the API/Steering, since that means no data got retrieved. This
        should not be a problem in real life scenarios though, as there's practically
        always something in the timetable.

        Args:
            fetched_timeslots ([dict]): List of timeslot dictionaries from the API

        """
        now_unix = SU.timestamp()
        scheduling_window_start = self.config.scheduler.scheduling_window_start

        local_timeslots = Timeslot.get_timeslots(datetime.now())
        for local_timeslot in local_timeslots:
            # Ignore timeslots which have already started
            if local_timeslot.start_unix > now_unix:
                # Filter the local timeslot from the fetched ones
                existing_remotely = list(
                    filter(
                        lambda new_timeslot: new_timeslot["timeslot_id"]
                        == local_timeslot.timeslot_id,
                        fetched_timeslots,
                    )
                )

                if not existing_remotely:
                    # Only allow deletion of timeslots which are deleted before the start of the
                    # scheduling window
                    if (local_timeslot.start_unix - scheduling_window_start) > now_unix:
                        msg = f"Timeslot #{local_timeslot.timeslot_id} has been deleted remotely,\
                            delete it locally too"
                        self.logger.info(msg)
                        local_timeslot.delete(commit=True)
                        msg = f"Deleted timeslot #{local_timeslot.timeslot_id} from locally"
                        self.logger.info(msg)
                    else:
                        msg = (
                            f"Timeslot #{local_timeslot.timeslot_id} has been deleted remotely."
                            f" Since the scheduling window has already started, it won't be"
                            f" deleted locally."
                        )
                        self.logger.error(SU.red(msg))

    def store_timeslot(self, timeslot):
        """
        Store the given timeslot to the database.

        Args:
            timeslot (Timeslot): The timeslot

        """
        timeslot_start = SU.to_datetime(timeslot["start"])
        timeslot_db = Timeslot.for_datetime(timeslot_start)
        havetoadd = False

        if not timeslot_db:
            self.logger.debug("no timeslot with given timeslot id in database => create new")
            timeslot_db = Timeslot()
            havetoadd = True

        timeslot_db.show_id = timeslot["showId"]
        timeslot_db.timeslot_id = timeslot["timeslot_id"]
        timeslot_db.timeslot_start = SU.to_datetime(timeslot["start"])
        timeslot_db.timeslot_end = SU.to_datetime(timeslot["end"])
        timeslot_db.show_name = timeslot["showName"]
        timeslot_db.show_hosts = timeslot["showHosts"]
        timeslot_db.repetition_of_id = timeslot["repetitionOfId"]
        timeslot_db.funding_category = timeslot["showFundingCategory"]
        timeslot_db.languages = timeslot["showLanguages"]
        timeslot_db.type = timeslot["showType"]
        timeslot_db.category = timeslot["showCategories"]
        timeslot_db.topic = timeslot["showTopics"]
        timeslot_db.musicfocus = timeslot["showMusicFocus"]
        timeslot_db.playlist_id = timeslot["playlist_id"]

        # Optional API properties
        if "default_schedule_playlistId" in timeslot:
            timeslot_db.default_schedule_playlist_id = timeslot["default_schedule_playlist_id"]
        if "default_show_playlist_id" in timeslot:
            timeslot_db.default_show_playlist_id = timeslot["default_show_playlist_id"]

        msg = f"Store/Update TIMESLOT havetoadd={havetoadd} - data: {timeslot}"
        self.logger.debug(SU.pink(msg))
        timeslot_db.store(add=havetoadd, commit=True)
        return timeslot_db

    def store_playlist(self, timeslot_db, playlist_id, fetched_playlist):
        """
        Store the Playlist to the database.
        """
        if not playlist_id or not fetched_playlist:
            self.logger.debug(f"Playlist ID#{playlist_id} is not available!")
            return

        playlist_db = Playlist.select_playlist_for_timeslot(
            timeslot_db.timeslot_start, playlist_id
        )
        havetoadd = False

        if not playlist_db:
            playlist_db = Playlist()
            havetoadd = True

        self.logger.debug(f"Storing playlist {playlist_id} for timeslot {timeslot_db}")
        playlist_db.playlist_id = playlist_id
        playlist_db.timeslot_start = timeslot_db.timeslot_start
        playlist_db.show_name = timeslot_db.show_name
        if "entries" in fetched_playlist:
            playlist_db.entry_count = len(fetched_playlist["entries"])
        else:
            playlist_db.entry_count = 0

        playlist_db.store(havetoadd, commit=True)

        if playlist_db.entry_count > 0:
            self.store_playlist_entries(timeslot_db, playlist_db, fetched_playlist)

        return playlist_db

    def store_playlist_entries(self, timeslot_db, playlist_db, fetched_playlist):
        """
        Store the playlist entries to the database.
        """
        entry_num = 0
        time_marker = playlist_db.start_unix
        entries = fetched_playlist["entries"]

        # "Hidden Functionality" to feed engine with M3U playlists via Tank's "Stream" playlist
        # entry type
        # See https://gitlab.servus.at/aura/engine/-/issues/53
        # In the future this is to be replaced by generic music pool feature.
        entries = self.m3u_processor.spread(entries)

        self.expand_entry_duration(timeslot_db, entries)
        self.delete_orphaned_entries(playlist_db, entries)

        for entry in entries:
            entry_db = PlaylistEntry.select_playlistentry_for_playlist(
                playlist_db.artificial_id, entry_num
            )
            havetoadd = False
            if not entry_db:
                entry_db = PlaylistEntry()
                havetoadd = True

            entry_db.entry_start = datetime.fromtimestamp(time_marker)
            entry_db.artificial_playlist_id = playlist_db.artificial_id
            entry_db.entry_num = entry_num
            entry_db.duration = SU.nano_to_seconds(entry["duration"])

            if "uri" in entry:
                entry_db.source = entry["uri"]
            if "filename" in entry:
                entry_db.source = entry["filename"]

            entry_db.store(havetoadd, commit=True)

            if "file" in entry:
                self.store_playlist_entry_metadata(entry_db, entry["file"]["metadata"])

            entry_num = entry_num + 1
            time_marker += entry_db.duration

    def delete_orphaned_entries(self, playlist_db, entries):
        """
        Delete all playlist entries which are beyond the current playlist's `entry_count`.

        Such entries might be existing due to a remotely changed playlist, which now has
        less entries than before.
        """
        new_last_idx = len(entries)
        existing_last_idx = PlaylistEntry.count_entries(playlist_db.artificial_id) - 1

        if existing_last_idx < new_last_idx:
            return

        for entry_num in range(new_last_idx, existing_last_idx + 1, 1):
            PlaylistEntry.delete_entry(playlist_db.artificial_id, entry_num)
            msg = f"Deleted playlist entry {playlist_db.artificial_id}:{entry_num}"
            self.logger.info(SU.yellow(msg))
            entry_num += 1

    def expand_entry_duration(self, timeslot_db, entries):
        """
        Expand entry to the timeslot gap left.

        If some playlist entry doesn't have a duration assigned, its duration is expanded to the
        remaining duration of the playlist (= timeslot duration minus playlist entries with
        duration).

        If there is more than one entry without duration, such entries are removed from the
        playlist.

        """
        total_seconds = (timeslot_db.timeslot_end - timeslot_db.timeslot_start).total_seconds()
        total_duration = SU.seconds_to_nano(total_seconds)
        actual_duration = 0
        missing_duration = []
        idx = 0

        for entry in entries:
            if "duration" not in entry:
                missing_duration.append(idx)
            else:
                actual_duration += entry["duration"]
            idx += 1

        if len(missing_duration) == 1:
            entries[missing_duration[0]]["duration"] = total_duration - actual_duration
            self.logger.info(f"Expanded duration of playlist entry #{missing_duration[0]}")

        elif len(missing_duration) > 1:
            # This case should actually never happen, as TANK doesn't allow more than one entry w/o
            # duration anymore
            for i in reversed(missing_duration[1:-1]):
                msg = f"Deleted Playlist Entry without duration: {entries[i]}"
                self.logger.error(SU.red(msg))
                del entries[i]

    def store_playlist_entry_metadata(self, entry_db, metadata):
        """
        Store the meta-data for a PlaylistEntry.
        """
        metadata_db = PlaylistEntryMetaData.select_metadata_for_entry(entry_db.artificial_id)
        havetoadd = False
        if not metadata_db:
            metadata_db = PlaylistEntryMetaData()
            havetoadd = True

        metadata_db.artificial_entry_id = entry_db.artificial_id

        if "artist" in metadata:
            metadata_db.artist = metadata["artist"]
        else:
            metadata_db.artist = ""

        if "album" in metadata:
            metadata_db.album = metadata["album"]
        else:
            metadata_db.album = ""

        if "title" in metadata:
            metadata_db.title = metadata["title"]
        else:
            metadata_db.title = ""

        metadata_db.store(havetoadd, commit=True)
