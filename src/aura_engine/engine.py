#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-2020 - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
The Engine.
"""

import json
import logging
import time
from contextlib import suppress
from enum import Enum

import tomli

from aura_engine.base.api import LiquidsoapUtil as LU
from aura_engine.base.config import AuraConfig
from aura_engine.base.lang import DotDict
from aura_engine.base.utils import SimpleUtil as SU
from aura_engine.core.channels import ChannelType, QueueChannel
from aura_engine.core.client import CoreConnectionError, PlayoutClient
from aura_engine.events import EngineEventDispatcher
from aura_engine.resources import (
    ResourceClass,
    ResourceMapping,
    ResourceType,
    ResourceUtil,
)


class Engine:
    """
    The Engine.
    """

    instance = None
    engine_time_offset = 0.0
    logger = None
    scheduler = None
    event_dispatcher = None
    client = None
    playout = None
    playout_state = None

    def __init__(self):
        """
        Initialize the engine.
        """
        if Engine.instance:
            raise Exception("Engine is already running!")
        Engine.instance = self
        self.logger = logging.getLogger("engine")
        self.config = AuraConfig.instance.config
        Engine.engine_time_offset = self.config.scheduler.audio.engine_latency_offset

    def start(self):
        """
        Start the engine.

        Called when the connection to the sound-system implementation has been established.
        """
        self.event_dispatcher = EngineEventDispatcher(self)
        self.event_dispatcher.on_initialized()

        while not self.is_connected():
            self.logger.info(SU.yellow("Waiting for Liquidsoap to be running ..."))
            time.sleep(2)
        self.logger.info(SU.green("Engine Core ------[ connected ]-------- Liquidsoap"))
        result = self.engine_update_config()
        if result == "OK":
            self.logger.info(SU.green("Playout config successfully updated"))
        else:
            self.logger.info(SU.red("Error while updating playout config"))

        self.event_dispatcher.on_boot()

        self.logger.info(EngineSplash.splash_screen(self.config))
        self.event_dispatcher.on_ready()

    #
    #   Basic Methods
    #

    def is_connected(self):
        """
        Check if there's a valid connection to Liquidsoap.
        """
        has_connection = False
        try:
            if not self.playout:
                self.playout = PlayoutClient.get_instance(self.event_dispatcher)
            self.playout.get_uptime()
            self.logger.info(SU.green("Initialize Player..."))
            self.player = Player(self.playout, self.event_dispatcher)
            has_connection = True
        except CoreConnectionError:
            self.logger.debug("Liquidsoap is not running so far")
        except Exception as e:
            self.logger.error("Cannot check if Liquidsoap is running. \nReason: {e.message}", e)

        return has_connection

    def update_playout_state(self):
        """
        Retrieve the state of all inputs and outputs.
        """
        state = self.playout.get_status()
        state = DotDict(LU.json_to_dict(state))

        def dispatch_fallback_event():
            timeslot = self.scheduler.timetable.get_current_timeslot()
            fallback_show_name = self.config.general.fallback_show_name
            self.event_dispatcher.on_fallback_active(timeslot, fallback_show_name)

        # Initialize state
        if not self.playout_state:
            if state.is_fallback:
                self.logger.info(SU.yellow("Set initial playout state to FALLBACK"))
                dispatch_fallback_event()
            else:
                self.logger.info(SU.green("Set initial playout state"))
        elif state and self.playout_state.is_fallback != state.is_fallback:
            if state.is_fallback:
                self.logger.info(SU.yellow("Playout turned into FALLBACK state"))
            else:
                self.logger.info(SU.green("Playout turned back into normal state"))

        self.playout_state = state
        return self.playout_state

    def get_playout_state(self):
        """
        Retrieve the state of Engine Core.
        """
        return self.playout_state

    def engine_update_config(self):
        """
        Update the config of playout with the current values.
        """
        playout_config = {
            "fallback_show_id": self.config.general.fallback_show_id,
            "fallback_show_name": self.config.general.fallback_show_name,
        }
        json_config = json.dumps(playout_config, ensure_ascii=False)
        response = self.playout.set_config(json_config)
        return response

    def init_version(self) -> dict:
        """
        Get the versions of Engine components and store in configuration.

        Returns:
            dict: {
                "control": Engine Control version
                "core": Engine Core version
                "liquidsoap": Liquidsoap version
            }

        """
        with open("pyproject.toml", mode="rb") as config:
            toml_file = tomli.load(config)
        ctrl_version = toml_file["tool"]["poetry"]["version"]
        versions = self.playout.get_version()
        versions = DotDict(json.loads(versions))
        versions.control = ctrl_version
        AuraConfig.instance.init_version(versions)

    @staticmethod
    def engine_time():
        """
        Get the engine perspective on time.

        Liquidsoap is slow in executing commands, therefore it's needed to schedule
        actions by (n) seconds in advance, as defined in the configuration file by
        the property `engine_latency_offset`. it's important to note that this method
        requires the class variable `EngineUtil.engine_time_offset` to be set on
        Engine initialization.

        Returns:
            (Integer):  the Unix epoch timestamp including the offset

        """
        return SU.timestamp() + Engine.engine_time_offset

    @staticmethod
    def get_instance():
        """
        Return the one and only engine.
        """
        return Engine.instance

    def terminate(self):
        """
        Terminate the engine and all related processes.
        """
        if self.scheduler:
            self.scheduler.terminate()
        # TODO terminate core connection


#
#   PLAYER
#


class Player:
    """
    Engine Player.
    """

    class TransitionType(str, Enum):
        """
        Types for instant and fade transitions.
        """

        INSTANT = "instant"
        FADE = "fade"

    class EntryPlayState(str, Enum):
        """
        Play-state of a playlist entry.
        """

        UNKNOWN = "unknown"
        LOADING = "loading"
        READY = "ready_to_play"
        PLAYING = "playing"
        FINISHED = "finished"

    config = None
    logger = None
    channels = None
    resource_map = None
    event_dispatcher = None
    mixer = None

    def __init__(self, playout, event_dispatcher):
        """
        Initialize the player.

        Args:
            playout (PlayoutClient): Client for connecting to Engine Core (Liquidsoap)
            event_dispatcher (EventDispather): Dispatcher for issuing events

        """
        self.config = AuraConfig.instance.config
        self.logger = logging.getLogger("engine")
        self.event_dispatcher = event_dispatcher
        self.resource_map = ResourceMapping()
        self.mixer = playout.get_mixer()

    def preload(self, entry):
        """
        Pre-Load the entry. This is required before the actual `play(..)` can happen.

        Be aware when using this method in order to queue a very short entry this may
        result in situations with incorrect timing. In this case bundle multiple short
        entries as one queue using `preload_playlist(self, entries)`.

        It's important to note, that his method is blocking until loading has finished. If this
        method is called asynchronously, the progress on the preloading state can be looked up in
        `entry.state`.

        Args:
            entry (Entry): An array holding filesystem entries

        """
        is_queue = entry.get_content_type() in ResourceClass.FILE.types
        metadata = ResourceUtil.generate_track_metadata(entry, not is_queue)
        entry.status = Player.EntryPlayState.LOADING
        self.logger.debug(SU.pink(f"Loading entry '{entry}'"))
        uri = None
        is_ready = False

        # LINE
        if entry.get_content_type() in ResourceClass.LIVE.types:
            channel_name = self.resource_map.live_channel_for_resource(entry.source)
            chosen_channel = self.mixer.get_channel(channel_name)
        else:
            channel_type = self.resource_map.type_for_resource(entry.get_content_type())
            chosen_channel = self.mixer.get_free_channel(channel_type)

            # QUEUE
            if is_queue:
                uri = ResourceUtil.source_to_filepath(entry.source, self.config)
            # STREAM
            elif entry.get_content_type() in ResourceClass.STREAM.types:
                uri = entry.source

        if not chosen_channel:
            self.logger.critical(SU.red("No channel for '{entry.source}' source found"))
        else:
            msg = f"Assign channel {chosen_channel} to entry"
            entry.channel = chosen_channel
            self.logger.info(SU.pink(msg))

        is_ready = entry.channel.load(uri, metadata=metadata)

        if is_ready:
            entry.status = Player.EntryPlayState.READY
        self.event_dispatcher.on_queue([entry])

    def preload_group(self, entries):
        """
        Preload multiple filesystem/queue entries at once.

        This call is required before the actual `play(..)` can happen. Due to their nature,
        non-filesystem entries cannot be queued using this method. In this case use
        `preload(self, entry)` instead. This method also allows queuing of very short files, such
        as jingles.

        It's important to note, that his method is blocking until loading has finished. If this
        method is called asynchronously, the progress on the preloading state can be looked up in
        `entry.state`.

        Args:
            entries ([Entry]): An array holding filesystem entries
            channel_type (ChannelType): The type of channel where it should be queued (optional)

        """
        free_channel = self.mixer.get_free_channel(ChannelType.QUEUE)
        channels = None

        # Validate entry type
        for entry in entries:
            if entry.get_content_type() != ResourceType.FILE:
                raise InvalidChannelException

        # Determine channel & queue entries

        for entry in entries:
            entry.channel = free_channel
            entry.status = Player.EntryPlayState.LOADING
            self.logger.debug(SU.pink(f"Loading entry '{entry}'"))

            # Choose and save the input channel
            metadata = ResourceUtil.generate_track_metadata(entry)
            file_path = ResourceUtil.source_to_filepath(entry.source, self.config)
            if entry.channel.load(file_path, metadata):
                entry.status = Player.EntryPlayState.READY

        self.event_dispatcher.on_queue(entries)
        return channels

    def play(self, entry, transition: TransitionType):
        """
        Play a new `Entry`.

        In case of a new timeslot (or some intended, immediate transition), a prepared channel is
        selected and transitions between old and new channel is performed.

        This method expects that the entry is pre-loaded using `preload(..)` or
        `preload_group(self, entries)` before being played. In case the pre-loading has happened
        for a group of entries, only the first entry of the group needs to be passed.

        Args:
            entry (PlaylistEntry): The audio source to be played
            transition (Player.TransitionType): The type of transition to use e.g. fade-in or
                instant volume level.
            queue (Boolean): If `True` the entry is queued if the `ChannelType` does allow so;
                otherwise a new channel of the same type is activated

        """
        with suppress(CoreConnectionError):
            # Stop any active channel
            self.stop(Player.TransitionType.FADE)

            # Start the new channel
            instant = not (transition == Player.TransitionType.FADE)
            entry.channel.fade_in(entry.volume, instant=instant)

            # Dispatch event
            self.event_dispatcher.on_play(entry)

    def stop(self, transition: TransitionType):
        """
        Stop the currently active channel either instantly or by fading out.

        Args:
            transition (Player.TransitionType): The type of transition to use e.g. fade-out
        """
        with suppress(CoreConnectionError):
            channel = self.mixer.get_active_channel()
            if not channel:
                self.logger.info(SU.pink("Nothing to stop, no active channel"))
                return

            instant = not (transition == Player.TransitionType.FADE)
            channel.fade_out(instant=instant)
            self.event_dispatcher.on_stop(channel)

    def roll(self, channel: QueueChannel, seconds: int) -> str:
        """
        Pre-roll the player of the given `ChannelType.QUEUE` channel by (n) seconds.

        Args:
            channel (ChannelName): The channel to push the file to
            seconds (Float): The seconds to roll

        Returns:
            (bool): True after successful rolling

        """
        return channel.roll(seconds)


class InvalidChannelException(Exception):
    """
    Exception thrown when the given channel is invalid.
    """

    pass


class EngineSplash:
    """Print the splash and version information on boot."""

    @staticmethod
    def splash_screen(config):
        """
        Print the engine logo and version info.
        """
        version = config.get("version_control")
        core_version = config.get("version_core")
        liq_version = config.get("version_liquidsoap")
        return f"""\n
             █████╗ ██╗   ██╗██████╗  █████╗     ███████╗███╗   ██╗ ██████╗ ██╗███╗   ██╗███████╗
            ██╔══██╗██║   ██║██╔══██╗██╔══██╗    ██╔════╝████╗  ██║██╔════╝ ██║████╗  ██║██╔════╝
            ███████║██║   ██║██████╔╝███████║    █████╗  ██╔██╗ ██║██║  ███╗██║██╔██╗ ██║█████╗
            ██╔══██║██║   ██║██╔══██╗██╔══██║    ██╔══╝  ██║╚██╗██║██║   ██║██║██║╚██╗██║██╔══╝
            ██║  ██║╚██████╔╝██║  ██║██║  ██║    ███████╗██║ ╚████║╚██████╔╝██║██║ ╚████║███████╗
            ╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═══╝ ╚═════╝ ╚═╝╚═╝  ╚═══╝╚══════╝
            control v{version}, core v{core_version}, liquidsoap v{liq_version} - Ready to play!
        \n"""
