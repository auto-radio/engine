#!/usr/bin/env python3
#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-2020 - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
Entrypoint to run the Engine.
"""

import logging
import signal
import sys
import threading

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from aura_engine.base.config import AuraConfig
from aura_engine.base.logger import AuraLogger
from aura_engine.engine import Engine
from aura_engine.scheduling.models import DB

config = AuraConfig.instance


def configure_flask():
    """Initialize Flask."""
    app.config["SQLALCHEMY_DATABASE_URI"] = config.get_database_uri()
    app.config["BABEL_DEFAULT_LOCALE"] = "de"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False


# FIXME Instantiate SQLAlchemy without the need for Flask
app = Flask(__name__)
configure_flask()
DB.set_flask_db(SQLAlchemy(app))


class EngineRunner:
    """
    EngineRunner is in charge of starting the engine.
    """

    logger = None
    config = None
    engine = None

    def __init__(self):
        """
        Constructor.
        """
        self.config = config.config
        AuraLogger(self.config)
        self.logger = logging.getLogger("engine")
        self.engine = Engine()

    def run(self):
        """
        Start Engine Core.
        """
        self.engine.start()

    def recreate_db(self):
        """
        Initialize the database and deletes any existing content.
        """
        from scheduling.models import AuraDatabaseModel

        AuraDatabaseModel.recreate_db()

    def exit_gracefully(self, signum, frame):
        """
        Shutdown of the engine. Also terminates the Liquidsoap thread.
        """
        for thread in threading.enumerate():
            self.logger.info(thread.name)

        if self.engine:
            self.engine.terminate()

        self.logger.info(f"Gracefully terminated Aura Engine! (signum:{signum}, frame:{frame})")
        sys.exit(0)


#
# START THE ENGINE
#


if __name__ == "__main__":
    runner = EngineRunner()
    signal.signal(signal.SIGINT, runner.exit_gracefully)
    signal.signal(signal.SIGTERM, runner.exit_gracefully)

    if len(sys.argv) >= 2:
        if "--recreate-database" in sys.argv:
            runner.recreate_db()
            sys.exit(0)

    runner.run()
