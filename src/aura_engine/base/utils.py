#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-2020 - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
A collection of all kinds of simplifications.
"""

import datetime
import time
from enum import Enum


class SimpleUtil:
    """
    A container class for simple utility methods.
    """

    @staticmethod
    def to_datetime(datetime_str: str):
        """
        Convert a timezone aware date-time string into `datetime`.
        """
        if datetime_str:
            return datetime.datetime.fromisoformat(datetime_str)
        return None

    @staticmethod
    def fmt_time(timestamp):
        """
        Format a UNIX timestamp to a String displaying time in the format '%H:%M:%S'.

        Args:
            (Integer) timestamp: Unix epoch

        Returns:
            (String): Displaying the time

        """
        return datetime.datetime.fromtimestamp(timestamp).strftime("%H:%M:%S")

    @staticmethod
    def round_seconds(dt: datetime) -> datetime:
        """
        Rounds date/time to the nearest second.

        Args:
            dt (datetime): the date/time object to round

        Returns:
            datetime: the rounded version
        """
        rounded_dt = dt + datetime.timedelta(seconds=0.5)
        return rounded_dt.replace(microsecond=0)

    @staticmethod
    def nano_to_seconds(nanoseconds):
        """
        Convert nano-seconds to seconds.

        Args:
            (Integer) nanoseconds

        Returns:
            (Float): seconds

        """
        return float(nanoseconds / 1000000000)

    @staticmethod
    def seconds_to_nano(seconds):
        """
        Convert seconds to nano-seconds.

        Args:
            (Integer) seconds

        Returns:
            (Float): nanoseconds

        """
        return int(seconds * 1000000000)

    @staticmethod
    def timestamp(date_and_time=None):
        """
        Transform the given `datetime` into a UNIX epoch timestamp.

        If no parameter is passed, the current timestamp is returned.

        Args:
            (Datetime) date_and_time: The date and time to transform.

        Returns:
            (Integer): timestamp in seconds.

        """
        if not date_and_time:
            date_and_time = datetime.datetime.now()
        return time.mktime(date_and_time.timetuple())

    @staticmethod
    def strike(text):
        """
        Create a strikethrough version of the given text.

        Args:
            (String) text: The text to strike.

        Returns:
            (String): the striked text.

        """
        result = ""
        for c in str(text):
            result += c + TerminalColors.STRIKE.value
        return result

    @staticmethod
    def bold(text):
        """
        Create a bold version of the given text.
        """
        return TerminalColors.BOLD.value + text + TerminalColors.ENDC.value

    @staticmethod
    def underline(text):
        """
        Create a underlined version of the given text.
        """
        return TerminalColors.UNDERLINE.value + text + TerminalColors.ENDC.value

    @staticmethod
    def blue(text):
        """
        Create a blue version of the given text.
        """
        return TerminalColors.BLUE.value + text + TerminalColors.ENDC.value

    @staticmethod
    def red(text):
        """
        Create a red version of the given text.
        """
        return TerminalColors.RED.value + text + TerminalColors.ENDC.value

    @staticmethod
    def pink(text):
        """
        Create a red version of the given text.
        """
        return TerminalColors.PINK.value + text + TerminalColors.ENDC.value

    @staticmethod
    def yellow(text):
        """
        Create a yellow version of the given text.
        """
        return TerminalColors.YELLOW.value + text + TerminalColors.ENDC.value

    @staticmethod
    def green(text):
        """
        Create a red version of the given text.
        """
        return TerminalColors.GREEN.value + text + TerminalColors.ENDC.value

    @staticmethod
    def cyan(text):
        """
        Create a cyan version of the given text.
        """
        return TerminalColors.CYAN.value + text + TerminalColors.ENDC.value


class TerminalColors(Enum):
    """
    Colors for formatting terminal output.
    """

    HEADER = "\033[95m"

    RED = "\033[31m"
    GREEN = "\033[32m"
    YELLOW = "\033[33m"
    BLUE = "\033[34m"
    PINK = "\033[35m"
    CYAN = "\033[36m"

    WARNING = "\033[31m"
    FAIL = "\033[41m"

    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"
    STRIKE = "\u0336"

    ENDC = "\033[0m"
