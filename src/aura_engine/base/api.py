#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-2020 - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
Simple API library.

~~~~~~~~~~~~~~~~~~~~~

Simple API is an HTTP library, for implicit error handling. This is meant for situations
where you are mostly interested in results and keep going independently if there is an
error or not.

By default the library is meant for digesting REST endpoints.

The responses are wrapped in a dictionary containing additial fields which are used at a
frequent basis:

```py
{
    "response" (Response): Actual Python response object
    "error" (String): In case an error occurred
    "exception" (Exception): In case an exception occurred
    "json" (Dictionary): In case of an json request this is the deserialized data
}
```

Basic GET usage:

   >>> from api import SimpleRestApi
   >>> r = api.get("https://aura.radio/foo.json")
   >>> r.response.status_code
   200
   >>> r.response.content
   <raw content>
   >>> r.json
   <unmarshalled json>

"""

import json
import logging
import os
from pathlib import Path
from urllib.parse import urlparse

import requests

from aura_engine.base.lang import DotDict
from aura_engine.base.utils import SimpleUtil as SU


class SimpleRestApi:
    """
    Simple wrapper on `requests` to deal with REST APIs.

    Use it for services which do not want to deal with exception
    handling but with results only.

    SimpleRestApi has implicit logging of invalid states at logs
    to the `engine` logger by default.
    """

    CONTENT_JSON = "application/json"
    default_headers = {"content-type": "application/json"}

    logger = None

    def __init__(self, logger_name="engine"):
        self.logger = logging.getLogger(logger_name)

    def exception_handler(func):
        """
        Decorate functions with `@exception_handler` to handle API exceptions in a simple way.

        Args:
            func (_type_): The decorated function

        """

        def handle_response(*args, **kwargs) -> dict:
            """Process the decorator response.

            Returns:
                dict: {
                    "response": requests.Response object
                    "error": String with error message
                    "exception": The actual exception
                }

            """
            fn = func.__name__.upper()
            msg_template = f"during {fn} at '{args[1]}'"
            error = None
            exc = None
            response_dict = None

            response = requests.Response()
            response.status_code = 400

            try:
                response = func(*args, **kwargs)
                if isinstance(response, dict):
                    response_dict = response
                    response = response_dict.response
                if int(response.status_code) >= 300:
                    reason = "-"
                    if hasattr(response, "reason"):
                        reason = response.reason
                    error = f"{response.status_code} | Error {msg_template}: {reason}"
                    args[0].logger.error(SU.red(error))
            except requests.exceptions.ConnectionError as e:
                exc = e
                error = f"Bad Request {msg_template}"
                args[0].logger.error(SU.red(error))
            except requests.exceptions.Timeout as e:
                exc = e
                error = f"Timeout {msg_template}"
                args[0].logger.error(SU.red(error))
            except requests.exceptions.RequestException as e:
                exc = e
                error = f"Unknown Error {msg_template}"
                args[0].logger.error(SU.red(error))
            except Exception as e:
                exc = e
                error = f"Unknown Exception {msg_template}"
                args[0].logger.error(SU.red(error), e)
            finally:
                result_dict = {"response": response, "error": error, "exception": exc}
                if response_dict:
                    result_dict = result_dict | response_dict
                return DotDict(result_dict)

        return handle_response

    def clean_dictionary(self, data: dict) -> dict:
        """
        Delete keys with the value `None` in a dictionary, recursively.

        Args:
            data (dict): The dictionary

        Returns:
            (dict): The cleaned dictionary

        """
        data = data.copy()
        for key, value in list(data.items()):
            if value is None:
                del data[key]
            elif isinstance(value, dict):
                SimpleRestApi.clean_dictionary(self, value)
        return data

    def serialize_json(self, data: dict, clean_data=True) -> str:
        """
        Marshall a dictionary as JSON String.

        Args:
            data (dict): Dictionary holding the data

        Returns:
            str: JSON String

        """
        if clean_data:
            data = self.clean_dictionary(data)
        json_data = json.dumps(data, indent=4, sort_keys=True, default=str)
        self.logger.info("Built JSON: " + json_data)
        return json_data

    def deserialize_json(self, response: str) -> str:
        """
        Unmarshall a JSON String to a dictionary.

        Args:
            response (dict): Response object

        Returns:
            dict: JSON as dictionary

        """
        json_data = None
        try:
            json_data = response.json()
        except Exception:
            self.logger.error(f"Invalid JSON: {response.content}")
            return None
        return json_data

    @exception_handler
    def get(self, url: str, headers: dict = None) -> requests.Response:
        """
        GET from an URL.

        Args:
            url (str): The URL of the request

        Returns:
            {
                "response": requests.Response,
                "error": str,
                "exception": Exception
            }

        """
        json_data = None
        if not headers:
            headers = SimpleRestApi.default_headers
        response = requests.get(url, headers=headers)
        if headers.get("content-type") == SimpleRestApi.CONTENT_JSON:
            json_data = self.deserialize_json(response)
        return DotDict({"response": response, "json": json_data})

    @exception_handler
    def post(self, url: str, data: dict, headers: dict = None):
        """
        POST to an URL.

        Args:
            url (str): The URL of the request
            data (dict): Data payload for request body
            headers (dict, optional): Optional headers, defaults to `SimpleRestApi.default_headers`

        Returns:
            {
                "response": requests.Response,
                "error": str,
                "exception": Exception
            }

        """
        if not headers:
            headers = SimpleRestApi.default_headers
        body: str = self.serialize_json(data)
        return requests.post(url, data=body, headers=headers)

    @exception_handler
    def put(self, url: str, data: dict, headers: dict = None) -> requests.Response:
        """
        PUT to an URL.

        Args:
            url (str): The URL of the request
            data (dict): Data payload for request body
            headers (dict, optional): Optional headers, defaults to `SimpleRestApi.default_headers`

        Returns:
            {
                "response": requests.Response,
                "error": str,
                "exception": Exception
            }

        """
        if not headers:
            headers = SimpleRestApi.default_headers
        body: str = self.serialize_json(data)
        return requests.put(url, data=body, headers=headers)


class SimpleCachedRestApi:
    """
    Wrapper to cache GET responses based on the simple REST API.

    It uses a network-first strategy:

      1. Query the requested API endpoint
      2. Store the result in a JSON file
      3. Return the result as a JSON object

    If the API endpoint is not available at step 1.) the cached JSON from the
    most recent, previously succeesful request is returned.

    """

    cache_location: str
    simple_api: SimpleRestApi

    logger = None

    def __init__(self, simple_api: SimpleRestApi, cache_location: str, logger_name="engine"):
        if cache_location[-1] != "/":
            cache_location += "/"
        cache_location += "api/"
        os.makedirs(cache_location, exist_ok=True)
        self.simple_api = simple_api
        self.cache_location = cache_location
        self.logger = logging.getLogger(logger_name)

    def get(self, url: str, headers: dict = None) -> requests.Response:
        """
        GET from an URL while also storing the result in the local cache.

        Args:
            url (str): The URL of the request

        Returns:
            {
                "response": requests.Response,
                "error": str,
                "exception": Exception
            }

        """
        filename = self.build_filename(url)
        cache_filepath = self.cache_location + filename
        result = self.simple_api.get(url, headers)

        if result and result.json and result.response.status_code == 200:
            with open(cache_filepath, "w") as file:
                json.dump(result.json, file)
                file.close()
        else:
            json_data = None
            try:
                file = open(cache_filepath, "r")
                json_data = json.load(file)
                file.close()
            except FileNotFoundError:
                pass

            if json_data:
                result = {
                    "response": DotDict({"status_code": 304, "error": "Not Modified"}),
                    "json": json_data,
                }
            else:
                result = {
                    "response": DotDict({"status_code": 404, "error": "Not Found in local cache"}),
                    "json": None,
                }

        return DotDict(result)

    def build_filename(self, url: str) -> str:
        """
        Build a valid file name based on the URI parts of an URL.

        Args:
            url (str): The URL to build the filename from

        Returns:
            str: File name representing an URL
        """
        parts = urlparse(url)
        dirs = parts.path.strip("/").split("/")
        return "-".join(dirs) + ".json"

    def prune_cache_dir(self):
        """
        Delete everything in the API cache directory.
        """
        [f.unlink() for f in Path(self.cache_location).iterdir() if f.is_file()]


class LiquidsoapUtil:
    """
    Utilities specific to Liquidsoap.
    """

    @staticmethod
    def json_to_dict(data: str) -> dict:
        """
        Convert a Liquidsoap JSON String to dictionary.
        """
        data = data.replace("+", " ")
        data = data.replace("-", " ")
        data = requests.utils.unquote(data)
        return json.loads(data)

    @staticmethod
    def annotate_uri(uri: str, annotations: dict) -> str:
        """
        Wrap the given URI with the passed annotation dictionary.
        """
        metadata = ""
        for k, v in annotations.items():
            metadata += f'{k}="{v}",'
        uri = f"annotate:{metadata[:-1]}:{uri}"
        return uri
