#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-2020 - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
Dealing with configuration data.
"""

import logging
import os
import os.path
import re
import sys
from pathlib import Path

import confuse
import yaml

template = {
    "general": {
        "socket_dir": str,
        "cache_dir": str,
        "fallback_show_name": str,
        "fallback_show_id": int,
    },
    "log": {
        "directory": str,
        "level": confuse.OneOf(["debug", "info", "warning", "error", "critical"]),
    },
    "monitoring": {
        "mail": {
            "host": str,
            "port": int,
            "user": str,
            "pwd": str,
            "from": str,
            "subject_prefix": str,
            "coordinator": {"enabled": bool, "mail": str},
            "admin": {"enabled": bool, "mail": str},
        },
        "heartbeat": {"host": str, "port": int, "frequency": int},
    },
    "api": {
        "steering": {"status": str, "calendar": str},
        "tank": {"session": str, "secret": str, "status": str, "playlist": str},
        "engine": {
            "number": int,
            "status": str,
            "store_playlog": str,
            "store_clock": str,
            "store_health": str,
        },
    },
    "scheduler": {
        "db": {
            "type": confuse.OneOf(["postgresql", "mysql", "sqlite"]),
            "name": str,
            "user": str,
            "pwd": str,
            "host": str,
            "charset": str,
        },
        "audio": {
            "source_folder": str,
            "source_extension": str,
            "playlist_folder": str,
            "engine_latency_offset": float,
        },
        "fetching_frequency": int,
        "scheduling_window_start": int,
        "scheduling_window_end": int,
        "preload_offset": int,
        "input_stream": {"retry_delay": int, "max_retries": int, "buffer": float},
        "fade_in_time": float,
        "fade_out_time": float,
    },
}


class AuraConfig:
    """
    Creates config by reading yaml file according to template above.
    """

    _instance = None
    yaml_path = ""
    confuse_config = None
    config = None  # points to a validated config (hopefully later)
    logger = None

    @classmethod
    @property
    def instance(cls):
        """Create and return singleton instance."""
        if cls._instance is None:
            cls._instance = AuraConfig()
        return cls._instance

    def __init__(self, yaml_path="/etc/aura/engine.yaml"):
        """
        Initialize the configuration, defaults to `/etc/aura/engine.yaml`.

        If this file doesn't exist it uses `./config/engine.yaml` from
        the project directory.

        Args:
            yaml_path(String): The path to the configuration file `engine.yaml`

        """
        self.logger = logging.getLogger("engine")
        config_file = Path(yaml_path)
        project_root = Path(__file__).parent.parent.parent.parent.absolute()

        if not config_file.is_file():
            yaml_path = f"{project_root}/config/engine.yaml"

        self.yaml_path = yaml_path
        print(f"Using configuration at: {yaml_path}")

        envar_matcher = re.compile(r"\$\{([^}^{]+)\}")

        def envar_constructor(loader, node):
            value = os.path.expandvars(node.value)
            # workaround not to parse numerics as strings
            try:
                value = int(value)
            except ValueError:
                pass
            try:
                value = float(value)
            except ValueError:
                pass
            return value

        envar_loader = yaml.SafeLoader
        envar_loader.add_implicit_resolver("!envar", envar_matcher, None)
        envar_loader.add_constructor("!envar", envar_constructor)

        self.confuse_config = confuse.Configuration("engine", loader=envar_loader)
        self.confuse_config.set_file(yaml_path)
        self.load_config()

        # custom overrides and defaults
        self.confuse_config["install_dir"].set(os.path.realpath(project_root))
        self.confuse_config["config_dir"].set(os.path.dirname(yaml_path))

        AuraConfig.instance = self

    def init_version(self, version: dict):
        """
        Read and set the component version from VERSION file in project root.
        """
        self.confuse_config["version_control"].set(version.get("control"))
        self.confuse_config["version_core"].set(version.get("core"))
        self.confuse_config["version_liquidsoap"].set(version.get("liquidsoap"))

    def get_database_uri(self):
        """
        Retrieve the database connection string.
        """
        db_cfg = self.config.scheduler.db
        db_name = db_cfg.name
        db_type = db_cfg.type
        if db_type in {"mysql", "postgresql"}:
            db_user = db_cfg.user
            db_pass = db_cfg.pwd
            db_host = db_cfg.host
            db_charset = db_cfg.charset
            if db_type == "mysql":
                return f"mysql://{db_user}:{db_pass}@{db_host}/{db_name}?charset={db_charset}"
            else:
                return (
                    f"postgresql+psycopg2://{db_user}:{db_pass}@{db_host}/{db_name}"
                    f"?client_encoding={db_charset}"
                )
        elif db_type == "sqlite":
            # "db_name" is expected to be either a relative or an absolute path to the sqlite file
            return f"sqlite:///{db_name}.db"
        else:
            return f"Error: invalid database type '{db_type}'"

    def load_config(self):
        """
        Set config defaults and load settings from file.
        """
        if not os.path.isfile(self.yaml_path):
            self.logger.critical(self.yaml_path + " not found  :(")
            sys.exit(1)

        self.config = self.confuse_config.get(template)

    def to_abs_path(self, path):
        """
        Transform any given (relative) path to an absolute paths.

        Starting at the project root.
        """
        if path.startswith("/"):
            return path
        else:
            return self.confuse_config["install_dir"].get() + "/" + path

    def abs_audio_store_path(self):
        """
        Return the absolute path to the audio store, based on the `audio_source_folder` setting.
        """
        return self.to_abs_path(self.config.scheduler.audio.source_folder)

    def abs_playlist_path(self):
        """
        Return the absolute path to the playlist folder.
        """
        return self.to_abs_path(self.config.scheduler.audio.playlist_folder)
