#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-2020 - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
Monitor the scheduling and playout status.

In case of irregularities:
    - Send heartbeats
    - Send health updates to Engine API
    - Log health updates
"""

import datetime
import json
import logging
import os
import platform
import threading
import urllib
from enum import Enum
from socket import AF_INET, SO_BROADCAST, SOCK_DGRAM, SOL_SOCKET, socket

import requests

from aura_engine.base.config import AuraConfig
from aura_engine.base.utils import SimpleUtil as SU


class EngineMalfunctionException(Exception):
    """
    Exception thrown when Engine turns into an status invalid for proper playout.
    """

    pass


class MonitorResponseCode(Enum):
    """
    Enumeration with status types.
    """

    OK = "OK"
    INVALID_STATE = "INVALID-STATE"


class AuraMonitor:
    """Monitoring of the Engine.

    It is in charge of:

    - Checking the overall status of all components and external API endpoints
    - Checking the vital parts, which are minimal requirements for running the engine
    - Sending a heartbeat to a defined server via socket

    """

    logger = None
    engine = None
    status = None
    already_invalid = None
    engine_id = None

    heartbeat_server = None
    heartbeat_port = None
    heartbeat_frequency = None
    heartbeat_socket = None
    heartbeat_running = None

    def __init__(self, engine):
        """
        Initialize Monitoring.
        """
        self.logger = logging.getLogger("engine")
        self.config = AuraConfig.instance.config
        self.engine = engine
        self.status = dict()
        self.status["engine"] = dict()
        self.status["lqs"] = dict()
        self.status["api"] = dict()
        self.status["api"]["steering"] = dict()
        self.status["api"]["tank"] = dict()
        self.status["api"]["engine"] = dict()
        self.already_invalid = False

        # Heartbeat settings
        self.heartbeat_running = False
        self.heartbeat_server = self.config.monitoring.heartbeat.host
        self.heartbeat_port = self.config.monitoring.heartbeat.port
        self.heartbeat_frequency = self.config.monitoring.heartbeat.frequency
        self.heartbeat_socket = socket(AF_INET, SOCK_DGRAM)

        self.engine_id = self.get_engine_id()

    #
    # EVENTS
    #

    def on_boot(self):
        """
        Call when the engine is booting.
        """
        # Start Monitoring
        is_valid = self.has_valid_status(False)
        status = self.get_status()
        self.logger.info("Status Monitor:\n%s" % json.dumps(status, indent=4))
        if not is_valid:
            self.logger.info("Engine Status: " + SU.red(status["engine"]["status"]))
            self.post_health(status, False)
            raise EngineMalfunctionException
        else:
            self.logger.info("Engine Status: " + SU.green("[OK]"))
            self.post_health(status, True)

    def on_sick(self, data):
        """
        Call when the engine is in some unhealthy state.
        """
        self.post_health(data, False)

    def on_resurrect(self, data):
        """
        Call when the engine turned healthy again after being sick.
        """
        self.post_health(data, True)

    #
    # PUBLIC METHODS
    #

    def get_status(self):
        """
        Retrieve the current monitoring status.
        """
        return self.status

    def has_valid_status(self, update_vitality_only):
        """
        Check if the current status is valid to run engine.

        By default it does not request new status information, rather using the cached one.
        To request new data either call `get_status()` before or use the `update_vital` parameter.

        Args:
            update_vitality_only (bool): Refreshes only the vital parts required for the heartbeat

        """
        is_valid = False

        if update_vitality_only:
            self.update_vitality_status()
        else:
            self.update_status()

        try:
            if (
                self.status["lqs"]["available"]
                and self.status["lqs"]["mixer"]["in_queue_0"]
                and self.status["audio_source"]["exists"]
            ):
                self.status["engine"]["status"] = MonitorResponseCode.OK.value
                is_valid = True
            else:
                self.status["engine"]["status"] = MonitorResponseCode.INVALID_STATE.value

        except Exception as e:
            self.logger.error("Exception while validating engine status: " + str(e))
            self.status["engine"]["status"] = MonitorResponseCode.INVALID_STATE.value

        return is_valid

    #
    # PRIVATE METHODS
    #

    def post_health(self, data, is_healthy):
        """
        Post unhealthy state info to Engine API.
        """
        body = dict()
        body["logTime"] = datetime.datetime.now()
        body["isHealthy"] = is_healthy
        body["details"] = json.dumps(data, default=str)
        json_data = json.dumps(body, default=str)
        url = self.config.api.engine.store_health
        url = url.replace("${ENGINE_NUMBER}", str(self.config.api.engine.number))
        headers = {"content-type": "application/json"}
        response = requests.Response()
        response.status_code = 404

        try:
            response = requests.post(url, data=json_data, headers=headers)
            if response.status_code == 204:
                self.logger.info(
                    "Successfully posted healthy=%s state to Engine API!" % is_healthy
                )
            else:
                msg = SU.red(
                    f"HTTP {response.status_code} | Error while pushing health state to Engine"
                    f" API: {response.json()}"
                )
                self.logger.error(msg)
        except requests.exceptions.ConnectionError:
            self.logger.error(SU.red(f"Bad Request when posting health-status to {url}"))
            return response
        except requests.exceptions.Timeout:
            self.logger.error(SU.red(f"Timeout when posting health-status to {url}"))
            return response
        except requests.exceptions.RequestException:
            self.logger.error(SU.red(f"Unknown Exception when posting health-status to {url}"))
            return response

    def update_status(self):
        """
        Request the current status of all components.
        """
        self.engine.init_version()
        ctrl_version = AuraConfig.instance.confuse_config["version_control"].get()
        core_version = AuraConfig.instance.confuse_config["version_core"].get()
        liq_version = AuraConfig.instance.confuse_config["version_liquidsoap"].get()
        self.status["engine"]["version"] = ctrl_version
        self.status["lqs"]["version"] = {"core": core_version, "liquidsoap": liq_version}
        self.status["lqs"]["outputs"] = self.engine.player.mixer.get_outputs()
        self.status["lqs"]["mixer"] = self.engine.player.mixer.get_inputs()
        self.status["api"]["steering"]["url"] = self.config.api.steering.status
        self.status["api"]["steering"]["available"] = self.validate_url_connection(
            self.config.api.steering.status
        )
        self.status["api"]["tank"]["url"] = self.config.api.tank.status
        self.status["api"]["tank"]["available"] = self.validate_url_connection(
            self.config.api.tank.status
        )
        self.status["api"]["tank"]["status"] = self.get_url_response(self.config.api.tank.status)
        self.status["api"]["engine"]["url"] = self.config.api.engine.status
        self.status["api"]["engine"]["available"] = self.validate_url_connection(
            self.config.api.engine.status
        )

        self.update_vitality_status()

    def update_vitality_status(self):
        """
        Refresh the vital status info which are required for the engine to survive.
        """
        self.status["lqs"]["status"] = self.engine.update_playout_state()
        self.status["lqs"]["available"] = self.status["lqs"]["status"] is not None
        self.status["audio_source"] = self.validate_directory(
            AuraConfig.instance.abs_audio_store_path()
        )

        # After first update start the Heartbeat Monitor
        if not self.heartbeat_running:
            self.heartbeat_running = True
            self.heartbeat()

    def heartbeat(self):
        """
        Send heartbeat tick.

        Every `heartbeat_frequency` seconds the current vitality status is checked. If it is okay,
        a heartbeat is sent to the configured server.
        """
        if self.has_valid_status(True):
            # Always check status, but only send heartbeat if wanted so
            if self.config.monitoring.heartbeat.host != "":
                self.heartbeat_socket.sendto(
                    str.encode("OK"), (self.heartbeat_server, self.heartbeat_port)
                )

            # Engine resurrected into normal state
            if self.already_invalid:
                self.already_invalid = False
                status = json.dumps(self.get_status())
                msg = SU.green("OK - Engine turned back into some healthy state!")
                self.logger.info(msg + "\n" + str(status))
                # Route call of event via event dispatcher to provide ability for additional hooks
                self.engine.event_dispatcher.on_resurrect(
                    {"engine_id": self.engine_id, "status": status}
                )
        else:
            # Engine turned into invalid state
            if not self.already_invalid:
                self.already_invalid = True
                status = json.dumps(self.get_status())
                self.logger.critical(
                    SU.red("Engine turned into some INVALID STATE!") + "\n" + str(status)
                )
                # Route call of event via event dispatcher to provide ability for additional hooks
                self.engine.event_dispatcher.on_sick(
                    {"engine_id": self.engine_id, "status": status}
                )

        heartbeat_frq = self.config.monitoring.heartbeat.frequency  # default: 1
        if int(heartbeat_frq or 0) < 1:
            heartbeat_frq = 1
        threading.Timer(heartbeat_frq, self.heartbeat).start()

    def validate_url_connection(self, url):
        """
        Check if connection to passed URL is successful.
        """
        try:
            request = urllib.request.Request(url)
            response = urllib.request.urlopen(request)
            response.read()
        except Exception:
            return False

        return True

    def validate_directory(self, dir_path):
        """
        Check if a given directory is existing and holds content.
        """
        status = dict()
        status["path"] = dir_path
        status["exists"] = os.path.exists(dir_path) and os.path.isdir(dir_path)
        status["has_content"] = False

        if status["exists"]:
            status["has_content"] = any([True for _ in os.scandir(dir_path)])
            if not status["has_content"]:
                msg = f"Directory '{dir_path}' has no contents!"
                self.logger.warning(SU.red(msg))
        else:
            msg = f"Directory '{dir_path}' doesn't exist!"
            self.logger.error(SU.red(msg))
        return status

    def get_url_response(self, url):
        """
        Fetch JSON data from the given URL.

        Args:
            url (String): The API endpoint to call

        Returns:
            (dict[]): A Python object representing the JSON structure

        """
        data = None

        try:
            request = urllib.request.Request(url)
            response = urllib.request.urlopen(request)
            data = response.read()
            return json.loads(data, strict=False)
        except (urllib.error.URLError, IOError, ValueError) as e:
            self.logger.error("Error while connecting to URL '%s' - %s" % (url, e))

        return MonitorResponseCode.INVALID_STATE.value

    def get_engine_id(self):
        """
        Retrieve a String identifier consisting of IP and Hostname.

        This is used to identify engine in status broadcasts.
        """
        host = platform.node()
        return "%s (%s)" % (self.get_ip(), host)

    def get_ip(self):
        """
        Return the IP of the Engine instance.
        """
        try:
            s = socket(AF_INET, SOCK_DGRAM)
            s.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
            s.connect(("<broadcast>", 0))
            return s.getsockname()[0]
        except OSError:
            self.logger.critical(SU.red("Error while accessing network via <broadcast>!"))
            return "<UNKNOWN NETWORK>"
