#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2022 - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
Update the clock information stored in Engine API.
"""

import logging
from datetime import datetime, timedelta

from aura_engine.base.api import SimpleRestApi
from aura_engine.base.config import AuraConfig
from aura_engine.base.lang import DotDict
from aura_engine.resources import ResourceUtil


class ClockInfoHandler:
    """
    Sends current studio clock information to the Engine API endpoint.
    """

    logger = None
    config = None
    api = None
    engine = None

    def __init__(self, engine):
        """
        Initialize.
        """
        self.logger = logging.getLogger("engine")
        self.config = AuraConfig.instance.config
        self.api = SimpleRestApi()
        self.engine = engine

        if not self.engine:
            self.logger.critical("ClockInfoHandler | Engine instance not available!")
            return

    def on_scheduler_cycle(self, programme):
        """Call when a new scheduling cycle has been started."""
        # TODO clock updates should also be triggered on scheduler update cycles
        # This will be part of the coming "default station playlist" logic
        pass

    def on_fallback_active(self, timeslot, fallback_name):
        """
        Call when a fallback is activated.
        """
        if timeslot:
            # Only update the current show to the fallback show,
            # in case no timeslot is scheduled
            return

        self.logger.info(f"Fallback '{fallback_name}' activated, clock update required")
        fallback_show_id = self.config.general.fallback_show_id
        fallback_show_name = self.config.general.fallback_show_name

        # Interpolate timeslot-less slot
        # TODO start time to be calculated based on previous timeslot (future station logic)
        upcoming_timeslots = self.engine.scheduler.timetable.get_next_timeslots()
        virtual_start_time = datetime.now()
        virtual_end_time = virtual_start_time + timedelta(hours=1)
        if len(upcoming_timeslots) > 0:
            virtual_end_time = upcoming_timeslots[0].timeslot_start

        fallback_timeslot = DotDict(
            {
                "timeslotId": -1,
                "timeslotStart": virtual_start_time,
                "timeslotEnd": virtual_end_time,
                "showId": fallback_show_id,
                "showName": fallback_show_name,
                "playlistId": -1,
                "playlistType": -1,
            }
        )
        self.post_clock_info(-1, None, fallback_timeslot, upcoming_timeslots)

    def on_play(self, _):
        """
        Event Handler which is called by the engine when some play command to Liquidsoap is issued.

        This does not indicate that Liquidsoap started playing actually, only that the command has
        been issued. Naturally this happens slightly before single tracks play audio.

        This event is not issued when media is played by Liquidsoap in fallback scenarios.

        Args:
            entry (PlaylistEntry):

        """
        active_playlist_type, active_playlist = self.engine.scheduler.get_active_playlist()
        active_type_id = active_playlist_type.get("id")
        active_timeslot = self.engine.scheduler.timetable.get_current_timeslot()
        upcoming_timeslots = self.engine.scheduler.timetable.get_next_timeslots()
        self.post_clock_info(active_type_id, active_playlist, active_timeslot, upcoming_timeslots)

    def post_clock_info(self, active_type, active_playlist, active_timeslot, upcoming_timeslots):
        """
        Post current information on timeslots and playlist to the Engine API clock endpoint.
        """
        if len(upcoming_timeslots) >= 2:
            upcoming_timeslots = upcoming_timeslots[0:2]

        built_upcoming = []
        for upcoming_timeslot in upcoming_timeslots:
            built_upcoming.append(self.build_timeslot(upcoming_timeslot))

        data = {
            "engineSource": self.config.api.engine.number,
            "currentTimeslot": self.build_timeslot(active_timeslot, active_type),
            "upcomingTimeslots": built_upcoming,
            "plannedPlaylist": self.build_playlist(active_playlist),
        }
        url = self.config.api.engine.store_clock
        self.logger.info(f"PUT clock info to '{url}': \n{data}")
        self.api.put(url, data=data)

    def build_timeslot(self, timeslot, playlist_type: int = 0) -> dict:
        """
        Transform a `Timeslot` object to a dictionary digestable by the clock API endpoint.

        Args:
            timeslot (Timeslot)
            playlist_type (int): Indicates if the playlist was assigned on
               (-1) fallback level (no playlist scheduled)
                (0) timeslot level (default)
                (1) schedule level
                (2) show level
                (3) station level

        Returns:
            timeslot (dict): JSON representation of the timeslot

        """
        if not timeslot:
            return

        return {
            "timeslotId": timeslot.timeslot_id,
            "timeslotStart": timeslot.timeslot_start,
            "timeslotEnd": timeslot.timeslot_end,
            "showId": timeslot.show_id,
            "showName": timeslot.show_name,
            "playlistId": timeslot.playlist_id,
            "playlistType": playlist_type,
        }

    def build_playlist(self, playlist) -> dict:
        """
        Transform a `Playlist` object to a dictionary digestable by the clock API endpoint.
        """
        if not playlist:
            return None

        entries = []
        for e in playlist.entries:
            content_class = ResourceUtil.get_content_class(e.get_content_type())
            entry = {
                "trackType": int(content_class.numeric),
                "trackStart": e.entry_start,
                "trackNum": e.entry_num,
                "trackDuration": e.duration,
            }
            if e.meta_data:
                entry |= {
                    "trackArtist": e.meta_data.artist,
                    "trackAlbum": e.meta_data.album,
                    "trackTitle": e.meta_data.title,
                }
            entries.append(entry)

        pls_dict = {"playlistId": playlist.playlist_id, "entries": entries}
        return pls_dict
