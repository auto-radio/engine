#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-now() - The Aura Engine Team.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
Remote-control the Engine with these services.
"""


import logging
import time
from datetime import datetime, timedelta
from threading import Lock, Thread, Timer

from aura_engine.base.utils import SimpleUtil as SU


class EngineExecutor(Timer):
    """
    Base class for timed or threaded execution of Engine commands.

    Primarily used for automations performed by the scheduler.
    """

    timer_store: dict = {}
    logger = logging.getLogger("engine")

    _lock = None
    direct_exec: bool = None
    is_aborted: bool = None
    parent_timer: Timer = None
    child_timer: Timer = None
    timer_id: str = None
    timer_type: str = None
    update_count: int = None
    func = None
    param = None
    diff = None
    dt = None

    def __init__(
        self,
        timer_type: str = "BASE",
        parent_timer: Timer = None,
        due_time=None,
        func=None,
        param=None,
    ):
        """
        Constructor.

        Args:
            timer_type (String): Prefix used for the `timer_id` to make it unique
            parent_timer (EngineExeuctor): Parent action which is a prerequisite for this timer
            due_time (Float): When timer should be executed.
                For values <= 0 execution happens immediately in a threaded way
            func (function): The function to be called
            param (object): Parameter passed to the function
        """
        self._lock = Lock()
        from aura_engine.engine import Engine

        now_unix = Engine.engine_time()

        # Init parent-child relation
        self.parent_timer = parent_timer
        if self.parent_timer:
            self.parent_timer.child_timer = self

        # Init meta data
        self.direct_exec = False
        self.is_aborted = False
        self.timer_type = timer_type
        self.update_count = 0

        diff = 0
        if due_time:
            diff = due_time - now_unix

        self.diff = diff
        self.dt = datetime.now() + timedelta(seconds=diff)
        dt_str = SU.round_seconds(self.dt).strftime("%Y-%m-%d_%H:%M:%S")
        self.timer_id = f"{timer_type}:{func.__name__}:{dt_str}"
        self.func = func
        self.param = param

        is_stored = self.update_store()
        if not is_stored:
            msg = f"Timer '{self.timer_id}' omitted cuz it already exists but is dead"
            self.logger.info(SU.yellow(msg))
            self.is_aborted = True
        else:
            if diff < 0:
                msg = f"Timer '{self.timer_id}' is due in the past. Executing immediately ..."
                self.logger.warning(SU.yellow(msg))
                self.exec_now()
            elif diff == 0:
                self.logger.debug(f"Timer '{self.timer_id}' to be executed immediately")
                self.exec_now()
            else:
                msg = f"Timer '{self.timer_id}' to be executed in default manner"
                self.logger.debug(msg)
                self.exec_timed()

    def wait_for_parent(self):
        """
        Child timers are dependent on their parents.

        So let's wait until parents are done with their stuff => finished execution.
        Checks the parent state to befinished every 0.2 seconds.

        @private
        """
        if self.parent_timer:
            while self.parent_timer.is_alive():
                msg = (
                    f"Timer '{self.timer_id}' is waiting for parent timer"
                    f" '{self.parent_timer.timer_id}' to finish"
                )
                self.logger.info(msg)
                time.sleep(0.2)

    def exec_now(self):
        """
        Immediate execution within a thread. It is not stored in the timer store.

        It also assigns the `timer_id` as the thread name.

        @private
        """
        self.direct_exec = True
        self.wait_for_parent()
        thread = Thread(name=self.timer_id, target=self.func, args=(self.param,))
        thread.start()

    def exec_timed(self):
        """
        Do timed execution in a thread.

        This method introduces a slight delay to ensure the thread is properly initialized before
        starting it.

        It also assigns the `timer_id` as the thread name.

        @private
        """

        def wrapper_func(param=None):
            self.wait_for_parent()
            if param:
                self.func(
                    param,
                )
            else:
                self.func()

        super().__init__(self.diff, wrapper_func, (self.param,))
        self._name = self.timer_id
        self.start()

    def update_store(self):
        """
        Add the instance to the store and cancels any previously existing commands.

        If a timer with the given ID is already existing but also already executed,
        then it is not added to the store. In such case the method returns `False`.

        Returns:
            (Boolean):  True if the timer has been added to the store. False if the
                        timer is already existing but dead.

        @private

        """
        with self._lock:
            existing_command = None
            if self.timer_id in EngineExecutor.timer_store:
                existing_command = EngineExecutor.timer_store[self.timer_id]

            if existing_command:
                # Check if existing timer has been executed already -> don't update
                if not existing_command.is_alive():
                    msg = f"Existing dead timer (ID: {self.timer_id}) - no update."
                    self.logger.debug(msg)
                    return False

                # Only update living timer when there's no completed parent
                elif self.parent_timer and not self.parent_timer.is_alive():
                    self.logger.debug("Parent finished, leave the existing child alone.")
                    return False

                # Parent and child are still waiting for execution -> update
                else:
                    msg = f"Cancelling existing timer with ID: {self.timer_id}"
                    self.logger.debug(msg)
                    existing_command.cancel()
                    self.update_count = existing_command.update_count + 1

            EngineExecutor.timer_store[self.timer_id] = self
            self.logger.debug(f"Stored command timer with ID: {self.timer_id}")
            return True

    def is_alive(self):
        """
        Return true if the command is still due to be executed.

        @private
        """
        if self.direct_exec:
            return False
        if self.is_aborted:
            return False
        return super().is_alive()

    def __str__(self):
        """
        Make a String representation of the timer.
        """
        return f"[{self.timer_id}] exec at {str(self.dt)} \
            (alive: {self.is_alive()}, updates: {self.update_count})"

    @staticmethod
    def remove_stale_timers():
        """
        Remove timers from store which have been executed and are older than 3 hours.
        """
        timers = EngineExecutor.timer_store.values()
        del_keys = []

        for timer in timers:
            if timer.dt < datetime.now() - timedelta(hours=3):
                if not timer.child_timer or (
                    timer.child_timer and not timer.child_timer.is_alive()
                ):
                    msg = f"Removing already executed timer with ID: {timer.timer_id}"
                    timer.logger.debug(msg)
                    del_keys.append(timer.timer_id)

        for timer_id in del_keys:
            del EngineExecutor.timer_store[timer_id]

    @staticmethod
    def command_history():
        """
        Return a list of recent active and inactive timers to the logger.
        """
        return EngineExecutor.timer_store.values()

    @staticmethod
    def log_commands():
        """
        Print a list of recent active and inactive timers to the logger.
        """
        msg = SU.blue("\n [ ENGINE COMMAND QUEUE ]\n")
        EngineExecutor.remove_stale_timers()
        timers = EngineExecutor.timer_store.values()

        if not timers:
            msg += "\nNone available!\n"
        else:
            for timer in timers:
                if not timer.parent_timer:
                    line = f"      =>   {str(timer)}\n"
                    if timer.is_alive():
                        line = SU.green(line)
                    msg += line
                if timer.child_timer:
                    line = f"            =>   {str(timer.child_timer)}\n"
                    if timer.child_timer.is_alive():
                        line = SU.green(line)
                    msg += line

        EngineExecutor.logger.info(msg + "\n")
