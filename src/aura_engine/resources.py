#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-2020 - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
Utilities and mappings for media resources.
"""

from enum import Enum

from aura_engine.core.channels import ChannelName, ChannelType


class ResourceType(Enum):
    """
    Media content types.
    """

    FILE = "file:"
    STREAM_HTTP = "http"
    LINE = "line:"
    PLAYLIST = "playlist:"
    POOL = "pool:"


class ResourceMapping:
    """
    Wires source types with channel-types.
    """

    resource_mapping = None

    def __init__(self):
        """
        Initialize the resource mapping.
        """

        self.resource_mapping = {
            ResourceType.FILE: ChannelType.QUEUE,
            ResourceType.STREAM_HTTP: ChannelType.HTTP,
            ResourceType.LINE: ChannelType.LIVE,
            ResourceType.PLAYLIST: ChannelType.QUEUE,
            ResourceType.POOL: ChannelType.QUEUE,
        }

    def type_for_resource(self, resource_type: ResourceType) -> ChannelType:
        """
        Retrieve a `ChannelType` for the given `ResourceType`.

        Only default mappings can be evaluated. Custom variations
        like fallback channels are not respected.
        """
        return self.resource_mapping.get(resource_type)

    def live_channel_for_resource(channel: str):
        """
        Return the channel enum for a given live channel string from Tank.
        """
        if not channel:
            return None
        channel_name = "in_line_" + channel.split("line://")[1]

        for cn in ChannelName:
            if cn.value == channel_name:
                return cn

        return None


class ResourceClass(Enum):
    """
    Media content classes.
    """

    FILE = {"id": "fs", "numeric": 0, "types": [ResourceType.FILE]}
    STREAM = {
        "id": "fs",
        "numeric": 1,
        "types": [ResourceType.STREAM_HTTP],
    }
    LIVE = {"id": "http", "numeric": 2, "types": [ResourceType.LINE]}
    PLAYLIST = {
        "id": "playlist",
        "numeric": 3,
        "types": [ResourceType.PLAYLIST, ResourceType.POOL],
    }

    @property
    def types(self):
        """Retrieve allowed types of the resource class."""
        return self.value["types"]

    @property
    def numeric(self):
        """Retrieve a numeric representation of the resource class."""
        return self.value["numeric"]

    def __str__(self):
        return str(self.value["id"])


class ResourceUtil(Enum):
    """
    Utilities for different resource types.
    """

    @staticmethod
    def get_content_type(uri):
        """
        Return the content type identified by the passed URI.

        Args:
            uri (String): The URI of the source

        Returns:
            (ResourceType)

        """
        if uri.startswith(ResourceType.STREAM_HTTP.value):
            return ResourceType.STREAM_HTTP
        if uri.startswith(ResourceType.POOL.value):
            return ResourceType.POOL
        if uri.startswith(ResourceType.FILE.value):
            return ResourceType.FILE
        if uri.startswith(ResourceType.LINE.value):
            return ResourceType.LINE

    @staticmethod
    def get_content_class(content_type):
        """
        Return the content class identified by the passed type.

        Args:
            content_type (ContentType): The content type

        Returns:
            (ResourceType)

        """
        if content_type in ResourceClass.FILE.types:
            return ResourceClass.FILE
        if content_type in ResourceClass.STREAM.types:
            return ResourceClass.STREAM
        if content_type in ResourceClass.LIVE.types:
            return ResourceClass.LIVE
        if content_type in ResourceClass.PLAYLIST.types:
            return ResourceClass.PLAYLIST

    @staticmethod
    def generate_m3u_file(target_file, audio_store_path, entries, entry_extension):
        """
        Write a M3U file based on the given playlist object.

        Args:
            target_file (File): The M3U playlist to write
            audio_store_path (String): Folder containing the source files
            entries (PlaylistEntry): Entries of the playlist
            entry_extension (String): The file extension of the playlist entries

        """
        file = open(target_file, "w")
        fb = ["#EXTM3U"]

        for entry in entries:
            if ResourceUtil.get_content_type(entry.source) == ResourceType.FILE:
                path = ResourceUtil.source_to_filepath(
                    audio_store_path, entry.source, entry_extension
                )
                fb.append(
                    f"#EXTINF:{entry.duration},{entry.meta_data.artist} - {entry.meta_data.title}"
                )
                fb.append(path)

        file.writelines(fb)
        file.close()

    @staticmethod
    def source_to_filepath(source_uri, config):
        """
        Create path from URI and extension.

        Convert a file-system URI starting with "file://" to an actual, absolute path to the file,
        appending the extension as provided in "source_extension".

        If the path starts with an "/", it indicates that it is already an absolute path including
        a valid extension.

        Args:
            source_uri (String): The URI of the file
            config (AuraConfig): The configuration

        Returns:
            path (String): Absolute file path

        """
        path = source_uri[7:]
        if path.startswith("/"):
            return path
        else:
            base_dir = config.abs_audio_store_path()
            extension = config.get("audio_source_extension")
            return base_dir + "/" + path + extension

    @staticmethod
    def get_entries_string(entries):
        """
        Return a list of entries as String for logging purposes.
        """
        s = ""
        if isinstance(entries, list):
            for entry in entries:
                s += str(entry)
                if entry != entries[-1]:
                    s += ", "
        else:
            s = str(entries)
        return s

    @staticmethod
    def generate_track_metadata(
        entry,
        assign_track_start: bool = False,
    ) -> dict:
        """
        Generate Liquidsoap track metadata based on an entry.
        """
        content_type = ResourceUtil.get_content_type(entry.source)
        content_class = ResourceUtil.get_content_class(content_type)

        annotations = {
            "show_name": str(entry.playlist.timeslot.show_name),
            "show_id": int(entry.playlist.timeslot.show_id),
            "timeslot_id": int(entry.playlist.timeslot.timeslot_id),
            "playlist_id": int(entry.playlist.playlist_id),
            "playlist_item": str(float(entry.entry_num)),
            "track_type": int(content_class.numeric),
            "track_start": "",
            "track_duration": -1,
            "track_title": "",
            "track_album": "",
            "track_artist": "",
        }

        if assign_track_start:
            # Convert to current Liquidsoap date/time format
            annotations["track_start"] = entry.entry_start.strftime("%Y/%m/%d %H:%M:%S")
        if entry.duration and entry.duration > 0:
            annotations["track_duration"] = entry.duration
        if entry.meta_data:
            annotations["track_title"] = str(entry.meta_data.title)
            annotations["track_album"] = str(entry.meta_data.album)
            annotations["track_artist"] = str(entry.meta_data.artist)

        return annotations
