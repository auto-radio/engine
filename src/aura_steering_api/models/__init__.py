""" Contains all the data models used in inputs/outputs """

from .timeslot import Timeslot

__all__ = ("Timeslot",)
