import datetime
from typing import Any, Dict, List, Type, TypeVar, Union

import attr
from dateutil.parser import isoparse

from ..types import UNSET, Unset

T = TypeVar("T", bound="Timeslot")


@attr.s(auto_attribs=True)
class Timeslot:
    """
    Attributes:
        id (Union[Unset, int]):
        start (Union[Unset, datetime.datetime]):
        end (Union[Unset, datetime.datetime]):
        title (Union[Unset, int]):
        schedule_id (Union[Unset, int]):
        repetition_of_id (Union[Unset, None, int]):
        playlist_id (Union[Unset, None, int]):
        schedule_default_playlist_id (Union[Unset, None, int]):
        show_default_playlist_id (Union[Unset, None, int]):
        show_id (Union[Unset, int]):
        show_name (Union[Unset, str]):
        show_hosts (Union[Unset, str]):
        show_type (Union[Unset, None, str]):
        show_categories (Union[Unset, None, str]):
        show_topics (Union[Unset, None, str]):
        show_music_focus (Union[Unset, None, str]):
        show_languages (Union[Unset, None, str]):
        show_funding_category (Union[Unset, None, str]):
    """

    id: Union[Unset, int] = UNSET
    start: Union[Unset, datetime.datetime] = UNSET
    end: Union[Unset, datetime.datetime] = UNSET
    title: Union[Unset, int] = UNSET
    schedule_id: Union[Unset, int] = UNSET
    repetition_of_id: Union[Unset, None, int] = UNSET
    playlist_id: Union[Unset, None, int] = UNSET
    schedule_default_playlist_id: Union[Unset, None, int] = UNSET
    show_default_playlist_id: Union[Unset, None, int] = UNSET
    show_id: Union[Unset, int] = UNSET
    show_name: Union[Unset, str] = UNSET
    show_hosts: Union[Unset, str] = UNSET
    show_type: Union[Unset, None, str] = UNSET
    show_categories: Union[Unset, None, str] = UNSET
    show_topics: Union[Unset, None, str] = UNSET
    show_music_focus: Union[Unset, None, str] = UNSET
    show_languages: Union[Unset, None, str] = UNSET
    show_funding_category: Union[Unset, None, str] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id
        start: Union[Unset, str] = UNSET
        if not isinstance(self.start, Unset):
            start = self.start.isoformat()

        end: Union[Unset, str] = UNSET
        if not isinstance(self.end, Unset):
            end = self.end.isoformat()

        title = self.title
        schedule_id = self.schedule_id
        repetition_of_id = self.repetition_of_id
        playlist_id = self.playlist_id
        schedule_default_playlist_id = self.schedule_default_playlist_id
        show_default_playlist_id = self.show_default_playlist_id
        show_id = self.show_id
        show_name = self.show_name
        show_hosts = self.show_hosts
        show_type = self.show_type
        show_categories = self.show_categories
        show_topics = self.show_topics
        show_music_focus = self.show_music_focus
        show_languages = self.show_languages
        show_funding_category = self.show_funding_category

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if start is not UNSET:
            field_dict["start"] = start
        if end is not UNSET:
            field_dict["end"] = end
        if title is not UNSET:
            field_dict["title"] = title
        if schedule_id is not UNSET:
            field_dict["scheduleId"] = schedule_id
        if repetition_of_id is not UNSET:
            field_dict["repetitionOfId"] = repetition_of_id
        if playlist_id is not UNSET:
            field_dict["playlistId"] = playlist_id
        if schedule_default_playlist_id is not UNSET:
            field_dict["scheduleDefaultPlaylistId"] = schedule_default_playlist_id
        if show_default_playlist_id is not UNSET:
            field_dict["showDefaultPlaylistId"] = show_default_playlist_id
        if show_id is not UNSET:
            field_dict["showId"] = show_id
        if show_name is not UNSET:
            field_dict["showName"] = show_name
        if show_hosts is not UNSET:
            field_dict["showHosts"] = show_hosts
        if show_type is not UNSET:
            field_dict["showType"] = show_type
        if show_categories is not UNSET:
            field_dict["showCategories"] = show_categories
        if show_topics is not UNSET:
            field_dict["showTopics"] = show_topics
        if show_music_focus is not UNSET:
            field_dict["showMusicFocus"] = show_music_focus
        if show_languages is not UNSET:
            field_dict["showLanguages"] = show_languages
        if show_funding_category is not UNSET:
            field_dict["showFundingCategory"] = show_funding_category

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id", UNSET)

        _start = d.pop("start", UNSET)
        start: Union[Unset, datetime.datetime]
        if isinstance(_start, Unset):
            start = UNSET
        else:
            start = isoparse(_start)

        _end = d.pop("end", UNSET)
        end: Union[Unset, datetime.datetime]
        if isinstance(_end, Unset):
            end = UNSET
        else:
            end = isoparse(_end)

        title = d.pop("title", UNSET)

        schedule_id = d.pop("scheduleId", UNSET)

        repetition_of_id = d.pop("repetitionOfId", UNSET)

        playlist_id = d.pop("playlistId", UNSET)

        schedule_default_playlist_id = d.pop("scheduleDefaultPlaylistId", UNSET)

        show_default_playlist_id = d.pop("showDefaultPlaylistId", UNSET)

        show_id = d.pop("showId", UNSET)

        show_name = d.pop("showName", UNSET)

        show_hosts = d.pop("showHosts", UNSET)

        show_type = d.pop("showType", UNSET)

        show_categories = d.pop("showCategories", UNSET)

        show_topics = d.pop("showTopics", UNSET)

        show_music_focus = d.pop("showMusicFocus", UNSET)

        show_languages = d.pop("showLanguages", UNSET)

        show_funding_category = d.pop("showFundingCategory", UNSET)

        timeslot = cls(
            id=id,
            start=start,
            end=end,
            title=title,
            schedule_id=schedule_id,
            repetition_of_id=repetition_of_id,
            playlist_id=playlist_id,
            schedule_default_playlist_id=schedule_default_playlist_id,
            show_default_playlist_id=show_default_playlist_id,
            show_id=show_id,
            show_name=show_name,
            show_hosts=show_hosts,
            show_type=show_type,
            show_categories=show_categories,
            show_topics=show_topics,
            show_music_focus=show_music_focus,
            show_languages=show_languages,
            show_funding_category=show_funding_category,
        )

        timeslot.additional_properties = d
        return timeslot

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
