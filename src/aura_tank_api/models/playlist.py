from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

import attr

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.playlist_entry import PlaylistEntry


T = TypeVar("T", bound="Playlist")


@attr.s(auto_attribs=True)
class Playlist:
    """
    Attributes:
        created (Union[Unset, str]):
        description (Union[Unset, str]):
        entries (Union[Unset, List['PlaylistEntry']]):
        id (Union[Unset, int]):
        playout_mode (Union[Unset, str]):
        show_name (Union[Unset, str]):
        updated (Union[Unset, str]):
    """

    created: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    entries: Union[Unset, List["PlaylistEntry"]] = UNSET
    id: Union[Unset, int] = UNSET
    playout_mode: Union[Unset, str] = UNSET
    show_name: Union[Unset, str] = UNSET
    updated: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        created = self.created
        description = self.description
        entries: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.entries, Unset):
            entries = []
            for entries_item_data in self.entries:
                entries_item = entries_item_data.to_dict()

                entries.append(entries_item)

        id = self.id
        playout_mode = self.playout_mode
        show_name = self.show_name
        updated = self.updated

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if created is not UNSET:
            field_dict["created"] = created
        if description is not UNSET:
            field_dict["description"] = description
        if entries is not UNSET:
            field_dict["entries"] = entries
        if id is not UNSET:
            field_dict["id"] = id
        if playout_mode is not UNSET:
            field_dict["playoutMode"] = playout_mode
        if show_name is not UNSET:
            field_dict["showName"] = show_name
        if updated is not UNSET:
            field_dict["updated"] = updated

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.playlist_entry import PlaylistEntry

        d = src_dict.copy()
        created = d.pop("created", UNSET)

        description = d.pop("description", UNSET)

        entries = []
        _entries = d.pop("entries", UNSET)
        for entries_item_data in _entries or []:
            entries_item = PlaylistEntry.from_dict(entries_item_data)

            entries.append(entries_item)

        id = d.pop("id", UNSET)

        playout_mode = d.pop("playoutMode", UNSET)

        show_name = d.pop("showName", UNSET)

        updated = d.pop("updated", UNSET)

        playlist = cls(
            created=created,
            description=description,
            entries=entries,
            id=id,
            playout_mode=playout_mode,
            show_name=show_name,
            updated=updated,
        )

        playlist.additional_properties = d
        return playlist

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
