from typing import Any, Dict, List, Type, TypeVar, Union

import attr

from ..types import UNSET, Unset

T = TypeVar("T", bound="FileMetadata")


@attr.s(auto_attribs=True)
class FileMetadata:
    """
    Attributes:
        album (Union[Unset, str]):
        artist (Union[Unset, str]): actually a full-text index would be nice here...
        isrc (Union[Unset, str]):
        organization (Union[Unset, str]):
        title (Union[Unset, str]):
    """

    album: Union[Unset, str] = UNSET
    artist: Union[Unset, str] = UNSET
    isrc: Union[Unset, str] = UNSET
    organization: Union[Unset, str] = UNSET
    title: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        album = self.album
        artist = self.artist
        isrc = self.isrc
        organization = self.organization
        title = self.title

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if album is not UNSET:
            field_dict["album"] = album
        if artist is not UNSET:
            field_dict["artist"] = artist
        if isrc is not UNSET:
            field_dict["isrc"] = isrc
        if organization is not UNSET:
            field_dict["organization"] = organization
        if title is not UNSET:
            field_dict["title"] = title

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        album = d.pop("album", UNSET)

        artist = d.pop("artist", UNSET)

        isrc = d.pop("isrc", UNSET)

        organization = d.pop("organization", UNSET)

        title = d.pop("title", UNSET)

        file_metadata = cls(
            album=album,
            artist=artist,
            isrc=isrc,
            organization=organization,
            title=title,
        )

        file_metadata.additional_properties = d
        return file_metadata

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
