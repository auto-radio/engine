from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

import attr

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.file_metadata import FileMetadata
    from ..models.file_source import FileSource


T = TypeVar("T", bound="File")


@attr.s(auto_attribs=True)
class File:
    """
    Attributes:
        created (Union[Unset, str]):
        duration (Union[Unset, int]):
        id (Union[Unset, int]):
        metadata (Union[Unset, FileMetadata]):
        show_name (Union[Unset, str]):
        size (Union[Unset, int]):
        source (Union[Unset, FileSource]):
        updated (Union[Unset, str]):
    """

    created: Union[Unset, str] = UNSET
    duration: Union[Unset, int] = UNSET
    id: Union[Unset, int] = UNSET
    metadata: Union[Unset, "FileMetadata"] = UNSET
    show_name: Union[Unset, str] = UNSET
    size: Union[Unset, int] = UNSET
    source: Union[Unset, "FileSource"] = UNSET
    updated: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        created = self.created
        duration = self.duration
        id = self.id
        metadata: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.metadata, Unset):
            metadata = self.metadata.to_dict()

        show_name = self.show_name
        size = self.size
        source: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.source, Unset):
            source = self.source.to_dict()

        updated = self.updated

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if created is not UNSET:
            field_dict["created"] = created
        if duration is not UNSET:
            field_dict["duration"] = duration
        if id is not UNSET:
            field_dict["id"] = id
        if metadata is not UNSET:
            field_dict["metadata"] = metadata
        if show_name is not UNSET:
            field_dict["showName"] = show_name
        if size is not UNSET:
            field_dict["size"] = size
        if source is not UNSET:
            field_dict["source"] = source
        if updated is not UNSET:
            field_dict["updated"] = updated

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.file_metadata import FileMetadata
        from ..models.file_source import FileSource

        d = src_dict.copy()
        created = d.pop("created", UNSET)

        duration = d.pop("duration", UNSET)

        id = d.pop("id", UNSET)

        _metadata = d.pop("metadata", UNSET)
        metadata: Union[Unset, FileMetadata]
        if isinstance(_metadata, Unset):
            metadata = UNSET
        else:
            metadata = FileMetadata.from_dict(_metadata)

        show_name = d.pop("showName", UNSET)

        size = d.pop("size", UNSET)

        _source = d.pop("source", UNSET)
        source: Union[Unset, FileSource]
        if isinstance(_source, Unset):
            source = UNSET
        else:
            source = FileSource.from_dict(_source)

        updated = d.pop("updated", UNSET)

        file = cls(
            created=created,
            duration=duration,
            id=id,
            metadata=metadata,
            show_name=show_name,
            size=size,
            source=source,
            updated=updated,
        )

        file.additional_properties = d
        return file

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
