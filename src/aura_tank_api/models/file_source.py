from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

import attr

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.import_ import Import


T = TypeVar("T", bound="FileSource")


@attr.s(auto_attribs=True)
class FileSource:
    """
    Attributes:
        hash_ (Union[Unset, str]):
        import_ (Union[Unset, Import]):
        uri (Union[Unset, str]):
    """

    hash_: Union[Unset, str] = UNSET
    import_: Union[Unset, "Import"] = UNSET
    uri: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        hash_ = self.hash_
        import_: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.import_, Unset):
            import_ = self.import_.to_dict()

        uri = self.uri

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if hash_ is not UNSET:
            field_dict["hash"] = hash_
        if import_ is not UNSET:
            field_dict["import"] = import_
        if uri is not UNSET:
            field_dict["uri"] = uri

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.import_ import Import

        d = src_dict.copy()
        hash_ = d.pop("hash", UNSET)

        _import_ = d.pop("import", UNSET)
        import_: Union[Unset, Import]
        if isinstance(_import_, Unset):
            import_ = UNSET
        else:
            import_ = Import.from_dict(_import_)

        uri = d.pop("uri", UNSET)

        file_source = cls(
            hash_=hash_,
            import_=import_,
            uri=uri,
        )

        file_source.additional_properties = d
        return file_source

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
