# Aura Engine

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](code_of_conduct.md) [![Latest Release](https://gitlab.servus.at/aura/engine/-/badges/release.svg)](https://gitlab.servus.at/aura/engine/-/releases) [![pipeline status](https://gitlab.servus.at/aura/engine/badges/main/pipeline.svg)](https://gitlab.servus.at/aura/engine/-/commits/main) [![coverage report](https://gitlab.servus.at/aura/engine/badges/main/coverage.svg?job=run_test_cases)](https://gitlab.servus.at/aura/engine/-/commits/main)

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-engine.png" width="120" align="right" />

AURA Engine is a scheduling service to control the playout of _Engine Core_.

This documentation is primarily meant for developers. For using the AURA Community Radio Suite
check out the documentation at [docs.aura.radio](https://docs.aura.radio/)

To learn more about Automated Radio go to [aura.radio](https://aura.radio).

## Overview

The following diagram gives an simplified overview on the architecture.

```mermaid
flowchart LR

    api-- GET calendar -->steering[Steering API]
    api-- GET playlist -->tank[Tank API]
    ea[Engine API]
    events-- on_boot\non_sick\non_resurrect -->monitor[Monitor]
    events-- on_play\non_fallback_active -->clock[Clock]
    clock-- POST clock_data -->ea
    monitor-- PING heartbeat -->monserver[Monitoring Server]
    monitor-- POST health_data -->ea
    mixer-- UNIX SOCKET -->engine_core

    subgraph external
        ea
        monserver
        engine_core
        steering
        tank
        db[(PostgreSQL DB)]
    end
    subgraph engine
        engine_instance[Engine]-.-oplayer & events & scheduler

        scheduler-- control\ncommand\ntimer -->player[Player]

        player-- control action -->mixer
        events[Event Dispatcher *]

        subgraph scheduling
            api[Fetch & Cache API]
            timetable<-->api
            timetable<-->db
            scheduler<-->timetable[Timetable Management ***]
            scheduler-- cycle -->scheduler
        end
        subgraph core[core]
            mixer[Mixer]
        end
        subgraph plugins
            clock
            monitor
        end
    end

    style external fill:#FFF
    style ea fill:#FFF,stroke:#333,stroke-width:1px
    style steering fill:#FFF,stroke:#333,stroke-width:1px
    style tank fill:#FFF,stroke:#333,stroke-width:1px
    style monserver fill:#FFF,stroke:#333,stroke-width:1px
    style engine_core fill:#FFF,stroke:#333,stroke-width:1px
```

- **Event Dispatcher**: Events are issued and consumed in multiple locations of Engine. The diagram shows only a few ones, in order to support the overview.
- **Timetable Management**: Currently a database is used for caching and high-availability purposes. This is [subject to changes](https://gitlab.servus.at/aura/engine/-/issues/100) in subsequent releases.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Operating system: Debian 11, Ubuntu 20.04 or newer
- `git`, `make`
- [Docker](https://www.docker.com/), optional if you want to run in a container
- [Python 3.9+](https://www.python.org/downloads/release/python-380/)
- [Poetry](https://python-poetry.org/)
- [PostgreSQL 13+](https://www.postgresql.org/)

Ensure that you have also dependencies such as `steering`, `tank`, `dashboard`, `engine-core`, and `engine-api` up and running.

## Preparation

### Initialize environment

Install dependencies and prepare config file:

```shell
make init.app
```

This also creates a default configuration file at `config/engine.yaml`.

For development install with:

```shell
make init.dev
```

Note, if some configuration exists under `/etc/aura/engine.yaml` the configuration by default is drawn from there. This overrides any configuration located in the local configuration file.

### Initialize database

Engine requires a PostgreSQL database to cache any programme info locally.

```bash
sudo -u postgres psql -f contrib/postgresql-create-database.sql
```

This creates a database and tables with default password `pass_1234`.

## Configuration

Edit the configuration file `config/engine.yaml`. Verify or change at least these config options:

```yaml
# The password for the local caching database holding scheduling information
db_pass: aura-engine-dbpass
# The secret which is used to authenticate against Tank
api_tank_secret: aura-engine-secret
```

## Running Engine

To start the Engine execute:

```shell
make run
```

## Docker

For production deployments follow the Docker Compose installation instruction for _AURA Playout_ at [docs.aura.radio](https://docs.aura.radio/).

The following instructions are meant for development.

### Build with Docker

Build your own, local Docker image

```shell
make docker.build
```

### Run with Docker

Run the locally build image

```shell
make docker.run
```

### Release to DockerHub

Releasing a new version to DockerHub

```shell
make docker.push
```

Usually this is not required, as it is done automatically by the CI/CD pipeline.

## Read more

- [Engine Developer Guide](docs/developer-guide.md)
- [Setting up the Audio Store](https://docs.aura.radio/en/latest/administration/setup-audio-store.html)
- [docs.aura.radio](https://docs.aura.radio)
- [aura.radio](https://aura.radio)
