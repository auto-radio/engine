#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-now() - The Aura Engine Team.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import time
import unittest

from aura_engine.base.utils import SimpleUtil as SU
from aura_engine.control import EngineExecutor


class TestEngineExecutor1(unittest.TestCase):
    """
    Testing the EngineExecutor.
    """

    def setUp(self):
        None

    def test_single_executor(self):
        print(self._testMethodName)

        # Initialize state and executor params
        EngineExecutor.timer_store = {}
        global_state = ["none"]
        due_time = SU.timestamp() + 2

        def f(param):
            global_state[0] = param

        # Before the executor is done there should be the initial value
        EngineExecutor("RANDOM_NAMESPACE", None, due_time, f, "hello singularity")
        self.assertEqual("none", global_state[0])

        # After 2 seconds there should be the updated value
        time.sleep(2)
        self.assertEqual("hello singularity", global_state[0])


class TestEngineExecutor2(unittest.TestCase):
    """
    Testing the EngineExecutor.
    """

    def setUp(self):
        None

    def test_two_executors(self):
        print(self._testMethodName)

        # Initialize state and executor params
        EngineExecutor.timer_store = {}
        global_state = ["none"]

        def f(param):
            global_state[0] = param

        # Before the executor 1 is done there should be the initial value
        due_time1 = SU.timestamp() + 3
        EngineExecutor("EXECUTOR_1", None, due_time1, f, "hello world from executor 1")
        self.assertEqual("none", global_state[0])
        self.assertNotEqual("hello world from executor 1", global_state[0])

        # Before the executor 2 is done there should be still the initial value
        due_time2 = SU.timestamp() + 1
        EngineExecutor("EXECUTOR_2", None, due_time2, f, "hello world from executor 2")
        self.assertEqual("none", global_state[0])
        self.assertNotEqual("hello world from executor 2", global_state[0])

        # After 0.3 seconds there still should be the initial value
        time.sleep(0.3)
        self.assertEqual("none", global_state[0])

        # After 1.5 seconds max there should be the updated value from executor 2
        time.sleep(1.2)
        self.assertEqual("hello world from executor 2", global_state[0])

        # After 5 seconds max there should be the updated value from executor 1
        time.sleep(3)
        self.assertEqual("hello world from executor 1", global_state[0])


class TestEngineExecutor3(unittest.TestCase):
    """
    Testing the EngineExecutor.
    """

    def setUp(self):
        None

    def test_parent_child_executors_in_order(self):
        print(self._testMethodName)

        # Initialize state and executor params
        EngineExecutor.timer_store = {}
        global_state = ["none"]

        def f(param):
            global_state[0] = param

        # Before the the parent is done there should be the initial value
        due_time1 = SU.timestamp() + 0.5
        parent = EngineExecutor("EXECUTOR_PARENT", None, due_time1, f, "hello world from parent")
        self.assertEqual("none", global_state[0])

        # Before the the child is done there should be the initial value
        due_time2 = SU.timestamp() + 1
        EngineExecutor("EXECUTOR_CHILD", parent, due_time2, f, "hello world from child")
        self.assertEqual("none", global_state[0])

        # After 0.3 seconds there still should be the initial value
        time.sleep(0.3)
        self.assertEqual("none", global_state[0])

        # After 0.6 seconds max there should be the updated value from parent executor
        time.sleep(0.3)
        self.assertEqual("hello world from parent", global_state[0])

        # After 1.2 seconds max there should be the updated value from child executor
        time.sleep(1.2)
        self.assertEqual("hello world from child", global_state[0])


class TestEngineExecutor4(unittest.TestCase):
    """
    Testing the EngineExecutor.
    """

    def setUp(self):
        None

    def test_parent_child_executors_with_child_before(self):
        print(self._testMethodName)

        # Initialize state and executor params
        EngineExecutor.timer_store = {}
        global_state = ["none", "never called by parent"]

        def f(param):
            global_state[0] = param
            if param == "hello world from parent":
                global_state[1] = param

        # Before the the parent is done there should be the initial value
        due_time1 = SU.timestamp() + 0.5
        parent = EngineExecutor("EXECUTOR_PARENT", None, due_time1, f, "hello world from parent")
        self.assertEqual("none", global_state[0])

        # Before the the child is done there should be the initial value
        due_time2 = SU.timestamp() + 1.5
        EngineExecutor("EXECUTOR_CHILD", parent, due_time2, f, "hello world from child")
        self.assertEqual("none", global_state[0])

        # After 0.2 seconds there still should be the initial value
        time.sleep(0.2)
        self.assertEqual("none", global_state[0])

        # After 0.4 seconds max there isn't a setting from the child yet, because it is waiting for
        # the parent
        time.sleep(0.2)
        self.assertNotEqual("hello world from child", global_state[0])

        # But the parent didn't set anything either, because it is scheduled for later
        self.assertNotEqual("hello world from parent", global_state[0])
        self.assertEqual("none", global_state[0])

        # Double check if it has ever been called by parent
        self.assertEqual("never called by parent", global_state[1])

        # After 2.2 seconds max there should be the updated value from parent & child
        # Because the child is due before the parent, it is executed right away,
        # hence overwriting the value just set by the parent
        time.sleep(2.2)
        self.assertNotEqual("hello world from parent", global_state[0])
        self.assertEqual("hello world from child", global_state[0])

        # But we do not just believe what we expect, but check if it really has ever been called by
        # a parent
        self.assertEqual("hello world from parent", global_state[1])


class TestEngineExecutor5(unittest.TestCase):
    """
    Testing the EngineExecutor.
    """

    def setUp(self):
        None

    def test_timer_store_replacement_after_parent_execution(self):
        print(self._testMethodName)

        # Initialize state and executor params
        EngineExecutor.timer_store = {}
        global_state = ["none", "none"]

        def f1(param):
            global_state[0] = param

        def f2(param):
            global_state[1] = param

        # There should be a total of 0 timers
        timers = EngineExecutor.command_history()
        self.assertEqual(0, len(timers))

        # Before the the parent is done there should be the initial value
        due_time1 = SU.timestamp() + 0.5
        parent = EngineExecutor("EXECUTOR_PARENT", None, due_time1, f1, "hello world from parent")
        self.assertEqual("none", global_state[0])

        # Before the the child is done there should be the initial value
        due_time2 = SU.timestamp() + 2
        EngineExecutor("EXECUTOR_CHILD", parent, due_time2, f2, "hello world from child")
        self.assertEqual("none", global_state[0])

        # There should be a total of 2 timers
        timers = EngineExecutor.command_history()
        self.assertEqual(2, len(timers))

        # Replacing the parent with a new instance
        parent = EngineExecutor(
            "EXECUTOR_PARENT", None, due_time1, f1, "hello world from alternative parent"
        )
        self.assertEqual("none", global_state[0])

        # Let the parent execute and do its stuff...
        time.sleep(1)
        # ... now there should be the updated value from the alternative parent
        self.assertEqual(False, parent.is_alive())
        self.assertEqual("hello world from alternative parent", global_state[0])

        # Now create a replacement child
        # Before the the child is done there should be the initial value
        EngineExecutor(
            "EXECUTOR_CHILD", parent, due_time2, f2, "hello world from alternative child"
        )
        self.assertEqual("none", global_state[1])

        # Wait for child execution
        time.sleep(2)

        # The child should not have been updated, since the parent was already finished before.
        self.assertEqual("hello world from child", global_state[1])

        # There should be a total of 2 timers, even though 4 got instantiated
        timers = EngineExecutor.command_history()
        self.assertEqual(2, len(timers))


class TestEngineExecutor6(unittest.TestCase):
    """
    Testing the EngineExecutor.
    """

    def setUp(self):
        None

    # Review why this test case fails from time to time:
    # @See https://gitlab.servus.at/aura/engine/-/issues/122

    # def test_parent_child_replacement_in_time(self):
    #     print(self._testMethodName)

    #     # Initialize state and executor params
    #     EngineExecutor.timer_store = {}
    #     global_state = ["none", "none"]

    #     def f1(param):
    #         global_state[0] = param

    #     def f2(param):
    #         global_state[1] = param

    #     # There should be a total of 0 timers
    #     timers = EngineExecutor.command_history()
    #     self.assertEqual(0, len(timers))

    #     # Before the the parent is done there should be the initial value
    #     due_time1 = SU.timestamp() + 2
    #     parent = EngineExecutor("EXECUTOR_PARENT", None, due_time1, f1, "hello world from parent")
    #     self.assertEqual("none", global_state[0])

    #     # Before the the child is done there should be the initial value
    #     due_time2 = SU.timestamp() + 3
    #     EngineExecutor("EXECUTOR_CHILD", parent, due_time2, f2, "hello world from child")
    #     self.assertEqual("none", global_state[1])

    #     # There should be a total of 2 timers
    #     timers = EngineExecutor.command_history()
    #     self.assertEqual(2, len(timers))

    #     time.sleep(0.1)

    #     # Create some new parent & child
    #     parent = EngineExecutor(
    #         "EXECUTOR_PARENT", None, due_time1, f1, "hello world from alternative parent"
    #     )
    #     child = EngineExecutor(
    #         "EXECUTOR_CHILD", parent, due_time2, f2, "hello world from alternative child"
    #     )

    #     # Nothing is executed yet, event after 1 seconds
    #     self.assertEqual("none", global_state[0])
    #     self.assertEqual("none", global_state[1])
    #     time.sleep(1)
    #     self.assertEqual("none", global_state[0])
    #     self.assertEqual("none", global_state[1])
    #     self.assertEqual(True, parent.is_alive())
    #     self.assertEqual(True, parent.is_alive())

    #     # Some seconds later, though...
    #     time.sleep(4)

    #     # Parent finished: There should be the updated value from the alternative parent
    #     self.assertEqual(False, parent.is_alive())
    #     self.assertEqual("hello world from alternative parent", global_state[0])

    #     # Child finished: There should be the updated value from the alternative child
    #     self.assertEqual(False, child.is_alive())
    #     self.assertEqual("hello world from alternative child", global_state[1])

    #     # There should be a total of 2 timers, even though 4 got instantiated
    #     timers = EngineExecutor.command_history()
    #     self.assertEqual(2, len(timers))


class TestEngineExecutor7(unittest.TestCase):
    """
    Testing the EngineExecutor.
    """

    def setUp(self):
        None

    def test_dead_parent_with_lively_child(self):
        print(self._testMethodName)

        # Initialize state and executor params
        EngineExecutor.timer_store = {}
        global_state = ["none", "none"]

        def f1(param):
            global_state[0] = param

        def f2(param):
            global_state[1] = param

        # Before the the parent is done there should be the initial value
        due_time1 = SU.timestamp() + 1
        parent = EngineExecutor("EXECUTOR_PARENT1", None, due_time1, f1, "hello parent1")
        self.assertEqual("none", global_state[0])

        # Before the the child is done there should be the initial value
        due_time2 = SU.timestamp() + 3
        child = EngineExecutor("EXECUTOR_CHILD1", parent, due_time2, f2, "hello child1")
        self.assertEqual("none", global_state[1])

        # Wait until the parent timer got executed
        time.sleep(2)
        self.assertEqual("hello parent1", global_state[0])

        # Parent dead - child alive
        self.assertEqual(False, parent.is_alive())
        self.assertEqual(True, child.is_alive())

        # Replacing the parent & child with a new instance
        parent = EngineExecutor("EXECUTOR_PARENT1", None, due_time1, f1, "hello parent2")
        child = EngineExecutor("EXECUTOR_CHILD1", parent, due_time2, f2, "hello child2")

        # New parent = dead before finished initialization already, so actually never born
        self.assertEqual(False, parent.is_alive())

        # Even though the late parent would be executed by now, it wasn't, because the initial
        # parent was finished at instantiation time already
        self.assertEqual("hello parent1", global_state[0])

        # Finally when the parent is already finished, the child should never update
        time.sleep(2)
        self.assertEqual("hello child1", global_state[1])


if __name__ == "__main__":
    unittest.main()
