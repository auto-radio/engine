#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-now() - The Aura Engine Team.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import unittest

from aura_engine.base.config import AuraConfig
from aura_engine.base.logger import AuraLogger


class TestLogger(unittest.TestCase):
    """
    Testing the Logger.
    """

    aura_logger = None

    def setUp(self):
        self.config = AuraConfig().config
        self.aura_logger = AuraLogger(self.config)

    def test_logger(self):
        print(self._testMethodName)
        self.assertTrue(self.aura_logger.logger.hasHandlers())


if __name__ == "__main__":
    unittest.main()
