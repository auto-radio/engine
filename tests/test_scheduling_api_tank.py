#
# Aura Engine (https://code.aura.radio/)
#
# Copyright (C) 2017-now() - The Aura Engine Team.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import json
import os
import unittest

import validators

from aura_engine.base.config import AuraConfig
from aura_tank_api.models.playlist import Playlist
from aura_tank_api.types import UNSET


class TestApiTank(unittest.TestCase):
    """
    Testing the Configuration.
    """

    config: AuraConfig
    mocked_tank_json = None

    def setUp(self):
        self.config = AuraConfig()
        with open("./tests/json/tank-api-v1-playlists-1.json", "r") as file:
            self.mocked_tank_json = json.load(file)

    def test_api_tank_model(self):
        print(self._testMethodName)

        print("Tank JSON: " + str(self.mocked_tank_json))
        playlist = Playlist()
        self.assertEqual(UNSET, playlist.id)
        playlist = playlist.from_dict(self.mocked_tank_json)
        self.assertIsNotNone(playlist.to_dict())

        self.assertEqual(1, playlist.id)
        self.assertEqual("musikprogramm", playlist.show_name)
        self.assertEqual("linear", playlist.playout_mode)
        self.assertEqual("test", playlist.description)
        self.assertEqual("2023-02-28T15:25:38.684803+01:00", playlist.created)
        self.assertEqual("2023-02-28T15:25:38.684803+01:00", playlist.updated)

        entries = playlist.entries
        self.assertIsNotNone(1, playlist.entries)
        self.assertEqual(1, len(entries))
        self.assertEqual("file://musikprogramm/2", entries[0].uri)
        self.assertEqual(199040000000, entries[0].duration)

        file = playlist.entries[0].file
        self.assertEqual(2, file.id)
        self.assertEqual(
            "sha256:b4e1922bad633ff0e11f55611f04cb3807d15d70bb09969d2b324373af47b574",
            file.source.hash_,
        )
        self.assertEqual("musikprogramm", file.show_name)
        self.assertEqual("upload://some-audio-file.flac", file.source.uri)
        self.assertEqual("Test Artist", file.metadata.artist)
        self.assertEqual("Test Track Title", file.metadata.title)
        self.assertEqual("Test Album", file.metadata.album)


if __name__ == "__main__":
    unittest.main()
