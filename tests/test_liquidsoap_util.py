#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-now() - The Aura Engine Team.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import unittest
from unittest import mock

import requests

from aura_engine.base.api import LiquidsoapUtil
from aura_engine.base.config import AuraConfig


class TestLiquidsoapUtil(unittest.TestCase):
    """
    Testing the Configuration.
    """

    config = None
    api = None

    #
    # Setup
    #

    def setUp(self):
        pass

    #
    # Tests
    #

    def test_json_to_dict(self):
        print(self._testMethodName)

        json_str = '+{-"key"-:-"value"-}+'
        json_dict = LiquidsoapUtil.json_to_dict(json_str)

        # Check if config is available
        self.assertIsNotNone(json_dict)
        self.assertEqual("value", json_dict.get("key"))

    def test_annotate_uri(self):
        print(self._testMethodName)

        uri = "/some/uri"
        meta = {"cue": 1, "volume": 100}
        uri = LiquidsoapUtil.annotate_uri(uri, meta)

        self.assertEqual('annotate:cue="1",volume="100":/some/uri', uri)
