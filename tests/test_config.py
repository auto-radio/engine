#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-now() - The Aura Engine Team.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import unittest

import validators

from aura_engine.base.config import AuraConfig


class TestConfig(unittest.TestCase):
    """
    Testing the Configuration.
    """

    config = None

    def setUp(self):
        self.config = AuraConfig()

    def test_config(self):
        print(self._testMethodName)

        # Check if config is available
        self.assertIsNotNone(self.config.yaml_path)

        # Check if "install_dir" is a valid directory (is evaluated at runtime)
        self.assertTrue(os.path.isdir(self.config.confuse_config["install_dir"].get()))

        # Reference to confuse config
        cfg = self.config.config

        # Check API Urls
        self.assertTrue(validators.url(cfg.api.steering.status))
        self.assertTrue(validators.url(cfg.api.steering.calendar))
        self.assertTrue(validators.url(cfg.api.tank.status))
        tank_playlist_url = cfg.api.tank.playlist.replace("${ID}", "1")
        self.assertTrue(validators.url(tank_playlist_url))
        self.assertTrue(validators.url(cfg.api.engine.status))
        self.assertTrue(validators.url(cfg.api.engine.store_playlog))
        self.assertTrue(validators.url(cfg.api.engine.store_clock))
        engine_health_url = cfg.api.engine.store_health.replace("${ENGINE_NUMBER}", "1")
        self.assertTrue(validators.url(engine_health_url))

        # Check if database settings are set
        self.assertIsNotNone(cfg.scheduler.db.user)
        self.assertIsNotNone(cfg.scheduler.db.pwd)
        self.assertIsNotNone(cfg.scheduler.db.name)
        self.assertIsNotNone(cfg.scheduler.db.host)


if __name__ == "__main__":
    unittest.main()
