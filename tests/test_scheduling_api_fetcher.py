#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-now() - The Aura Engine Team.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import json
import os
import unittest
from datetime import datetime, timedelta
from unittest import mock

from aura_engine.base.config import AuraConfig
from aura_engine.scheduling.api import ApiFetcher


class TestSchedulingApiFetcher(unittest.TestCase):
    """
    Testing the Configuration.
    """

    config = None
    api_fetcher = None
    mocked_steering_json = None
    mocked_tank_json = None

    #
    # Mock
    #

    # Mock `requests.get` in `SimpleRestApi`
    def mocked_requests_get(*args, **kwargs):
        class MockResponse:
            def __init__(self, json_data, status_code):
                self.json_data = json_data
                self.status_code = status_code

            def json(self):
                return self.json_data

        print(f"Calling mocked 'requests.get' with '{args[0]}'")
        if "/api/v1/playout" in args[0]:
            return MockResponse(TestSchedulingApiFetcher.mocked_steering_json, 200)
        elif "/api/v1/playlists/1" in args[0]:
            return MockResponse(TestSchedulingApiFetcher.mocked_tank_json, 200)

        return MockResponse(None, 404)

    #
    # Setup
    #

    def setUp(self):
        self.config = AuraConfig()
        self.api_fetcher = ApiFetcher()

        with open("./tests/json/steering-api-v1-playout.json", "r") as file:
            TestSchedulingApiFetcher.mocked_steering_json = json.load(file)
        with open("./tests/json/tank-api-v1-playlists-1.json", "r") as file:
            TestSchedulingApiFetcher.mocked_tank_json = json.load(file)

        # Update dates that they are in the future
        # Format e.g. "start":"2023-05-16T10:06:00"
        now = datetime.now()
        hour: int = -1

        for timeslot in TestSchedulingApiFetcher.mocked_steering_json:
            start = (now + timedelta(hours=hour)).strftime("%Y-%m-%dT%H:%M:%S")
            end = (now + timedelta(hours=hour + 1)).strftime("%Y-%m-%dT%H:%M:%S")
            timeslot["start"] = start
            timeslot["end"] = end
            hour += 1

    #
    # Test Cases
    #

    @mock.patch("aura_engine.base.api.requests.get", side_effect=mocked_requests_get)
    def test_fetch_data(self, mock_get):
        print(self._testMethodName)

        self.api_fetcher.start()
        response = self.api_fetcher.get_fetched_data()
        # print("[test_fetch_data] response: " + str(response))

        # Test Timetable
        self.assertEqual(list, type(response))
        self.assertEqual(2, len(response))

        # Test Timeslot 1
        ts1 = response[0]
        self.assertEqual(1, ts1["showId"])
        self.assertEqual("Musikprogramm", ts1["showName"])
        self.assertEqual("Wurlitzer", ts1["title"])
        self.assertEqual(1, ts1["playlist_id"])
        self.assertEqual(1, ts1["playlist"]["id"])
        self.assertEqual("musikprogramm", ts1["playlist"]["showName"])
        self.assertEqual("test", ts1["playlist"]["description"])
        self.assertEqual("linear", ts1["playlist"]["playoutMode"])

        # Test Timeslot 2
        ts2 = response[1]
        self.assertEqual(1, ts2["showId"])
        self.assertEqual("Musikprogramm", ts2["showName"])
        self.assertEqual("Pepi's Polka II", ts2["title"])
        self.assertEqual(1, ts2["playlist_id"])
        self.assertEqual(1, ts2["playlist"]["id"])
        self.assertEqual("musikprogramm", ts2["playlist"]["showName"])
        self.assertEqual("test", ts2["playlist"]["description"])
        self.assertEqual("linear", ts2["playlist"]["playoutMode"])
        self.assertEqual("musikprogramm", ts2["default_schedule_playlist"]["showName"])
        self.assertEqual("test", ts2["default_schedule_playlist"]["description"])
        self.assertEqual("linear", ts2["default_schedule_playlist"]["playoutMode"])
        self.assertEqual("musikprogramm", ts2["default_show_playlist"]["showName"])
        self.assertEqual("test", ts2["default_show_playlist"]["description"])
        self.assertEqual("linear", ts2["default_show_playlist"]["playoutMode"])


if __name__ == "__main__":
    unittest.main()
