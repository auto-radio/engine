#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-now() - The Aura Engine Team.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import unittest

# from aura_engine.app import EngineRunner
from aura_engine.base.config import AuraConfig

# from unittest import mock


class TestApp(unittest.TestCase):
    """
    Testing the Configuration.
    """

    config = None

    #
    # Mock
    #

    def mocked_engine_start(*args, **kwargs):
        print(args, kwargs)
        print("Mocked start finished")

    #
    # Setup
    #

    def setUp(self):
        self.config = AuraConfig()

    #
    # Test Cases
    #

    # FIXME For some unknown reason, this Mock leaks into other test cases
    # When this test case is enabled, these tests fail:
    # - tests.test_engine_executor - test_parent_child_executors_in_order
    # - tests.test_engine_executor - test_parent_child_executors_with_child_before
    # - tests.test_engine_executor - test_timer_store_replacement_after_parent_execution
    #
    # @mock.patch("aura_engine.engine.Engine.start", side_effect=mocked_engine_start)
    # def test_run_app(self, engine_start_mock):
    #     runner = EngineRunner()
    #     self.assertIsNotNone(runner)
    #     runner.run()

    #     try:
    #         runner.exit_gracefully(signum=1, frame=1)
    #     except:
    #         pass


if __name__ == "__main__":
    unittest.main()
