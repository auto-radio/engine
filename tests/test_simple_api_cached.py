#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-now() - The Aura Engine Team.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import unittest
from unittest import mock

from aura_engine.base.api import SimpleCachedRestApi, SimpleRestApi
from aura_engine.base.config import AuraConfig


class TestCachedApi(unittest.TestCase):
    """
    Testing the Configuration.
    """

    config = None
    api = None

    #
    # Mock
    #

    # Mock `requests.get` in `SimpleRestApi`
    def mocked_requests_get(*args, **kwargs):
        class MockResponse:
            def __init__(self, json_data, status_code):
                self.json_data = json_data
                self.status_code = status_code

            def json(self):
                return self.json_data

        print(f"Calling mocked 'requests.get' with '{args[0]}'")
        if args[0] == "http://aura.test.available/dummy-api/v1/playout":
            return MockResponse({"foo": "bar"}, 200)
        elif args[0] == "http://aura.test.404/dummy-api/v1/playout":
            return MockResponse(None, 404)
        elif args[0] == "http://aura.test.not-json/dummy-api/v1/not-json":
            return MockResponse("{-that's-definitely-not-json}", 200)

        return MockResponse(None, 404)

    #
    # Setup
    #

    def setUp(self):
        self.config = AuraConfig()
        cache_location = self.config.config.general.cache_dir
        self.api = SimpleCachedRestApi(SimpleRestApi(), cache_location)

    #
    # Test Cases
    #

    def test_config(self):
        print(self._testMethodName)

        # Check if config is available
        self.assertIsNotNone(self.config.yaml_path)

    def test_build_filename(self):
        url = "https://dashboard.aura.radio/steering/api/v1/playout"
        filename = self.api.build_filename(url)

        # Success
        expected = "steering-api-v1-playout.json"
        self.assertEqual(expected, filename)

    def test_prune_cache_dir(self):
        dir = self.api.cache_location
        f = dir + "dummy-file"
        with open(f, "a"):
            os.utime(f, None)

        count = len(os.listdir(dir))
        self.assertNotEqual(0, count)
        self.api.prune_cache_dir()
        count = len(os.listdir(dir))
        self.assertEqual(0, count)

    @mock.patch("aura_engine.base.api.requests.get", side_effect=mocked_requests_get)
    def test_get_from_network(self, mock_get):
        print(self._testMethodName)
        self.api.prune_cache_dir()
        url = "http://aura.test.available/dummy-api/v1/playout"
        result = self.api.get(url)

        # 200 - Success
        self.assertEqual(200, result.response.status_code)
        self.assertEqual("bar", result.json.get("foo"))

    @mock.patch("aura_engine.base.api.requests.get", side_effect=mocked_requests_get)
    def test_get_from_cache(self, mock_get):
        print(self._testMethodName)
        self.api.prune_cache_dir()
        # Ensure a cached response is created
        self.api.get("http://aura.test.available/dummy-api/v1/playout")
        # Read the same endpoint from an invalid to domain, enforcing a local cache response
        result = self.api.get("http://aura.test.404/dummy-api/v1/playout")

        # Read from local cache: 304 - Not Modified
        self.assertEqual(304, result.response.status_code)

    @mock.patch("aura_engine.base.api.requests.get", side_effect=mocked_requests_get)
    def test_get_not_existing_from_cache(self, mock_get):
        print(self._testMethodName)
        self.api.prune_cache_dir()
        # Read the same endpoint from an invalid to domain, enforcing a local cache response
        result = self.api.get("http://aura.test.404/dummy-api/v1/404")

        # Read from local cache: 404 - Not Found
        self.assertEqual(404, result.response.status_code)

    @mock.patch("aura_engine.base.api.requests.get", side_effect=mocked_requests_get)
    def test_get_from_cache_with_invalid_json(self, mock_get):
        print(self._testMethodName)
        self.api.prune_cache_dir()
        # Get response with invalid JSON data
        result = self.api.get("http://aura.test.not-json/dummy-api/v1/not-json")
        self.assertEqual(200, result.response.status_code)
        # self.assertEqual(None, result.json)
        self.assertEqual("{-that's-definitely-not-json}", result.json)

        # Read the same endpoint from an invalid to domain, enforcing a local cache response
        result = self.api.get("http://aura.test.404/dummy-api/v1/not-json")

        # Read from local cache: 304 - Not Found
        self.assertEqual(304, result.response.status_code)
        self.assertEqual("{-that's-definitely-not-json}", result.json)


if __name__ == "__main__":
    unittest.main()
