#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-now() - The Aura Engine Team.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import json
import unittest
from unittest import mock

import requests

from aura_engine.base.api import SimpleRestApi
from aura_engine.base.config import AuraConfig


class TestApi(unittest.TestCase):
    """
    Testing the Configuration.
    """

    config = None
    api = None

    #
    # Mock
    #

    # Mock `requests.get` in `SimpleRestApi`
    def mocked_requests_get(*args, **kwargs):
        class MockResponse:
            def __init__(self, json_data, status_code):
                self.json_data = json_data
                self.status_code = status_code

            def json(self):
                return self.json_data

        print(f"Calling mocked 'requests.get' with '{args[0]}'")
        if args[0] == "http://aura.test.available":
            return MockResponse({"foo": "bar"}, 200)
        if args[0] == "http://aura.test.available/bad-request":
            return MockResponse({}, 400)
        if args[0] == "http://aura.test.available/connection-error":
            raise requests.exceptions.ConnectionError
        if args[0] == "http://aura.test.available/timeout":
            raise requests.exceptions.Timeout
        if args[0] == "http://aura.test.available/exception":
            raise Exception
        if args[0] == "http://aura.test.available/not-found":
            return MockResponse({}, 404)
        if args[0] == "https://some.website.no.api":
            return MockResponse({}, 405)

        return MockResponse(None, 404)

    # Mock `requests.put` in `SimpleRestApi`
    def mocked_requests_put(*args, **kwargs):
        class MockResponse:
            def __init__(self, json_data, status_code):
                self.json_data = json_data
                self.status_code = status_code

            def json(self):
                return self.json_data

        print(f"Calling mocked 'requests.put' with '{args[0]}'")
        if args[0] == "http://aura.test.available/bad-request":
            return MockResponse({}, 400)
        if args[0] == "http://aura.test.available/not-found":
            return MockResponse({}, 404)

        return MockResponse(None, 404)

    #
    # Setup
    #

    def setUp(self):
        self.config = AuraConfig()
        self.api = SimpleRestApi()

    #
    # Tests
    #

    def test_config(self):
        print(self._testMethodName)

        # Check if config is available
        self.assertIsNotNone(self.config.yaml_path)

    def test_clean_dict(self):
        print(self._testMethodName)

        data = {"foo": {"bar": None}, "foo2": None}
        self.assertEqual(2, len(data.keys()))
        data = self.api.clean_dictionary(data)

        self.assertEqual(1, len(data.keys()))
        self.assertEqual(["foo"], list(data.keys()))

    def test_serialize_json(self):
        print(self._testMethodName)

        import re

        data = {"foo": "bar", "fooBar2000": None}
        json_string = '{"foo":"bar"}'

        # Success
        result = self.api.serialize_json(data)
        result = re.sub(r'[^a-z0-9\{\}\:"]+', "", result, flags=re.IGNORECASE)
        self.assertEqual(json_string, result)

        # Check for cleaned up key
        try:
            print(result.index("fooBar2000"))
            self.assertFalse("Error: Value found in dict")
        except ValueError:
            self.assertTrue("Value not found in dict")

    @mock.patch("aura_engine.base.api.requests.get", side_effect=mocked_requests_get)
    def test_get(self, mock_get):
        print(self._testMethodName)
        result = self.api.get("http://aura.test.available")

        # Success
        self.assertEqual(200, result.response.status_code)
        self.assertEqual("bar", result.json["foo"])

    @mock.patch("aura_engine.base.api.requests.get", side_effect=mocked_requests_get)
    def test_get_bad_request(self, mock_get):
        print(self._testMethodName)
        result = self.api.get("http://aura.test.available/bad-request")

        # Bad Request
        self.assertEqual(400, result.response.status_code)

    @mock.patch("aura_engine.base.api.requests.get", side_effect=mocked_requests_get)
    def test_get_connection_error(self, mock_get):
        print(self._testMethodName)
        result = self.api.get("http://aura.test.available/connection-error")

        # Bad Request
        self.assertEqual(400, result.response.status_code)

    @mock.patch("aura_engine.base.api.requests.post")
    def test_post_success(self, mock_post):
        mock_post.return_value.status_code = 201
        mock_post.return_value.json.return_value = "mock response"

        print(self._testMethodName)
        data = {"foo": "bar"}

        result = self.api.post("http://aura.test.available/api", data=data)

        mock_post.assert_called_once_with(
            "http://aura.test.available/api",
            data=json.dumps({"foo": "bar"}, indent=4, sort_keys=True, default=str),
            headers={"content-type": "application/json"},
        )

        # print(result)
        self.assertEqual(201, result.response.status_code)

    @mock.patch("aura_engine.base.api.requests.put")
    def test_put_success(self, mock_post):
        mock_post.return_value.status_code = 200
        mock_post.return_value.json.return_value = "mock response"

        print(self._testMethodName)
        data = {"foo": "bar"}

        result = self.api.put("http://aura.test.available/api", data=data)

        mock_post.assert_called_once_with(
            "http://aura.test.available/api",
            data=json.dumps({"foo": "bar"}, indent=4, sort_keys=True, default=str),
            headers={"content-type": "application/json"},
        )

        # print(result)
        self.assertEqual(200, result.response.status_code)

    @mock.patch("aura_engine.base.api.requests.put", side_effect=mocked_requests_put)
    def test_put(self, mock_put):
        print(self._testMethodName)
        data = {"foo": "bar"}

        # Bad request: Invalid URL
        result = self.api.put("http://aura.test.available/bad-request", data=data)
        # print(result)
        self.assertEqual(400, result.response.status_code)

        # Not found
        result = self.api.put("http://aura.test.available/not-found", data=data)
        # print(result)
        self.assertEqual(404, result.response.status_code)


if __name__ == "__main__":
    unittest.main()
