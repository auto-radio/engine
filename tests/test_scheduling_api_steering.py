#
# Aura Engine (https://code.aura.radio/)
#
# Copyright (C) 2017-now() - The Aura Engine Team.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import json
import unittest
from datetime import datetime

from aura_engine.base.config import AuraConfig
from aura_steering_api.models.timeslot import Timeslot
from aura_steering_api.types import UNSET


class TestApiSteering(unittest.TestCase):
    """
    Testing the Configuration.
    """

    config: AuraConfig
    mocked_steering_json = None

    def setUp(self):
        self.config = AuraConfig()
        with open("./tests/json/steering-api-v1-playout.json", "r") as file:
            self.mocked_steering_json = json.load(file)

    def test_api_steering_timeslot_model(self):
        print(self._testMethodName)
        print("Steering JSON: " + str(self.mocked_steering_json))

        # FIXME Steering doesn't provide JSON as per a valid JSON schema.
        # Hence we need to split the array manually
        timetable = []
        timeslot = Timeslot()
        for entry in self.mocked_steering_json:
            t = timeslot.from_dict(entry)
            self.assertIsNotNone(t.to_dict())
            timetable.append(t)

        self.assertIsNotNone(timetable)
        self.assertEqual(3, len(timetable))

        t1 = timetable[0]
        self.assertEqual(17, t1.id)
        self.assertEqual(datetime.strptime("2023-05-16T10:06:00", "%Y-%m-%dT%H:%M:%S"), t1.start)
        self.assertEqual(datetime.strptime("2023-05-16T10:10:00", "%Y-%m-%dT%H:%M:%S"), t1.end)
        self.assertEqual("Pepi's Polka", t1.title)
        self.assertEqual(17, t1.schedule_id)
        self.assertEqual(13, t1.repetition_of_id)
        self.assertEqual(1, t1.playlist_id)
        self.assertEqual(None, t1.schedule_default_playlist_id)
        self.assertEqual(None, t1.show_default_playlist_id)
        self.assertEqual(1, t1.show_id)
        self.assertEqual("Musikprogramm", t1.show_name)
        self.assertEqual("Musikredaktion", t1.show_hosts)
        self.assertEqual("Unmoderiertes Musikprogramm", t1.show_type)
        self.assertEqual("", t1.show_categories)
        self.assertEqual("", t1.show_topics)
        self.assertEqual("", t1.show_music_focus)
        self.assertEqual("", t1.show_languages)
        self.assertEqual("Standard", t1.show_funding_category)


if __name__ == "__main__":
    unittest.main()
